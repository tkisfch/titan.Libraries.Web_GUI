// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
"use strict";

/* oldff means firefox < 4.0, which has a buggy SVG support and missing a lot of features, which sucks. */
var oldff = /Firefox\/([1-3])\./i.test(navigator.userAgent);
/* isIE10 means exactly IE10, which has a buggy SVG support and crashes the renderer(== not drawing anything) if markers are applied dynamically, which sucks. */
var isIE10 = false; // Will be changed by IE-specific Conditional Compilation. JS minification will corrupt this!!!
/*@cc_on
    if (/^10/.test(@_jscript_version)) {
        isIE10 = true;
    }
@*/
var isIE11 = !(window.ActiveXObject) && "ActiveXObject" in window;

function browserCheck() {
    var N= navigator.appName, ua= navigator.userAgent, tem;
    var M= ua.match(/(opera|chrome|safari|firefox|msie|trident)\/?\s*(\.?\d+(\.\d+)*)/i);
    if(M && (tem= ua.match(/version\/([\.\d]+)/i))!= null) {M[2]=tem[1];}
    M= M? [M[1], M[2]]: [N, navigator.appVersion,'-?'];
    return M;
}

//# sourceURL=Utils\Compat.js