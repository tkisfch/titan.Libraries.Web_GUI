// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function DataSourceUtils() {
    "use strict";
    
    var v_this = this;
    
    this.convertHelpToTreeDataArray = function(p_help, p_elementIcon) {
        var v_contents = [];
        var info;

        for (var i = 0; i < p_help.length; i++) {
            var v_childrens = [];
            var v_row = {};

            if (p_help[i].dataElement != undefined) {
                v_row.text = p_help[i].dataElement.name;
                if (p_elementIcon)
                    v_row.icon = p_elementIcon;
            }
            else {
                v_row.text = p_help[i].source;
            }

            if (p_help[i].dataElement != undefined) {
                info = JSON.stringify(p_help[i].dataElement, null, 4);
                info = replaceAll(info , '\\"', "'");
                info = replaceAll(info , '"', "'");
            } else {
                info = "No description";
            }
            v_row.data = info;
            
            if (p_help[i].children != undefined && p_help[i].children.length > 0) {
                v_childrens = this.convertHelpToTreeDataArray(p_help[i].children, p_elementIcon);
            } else if (p_help[i].dataElements != undefined && p_help[i].dataElements.length > 0) {
                v_childrens = this.convertHelpToTreeDataArray(p_help[i].dataElements, p_elementIcon);
            }
            v_row.children = v_childrens;
            v_contents.push(v_row);
        }
        return v_contents;
    };    

    this.convertRequestToTreeDataArray = function(a_request) {
        var v_content = [];

        for (var i = 0; i < a_request.length; i++) {
            var v_childrens = [];
            var v_row = {};
            
            var setOrGetData;
            if (a_request[i].getData)
                setOrGetData = "getData";
            else if (a_request[i].setData)
                setOrGetData = "setData";
            if (setOrGetData && a_request[i][setOrGetData] && a_request[i][setOrGetData].children != undefined && a_request[i][setOrGetData].children.length > 0) {
                v_childrens = this.convertRequestToTreeDataArray(a_request[i][setOrGetData].children);
            }
            if (a_request[i][setOrGetData])
                v_row.text = a_request[i][setOrGetData].element;

            if (v_childrens.length > 0) {
                v_row.children = v_childrens;
            }
            v_content.push(v_row);
        }
        return v_content;
    };    
    
    this.dataSkeletonEquals = function(response1, response2) {
        if (response1 == undefined && response2 == undefined)
            return true;
        else if ((response1 == undefined && response2 != undefined) || (response2 == undefined && response1 != undefined))
            return false;
        else if (response1.node != undefined && response2.node != undefined)
        {
            if ((response1.node.childVals == undefined) && (response2.node.childVals == undefined))
                return true;
            else if ((response1.node.childVals != undefined) && (response2.node.childVals != undefined))
            {
                if (response1.node.childVals.length !== response2.node.childVals.length)
                    return false;            
                for (var i = 0; i < response1.node.childVals.length; i++)
                    if (!this.dataSkeletonEquals(response1.node.childVals[i], response2.node.childVals[i]))
                        return false;
                return true;
            }
            else
                return false;
        }
        else if (response1.list != undefined && response2.list != undefined)
        {
            if (response1.list.length !== response2.list.length)
                return false;
            for (var j = 0; j < response1.list.length; j++)
                if (!this.dataSkeletonEquals(response1.list[j], response2.list[j]))
                    return false;
            return true;
        }
        else if (response1.length != undefined)
        {
            if (response2.length == undefined)
                return false;
            if (response1.length !== response2.length)
                return false;
            for (var k = 0; k < response1.length; k++)
                if (!this.dataSkeletonEquals(response1[k], response2[k]))
                    return false;
            return true;
        }
        else
            return false;
    };
    
    this.expandResponse = function(a_response, a_nestingLevel, a_parentList, a_parentIndexes, a_request, aIdxInList) {
        var lParentList;
        var lParentIndexes;
        if (a_response)
        {
            if (a_response.length != undefined) // childVals or root list
            {
                if (a_request == undefined || a_response.length != a_request.length) {
                    return false;
                }
                for (var i = 0; i < a_response.length; i++)
                {
                    lParentList = [];
                    for (var j = 0; j < a_parentList.length; j++)
                        lParentList[j] = a_parentList[j];
                    lParentIndexes = mcopy(a_parentIndexes);
                    if (a_request)
                        if (!this.expandResponse(a_response[i], a_nestingLevel + 1, lParentList, lParentIndexes, a_request[i])) {
                            return false;
                        }
                }
            }
            else if (a_response.node != undefined && a_request != undefined)
            {
                var getOrSetData;
                if (a_request.getData != undefined) {
                    getOrSetData = "getData";
                } else if (a_request.setData != undefined) {
                    getOrSetData = "setData";
                } else {
                    return false;
                }
                a_response.getData = {};
                a_response.getData.tp = a_response.node.tp;
                a_response.getData.source = a_request[getOrSetData].source;
                a_response.getData.element = a_request[getOrSetData].element;
                if (a_request[getOrSetData].ptcname != undefined)
                    a_response.getData.ptcname = a_request[getOrSetData].ptcname;
                if (a_request[getOrSetData].params != undefined)
                {
                    a_response.getData.params = [];
                    for (var j = 0; j < a_request[getOrSetData].params.length; j++)
                    {
                        a_response.getData.params[j] = {};
                        a_response.getData.params[j].paramName = a_request[getOrSetData].params[j].paramName;
                        a_response.getData.params[j].paramValue = a_request[getOrSetData].params[j].paramValue;
                    }
                }
                a_response.getData.idxInList = aIdxInList;
                a_parentList[a_parentList.length] = a_response.node.val;
                a_parentIndexes[a_parentIndexes.length] = aIdxInList;
                completeGetDataParameters(a_response.getData, a_parentList, a_parentIndexes);
                if (a_response.node.childVals != undefined) {
                    var ans = this.expandResponse(a_response.node.childVals, a_nestingLevel, a_parentList, a_parentIndexes, a_request[getOrSetData].children);
                    if (!ans) {
                        return false;
                    } else {
                        a_response.node.childVals = ans;
                    }
                } else {
                    if (a_response.node.tp != 0 && a_request[getOrSetData].children != undefined && a_request[getOrSetData].children.length != 0 && (a_request[getOrSetData].selection == undefined ||  aIdxInList == a_request[getOrSetData].selection[0])) {
                        return false;
                    }
                }
                a_response.getData.origRq = a_request[getOrSetData];
            }
            else if (a_response.list != undefined)
            {
                for (var i = 0; i < a_response.list.length; i++)
                {
                    lParentList = [];
                    for (var j = 0; j < a_parentList.length; j++)
                        lParentList[j] = a_parentList[j];
                    lParentIndexes = mcopy(a_parentIndexes);
                    if (!this.expandResponse(a_response.list[i], a_nestingLevel + 1, lParentList, lParentIndexes, a_request, i)) {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }
        return a_response;
    };
    
    this.isResponseToRequest = function(p_contentList, p_requests) {
        if (p_contentList.length != p_requests.length) {
            // at least one response is missing
            return false;
        }
        
        for (var i = 0; i < p_requests.length; ++i) {
            var request = p_requests[i].getData;
            var response = p_contentList[i];
            if (response.node != undefined) {
                if (request.children != undefined && response.node.childVals != undefined) {
                    if (!this.isResponseToRequest(response.node.childVals, request.children)) {
                        return false;
                    }
                }
                if (request.children != undefined && request.children.length > 0 && (response.node.childVals == undefined || response.node.childVals.length == 0) && request.filter == undefined) {
                    // the response is a node, the request has children and no filter but the response does not have childVals
                    return false;
                } else if (response.node.childVals != undefined && response.node.childVals.length != 0 && (request.children == undefined || request.children.length == 0)) {
                    // the response has childVals but the request does not have children
                    return false;
                }
            } else if (response.list != undefined) {
                if (request.selectionValues != undefined && request.selectionValues[0] != undefined && response.list[0] != undefined && request.selectionValues[0] != response.list[0].node.val) {
                    // response is a lisr, request has selection value but the response is different
                    return false;
                }
                
                for (var j = 0; j < response.list.length; ++j) {
                    var responseNode = response.list[j];
                    if (responseNode.node.childVals != undefined && responseNode.node.childVals.length > 0) {
                        if (request.children == undefined || request.children.length == 0) {
                            // response has childVals but the request does not have children
                            return false;
                        }
                        
                        if (request.selectionValues != undefined) {
                            if (j > 0) {
                                // request has selection values and a response other than the first has childVals
                                return false;
                            }
                        } else if (request.selection != undefined) {
                            if (request.selection[0] != j) {
                                // request has selection and a response other than the one specified by the selection has childVals
                                return false;
                            }
                        }
                        
                        if (!this.isResponseToRequest(responseNode.node.childVals, request.children)) {
                            return false;
                        }
                    }
                }
            } else {
                // response is not a node and not a list
                return false;
            }
        }
        
        return true;
    }
    
    /*
    // childVals or root response, children or root requests
    this.expandResponse = function(p_contentList, p_requests, p_parentList) {
        if (p_contentList != undefined) {
            for (var i = 0; i < p_requests.length; ++i) {
                expand(p_contentList[i], p_requests[i], p_parentList);
            }
        }
        return p_contentList;
    };
    
    function expand(p_content, p_request, p_parentList) {
        if (p_content != undefined && p_content.node != undefined) {
            addRequestToResponse(p_content, p_request, undefined, p_parentList);
            if (p_request.getData != undefined && p_request.getData.children != undefined) {
                p_parentList.push(p_content.node.val);
                v_this.expandResponse(p_content.node.childVals, p_request.getData.children, p_parentList);
                p_parentList.pop();
            }
        } else if (p_content != undefined && p_content.list != undefined) {
            for (var i = 0; i < p_content.list.length; ++i) {
                addRequestToResponse(p_content.list[i], p_request, i, p_parentList);
                if (p_request.getData != undefined && p_request.getData.children != undefined) {
                    p_parentList.push(p_content.list[i].node.val);
                    v_this.expandResponse(p_content.list[i].node.childVals, p_request.getData.children, p_parentList);
                    p_parentList.pop();
                }
            }
        }
    }
    
    function addRequestToResponse(p_node, p_request, p_indexInList, p_parentList) {
        var getOrSetData;
        if (p_request.getData != undefined) {
            getOrSetData = p_request.getData;
        } else if (p_request.setData != undefined) {
            getOrSetData = p_request.setData;
        } else {
            return;
        }
        
        p_node.getData = {};
        p_node.getData.tp = p_node.node.tp;
        p_node.getData.source = getOrSetData.source;
        p_node.getData.element = getOrSetData.element;
        p_node.getData.ptcname = getOrSetData.ptcname;
        if (getOrSetData.params != undefined) {
            p_node.getData.params = [];
            for (var i = 0; i < getOrSetData.params.length; i++) {
                p_node.getData.params[i] = {};
                p_node.getData.params[i].paramName = getOrSetData.params[i].paramName;
                p_node.getData.params[i].paramValue = getOrSetData.params[i].paramValue;
            }
        }
        p_node.getData.idxInList = p_indexInList;
        p_node.getData.origRq = getOrSetData;
        completeGetDataParameters(p_node.getData, p_parentList);
    }
    */
    
    function completeGetDataParameters(pl_getData, pl_parentList, pl_parentIndexes) {
        function replaceTemplate(pl_replaceTemplate, pl_replaceWith) {
            if (pl_getData.source === pl_replaceTemplate)
                pl_getData.source = pl_replaceWith;
            if (pl_getData.element === pl_replaceTemplate)
                pl_getData.element = pl_replaceWith;
            if (pl_getData.ptcname === pl_replaceTemplate)
                pl_getData.ptcname = pl_replaceWith;
            if (pl_getData.params != undefined)
                for (var i = 0; i < pl_getData.params.length; i++)
                    if (pl_getData.params[i].paramValue === pl_replaceTemplate)
                        pl_getData.params[i].paramValue = pl_replaceWith;
        }
        
        for (var vl_i = 0; vl_i < pl_parentList.length; vl_i++) {
            replaceTemplate("%Parent" + vl_i + "%", pl_parentList[vl_i]);
            replaceTemplate("%Parent" + vl_i + "::idx%", "" + pl_parentIndexes[vl_i]);
        }
    }
        
    this.setValue = function(requestList, contentList, path, indexInList, value, additionalSelections) {
        var nextSelectionIndex = 0;
        var parents = [];
        var parentIndexes = [];
        var request;
        var response;
        
        for (var i = 0; i < path.length; ++i) {
            if (requestList == undefined || contentList == undefined || requestList[path[i]] == undefined || contentList[path[i]] == undefined) {
                // the response or the request has no more children, but the path points below the current element
                return undefined;
            }
            
            request = requestList[path[i]];
            response = contentList[path[i]];
            
            if (response.node != undefined) {
                parents.push(response.node.val);
                parentIndexes.push(0);
            } else if (response.list != undefined) {
                if (request.getData.selectionValues != undefined && request.getData.selectionValues[0] != undefined) {
                    response = response.list[0];
                    if (response == undefined) {
                        // the response list is not long enough
                        return undefined;
                    }
                    parents.push(response.node.val);
                    parentIndexes.push(request.getData.selection[0]);
                } else if (request.getData.selection != undefined && request.getData.selection[0] != undefined) {
                    response = response.list[request.getData.selection[0]];
                    if (response == undefined) {
                        return undefined;
                    }
                    parents.push(response.node.val);
                    parentIndexes.push(request.getData.selection[0]);
                } else if (additionalSelections != undefined && additionalSelections[nextSelectionIndex] != undefined) {
                    response = response.list[additionalSelections[nextSelectionIndex]];
                    if (response == undefined) {
                        return undefined;
                    }
                    parents.push(response.node.val);
                    parentIndexes.push(additionalSelections[nextSelectionIndex]);
                    ++nextSelectionIndex;
                } else {
                    if (i == path.length - 1) {
                        // this is the last element and we might be setting a list in which case we do not need to go further
                        break;
                    } else {
                        // we could not find out the index of the element below which the path points 
                        return undefined;
                    }
                }
            }
            
            requestList = request.getData.children;
            contentList = response.node.childVals;
        }
        
        if (request != undefined && response != undefined) {
            return createSetData(request, response, parents, parentIndexes, indexInList, value);
        } else {
            return undefined;
        }
    };
    
    function createSetData(request, response, parents, parentIndexes, indexInList, value) {
        var setData = {"setData": {}};
        setData.setData.source = request.getData.source;
        setData.setData.element = request.getData.element;
        setData.setData.ptcname = request.getData.ptcname;
        if (indexInList != undefined) {
            setData.setData.indxsInList = [indexInList];
        }
        setData.setData.content = value;
        if (request.getData.params != undefined) {
            setData.setData.params = mcopy(request.getData.params);
        }
        if (response.node != undefined) {
            setData.setData.tp = response.node.tp;
        } else if (response.list != undefined) {
            setData.setData.tp = response.list[0].node.tp;
        } else {
            return undefined
        }
        
        for (var i = 0; i < parents.length; ++i) {
            completeParams(setData.setData, parents, parentIndexes, i);
        }
        
        return setData;
    }
    
    function completeParams(setData, parents, parentIndexes, i) {
        var patternForParent = new RegExp("%Parent" + i + "%");
        var patternForParentIndex = new RegExp("%Parent" + i + "::idx%");
        setData.source = setData.source.replace(patternForParentIndex, parentIndexes[i]);
        setData.source = setData.source.replace(patternForParent, parents[i]);
        setData.element = setData.element.replace(patternForParentIndex, parentIndexes[i]);
        setData.element = setData.element.replace(patternForParent, parents[i]);
        if (setData.ptcname != undefined) {
            setData.ptcname = setData.ptcname.replace(patternForParentIndex, parentIndexes[i]);
            setData.ptcname = setData.ptcname.replace(patternForParent, parents[i]);
        }
        if (setData.params != undefined) {
            for (var j = 0; j < setData.params.length; ++j) {
                setData.params[j].paramValue = setData.params[j].paramValue.replace(patternForParentIndex, parentIndexes[i]);
                setData.params[j].paramValue = setData.params[j].paramValue.replace(patternForParent, parents[i]);
            }
        }
    }
}