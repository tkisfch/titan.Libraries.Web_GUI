// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function FileHandler() {
    "use strict";
    
    // disable cache (mainly for IE, it works all right with firefox and chrome)
    $.ajaxSetup({cache: false});
    
    var v_cache = {};
    var v_useCache = true;
    
    this.useCache = function(p_useCache) {
        v_useCache = p_useCache;
        v_cache = {};
    };
    
    this.putFile = function(url, p_data, callback) {
        $.ajax({
            url: url,
            type: 'PUT',
            success: function(data) { callback(true, data); },
            error: function(data) { callback(false, data); },
            data: p_data,
            dataType: 'text',
            contentType: 'text',
            timeout: 10000
        });
        
        if (v_useCache) {
            v_cache[url] = p_data;
        }
    };
    
    this.getDirectory = function(url, callback) {
        $.ajax({
            url: url,
            type: 'LSDIR',
            success: function(aData) {callback(JSON.parse(aData).fileList.sort());},
            error: function(aData) {callback(0);},
            data: "",
            dataType: 'text',
            contentType: 'text',
            timeout: 10000
        });
    };
    
    this.createDirectory = function(url, callback) {
        $.ajax({
            url: url,
            type: 'MKDIR',
            success: function(data) {callback(true, data);},
            error: function(data) {callback(false, data);},
            data: "",
            dataType: "text",
            timeout: 10000
        });
    };

    this.delDirectory = function(url, callback) {
        $.ajax({
            url: url,
            type: 'RMDIR',
            success: function(data) { callback(true, data); },
            error: function(data) { callback(false, data); },
            data: "",
            dataType: "text",
            timeout: 10000
        });
    };
    
    

    this.loadFile = function(url, callback) {
        if (v_useCache && v_cache[url] != undefined) {
            callback(true, v_cache[url]);
            return;
        }
        
        function success(data, textStatus, jqXHR) {
            if (v_useCache) {
                v_cache[url] = data;
            }
            callback(true, data);
        }
        
        function error(data, textStatus, jqXHR) {
            callback(false, "Loading: " + textStatus + " " + jqXHR);
        }
        
        $.ajax({
            url: url,
            type: 'GET',
            success: success,
            error: error,
            dataType: "text",
            data: "",
            timeout: 10000
        });
    };

    this.importJsFile = function(file, p_callback) {
        var v_callback = p_callback;
        function success(data, textStatus, jqXHR) {
            v_callback(true);
        }
        
        function error(data, textStatus, jqXHR) {
            v_callback(false, "Loading: " + textStatus + " " + jqXHR + " " + file);
        }
        
        $.ajax({
            url: file,
            dataType: "script",
            success: success,
            error: error,
            timeout: 10000
        });
    };
    
    this.loadCss = function(hmtlContentId, cssFile, callback) {
        $("#" + hmtlContentId).load(cssFile, callback);
    };
    
    this.loadCssFiles = function(cssFiles, id) {
        for (var i = 0; i < cssFiles.length; ++i) {
            // html ids should not contain / and . so we replace them with _
            var cssId = cssFiles[i].replace(/[\/\.]/gi, "_");
            $("#" + id).prepend('<style id="' + cssId + '" type="text/css"></style>');
            this.loadCss(cssId, cssFiles[i]);
        }
    };
}