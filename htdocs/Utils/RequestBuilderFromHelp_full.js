// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function DSHelpToRequest_full(aHelp)
{
    //mlog("aHelp: ", aHelp);
    //mlog("aHelp JSON: ",JSON.stringify(aHelp, null, 2));
    //mlog("Requests.ExecCtrl: ", Requests.ExecCtrl);
    var aParentIndexes = [];
    //for (var i =3; i < 4; i++) // currently only ExecCtrl
    var lHelp=JSON.parse(JSON.stringify(aHelp));
    if (lHelp.sources)
    {
        for (var i = 0; i < lHelp.sources.length; i++)
        {
            PreprocessHelpList(lHelp.sources[i].dataElements, lHelp.sources[i].source, lHelp.sources[i].dataElements, aParentIndexes);
        }
    }
    var lReq = [];
    aParentIndexes = [];
    if (lHelp.sources)
    {
        for (var i = 0; i < lHelp.sources.length; i++)
        {
            HelpListToRequestList(lReq, lHelp.sources[i].dataElements, lHelp.sources[i].source, lHelp.sources[i].dataElements, aParentIndexes);
        }
    }
    //mlog("lReq: ", lReq);
    //mlog("lReq JSON: ",JSON.stringify(lReq, null, 2));
    return lReq;
    //return JSON.stringify(lReq, null, 2);
}

//                          children                 root          index of child at current level
function PreprocessHelpList(aHelpList, aSourceName, aHelpListTop, aParentIndexes)
{
    var tempParentIndexes = JSON.stringify(aParentIndexes, null, 2); // TODO replace it
    for (var i = 0; i < aHelpList.length; i++)
    {
        aParentIndexes = JSON.parse(tempParentIndexes);
        if (aHelpList[i].children != undefined)
        {
            aParentIndexes[aParentIndexes.length] = i;
            PreprocessHelpList(aHelpList[i].children, aSourceName, aHelpListTop, aParentIndexes);
        }

        CalcParamsOrInsertParents(aHelpList[i].dataElement, aHelpListTop, aParentIndexes, i,false);
    }
}

function HelpListToRequestList(aOutReq, aHelpList, aSourceName, aHelpListTop, aParentIndexes)
{
    var tempParentIndexes = JSON.stringify(aParentIndexes, null, 2); // TODO replace it
    for (var i = 0; i < aHelpList.length; i++)
    {
        aParentIndexes = JSON.parse(tempParentIndexes);
        var idx = aOutReq.length;
        var lReq = [];
        if (aHelpList[i].children != undefined)
        {
            aParentIndexes[aParentIndexes.length] = i;
            HelpListToRequestList(lReq, aHelpList[i].children, aSourceName, aHelpListTop, aParentIndexes);
        }

        aOutReq[idx] = {};
        aOutReq[idx].getData = {
            source: aSourceName,
            element: aHelpList[i].dataElement.name,
            selection: null,
            params: CalcParamsOrInsertParents(aHelpList[i].dataElement, aHelpListTop, aParentIndexes, i, true),
            children: lReq
        };
    }
}

function CalcParamsOrInsertParents(aDataElement, aHelpListTop, aParentIndexes, aCurrentIndex, aCalcParams)
{
    //mlog("*** CalcParamsOrInsertParents processing: ", aDataElement);
    var lParameters = [];
    var lParams = [];

    if (aDataElement.parameters != undefined)
    {
        lParameters = aDataElement.parameters;
        for (var i = 0; i < lParameters.length; i++)
        {
            //mlog("processing parameter #",JSON.parse(JSON.stringify(i)),": ", JSON.parse(JSON.stringify(lParameters[i].name)));
            lParams[i]= {};
            lParams[i].paramName = lParameters[i].name;

            if (lParameters[i].typeDescriptor.reference != undefined)
            {
                var lLevel=0;
                //if (aCalcParams == true)
                //    mlog("\\\\\\\\\\searching for", lParameters[i].typeDescriptor.reference.typeName);
                var lNestingLevel = GetReferenceNestingLevel(lParameters[i].typeDescriptor.reference.typeName, aHelpListTop, lLevel, aParentIndexes);
                if (aCalcParams === false)
                {
                    if (lNestingLevel === -1)
                    {
                        //mlog("ELEMENT NOT FOUND IN PARENT TREE: ",lParameters[i].typeDescriptor.reference.typeName);
                        lLevel = 0;
                        var lParentIndexes = [];
                        //mlog("\\\\\\\\\\searching globally for", lParameters[i].typeDescriptor.reference.typeName);
                        //mlog("******searching in: ", aHelpListTop);
                        lNestingLevel = GetReferenceNestingLevelFromFullBranch(lParameters[i].typeDescriptor.reference.typeName, aHelpListTop, lLevel, lParentIndexes);
                        //mlog("*****lParentIndexes calculated:", lParentIndexes, " nesting level: ", lNestingLevel);
                        if (lNestingLevel === -1)
                        {
                            mlog("!!!!THIS SHOULD NEVER HAPPEN2!!!!  dataelement: ", aDataElement);
                        }
                        else
                        {
                            InsertParents(aCurrentIndex,aParentIndexes,lParentIndexes,aHelpListTop);
                        }
                    }
                }
                else
                {
                    if (lNestingLevel === -1)
                    {
                        mlog("!!!!THIS SHOULD NEVER HAPPEN!!!!  dataelement: ", aDataElement);
                    }
                    lParams[i].paramValue= "%Parent"+lNestingLevel+"%";
                    //mlog("paramValue of ", lParams[i].paramName, ": ",lParams[i].paramValue)
                }
            }
            if (lParameters[i].typeDescriptor.valueType != undefined && aCalcParams === true)
            {
                lParams[i].paramValue= lParameters[i].exampleValue;
            }
        }
    }
    return lParams;
}

function GetCurrentElement(aCurrentIndex, aParentIndexesCurrent, aHelpCurrent, aLevel)
{
    if (aParentIndexesCurrent.length === aLevel)
    {
        return aHelpCurrent[aCurrentIndex];
    }
    ++aLevel;
    return GetCurrentElement(aCurrentIndex, aParentIndexesCurrent, aHelpCurrent[aParentIndexesCurrent[aLevel-1]].children, aLevel);
}

function SetCurrentElement(aCurrentIndex, aParentIndexesCurrent, aHelpCurrent, aLevel, aElementToSet)
{
    if (aParentIndexesCurrent.length === aLevel)
    {
        aHelpCurrent[aCurrentIndex] = aElementToSet;
    }
    else
    {
        ++aLevel;
        SetCurrentElement(aCurrentIndex, aParentIndexesCurrent, aHelpCurrent[aParentIndexesCurrent[aLevel-1]].children, aLevel, aElementToSet);
    }
}

function InsertParents(aCurrentIndex, aParentIndexesCurrent, aParentIndexesSource, aHelpListTop)
{
    //mlog("*********InsertParents*************");
    //mlog("aCurrentIndex: ", aCurrentIndex)
    //mlog("aParentIndexesCurrent: ", aParentIndexesCurrent)
    //mlog("aParentIndexesSource: ", aParentIndexesSource)
    //mlog("aHelpListTop: ", JSON.parse(JSON.stringify(aHelpListTop,null,2)));

    var lLevel = 0;
    var lCurrentElement = GetCurrentElement(aCurrentIndex, aParentIndexesCurrent, aHelpListTop, lLevel);
    //mlog("current element: ", lCurrentElement);


    var lSourceBranch = [];
    lLevel = 0;
    GetSourceBranch(aParentIndexesSource, aHelpListTop, lSourceBranch, lLevel, lCurrentElement);
    //mlog("sourceBranch: ", lSourceBranch);
    //lCurrentElement = lSourceBranch[0];
    lLevel = 0;
    SetCurrentElement(aCurrentIndex, aParentIndexesCurrent, aHelpListTop, lLevel, lSourceBranch[0]);
    //mlog("aHelpListTopAfter: ", JSON.parse(JSON.stringify(aHelpListTop)));

}

function GetSourceBranch(aParentIndexesSource, aHelpListTop, aSourceBranch, aLevel, aCurrentElement)
{
    if (aParentIndexesSource.length === aLevel)
    {
        aSourceBranch[0] = aCurrentElement;
    }
    else
    {
        aSourceBranch[0] = JSON.parse(JSON.stringify(aHelpListTop[aParentIndexesSource[aLevel]]));
        aSourceBranch[0].children = [];
        ++aLevel;
        GetSourceBranch(aParentIndexesSource, aHelpListTop[aParentIndexesSource[aLevel-1]].children, aSourceBranch[0].children, aLevel, aCurrentElement);
    }
}

function GetReferenceNestingLevel(aTypeName, aCurrentBranch, aLevel, aParentIndexes)
{
    //mlog("\\\\\\\\\\\\ searchin' in: ", aCurrentBranch, aParentIndexes);
    var lRetVal=-1;
    var i = aParentIndexes[aLevel];

    if (aCurrentBranch[i] != undefined)
    {
        //mlog("---------aCurrentBranch[i].dataElement.typeDescriptor.typeName / aTypeName", aCurrentBranch[i].dataElement.typeDescriptor.typeName, "/", aTypeName, aLevel);
        if (aCurrentBranch[i].dataElement.typeDescriptor != undefined && aCurrentBranch[i].dataElement.typeDescriptor.typeName === aTypeName)
        {
            lRetVal = aLevel;
        }
        else if (aCurrentBranch[i].children != undefined)
        {
            lRetVal = GetReferenceNestingLevel(aTypeName, aCurrentBranch[i].children, aLevel+1, aParentIndexes);
        }
    }
    //mlog("///////////returning: ", lRetVal);
    return lRetVal;
}

function GetReferenceNestingLevelFromFullBranch(aTypeName, aCurrentBranch, aLevel, aParentIndexes)
{
    //mlog("**********************GetReferenceNestingLevelFromFullBranch*******************")
    //mlog("aCurrentBranch: ",aCurrentBranch);
    var lRetVal = -1;
    for (var i = 0; i < aCurrentBranch.length; i++)
    {
        aParentIndexes[aLevel] = i;
        if (aCurrentBranch[i] != undefined)
        {
            //mlog("currentBranch[", i,"]=",aCurrentBranch[i]);
            //mlog("-------typeName: ",aCurrentBranch[i].dataElement.typeDescriptor.typeName, ":", aTypeName);
            if (aCurrentBranch[i].dataElement.typeDescriptor != undefined && aCurrentBranch[i].dataElement.typeDescriptor.typeName === aTypeName)
            {
                lRetVal = aLevel;
                return lRetVal;
            }
            if (aCurrentBranch[i].children != undefined)
            {
                ++aLevel;
                lRetVal = GetReferenceNestingLevelFromFullBranch(aTypeName, aCurrentBranch[i].children, aLevel, aParentIndexes);
                if (lRetVal !== -1)
                {
                    return lRetVal;
                } else {
                  --aLevel;
                  aParentIndexes.pop();
                }
            }
        }
    }
    return lRetVal;
}
