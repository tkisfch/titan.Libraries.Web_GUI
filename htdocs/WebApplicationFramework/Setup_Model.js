// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CSetup_Model(p_fileHandler) {
    "use strict";
    
    var v_setupDirectory = "";
    var v_fileHandler = p_fileHandler;
    var v_setup = {};

    this.setSetupDirectory = function(directory) {
        v_setupDirectory = directory;
    };

    this.newSetup = function() {
        v_setup.request = new JsonLoader(undefined, v_fileHandler, []);
        v_setup.viewmodels = new JsonLoader(undefined, v_fileHandler, []);
        v_setup.views = new JsonLoader(undefined, v_fileHandler, []);
        v_setup.imports = new JsonLoader(undefined, v_fileHandler, []);
        v_setup.desktop = new JsonLoader(undefined, v_fileHandler, {});
        v_setup.html = new FileLoader(undefined, v_fileHandler, '<div id="div_id"></div>');
        v_setup.css = new FileLoader(undefined, v_fileHandler);
        v_setup.name = undefined;
        return v_setup;
    };

    this.loadSetup = function(setupName, callback, doNotResolveImports, setupParams) {
        function importsResolved(ok) {
            callback(ok, v_setup, setupName)
        }

        function setupLoaded(ok, setup, setupName) {
            if (doNotResolveImports == undefined || doNotResolveImports == false) {
                resolveImports(importsResolved)
            } else {
                callback(ok, setup, setupName);
            }
        }
        loadSetupFromFiles(setupName, v_setup, setupParams, setupLoaded);
    };

    function loadSetupFromFiles(setupName, setup, setupParams, callback) {
        var setupLocation = v_setupDirectory + "/" + setupName;
        function jsonImported(ok, data) {
            if (setupParams != undefined && Object.keys(setupParams).length > 0) {
                replaceSetupParams(setup);
            }
            if (callback != undefined) {
                callback(ok, setup, setupName);
            }
        }

        setup.request = new JsonLoader(setupLocation + "/Request.json", v_fileHandler, []);
        setup.viewmodels = new JsonLoader(setupLocation + "/ViewModelInstances.json", v_fileHandler, []);
        setup.views = new JsonLoader(setupLocation + "/ViewInstances.json", v_fileHandler, []);
        setup.imports = new JsonLoader(setupLocation + "/Imports.json", v_fileHandler, []);
        setup.desktop = new JsonLoader(setupLocation + "/Desktop.json", v_fileHandler, {});
        setup.html = new FileLoader(setupLocation + "/Setup.html", v_fileHandler);
        setup.css = new FileLoader(setupLocation + "/Setup.css", v_fileHandler);
        setup.name = setupName;
        setup.setupParams = setupParams;

        var taskList = new TaskList([setup.request, setup.viewmodels, setup.views, setup.imports, setup.desktop, setup.html, setup.css], jsonImported);
        taskList.taskOperation();
    }

    this.getSetup = function() {
        return v_setup;
    };

    this.saveSetup = function(callback) {
        var taskList = new TaskList([
            new JsonSaveTask(v_setup.request, undefined),
            new JsonSaveTask(v_setup.viewmodels, undefined),
            new JsonSaveTask(v_setup.views, undefined),
            new JsonSaveTask(v_setup.imports, undefined),
            new JsonSaveTask(v_setup.desktop, undefined),
            new FileSaveTask(v_setup.html, undefined, undefined),
            new FileSaveTask(v_setup.css, undefined, undefined),
        ], callback);
        taskList.taskOperation();
    };

    this.saveSetupAs = function(setupName, callback) {
        v_setup.name = setupName;
        var directory = v_setupDirectory + "/" + setupName;

        function directoryCreated(success) {
            if (!success) {
                callback(false);
                return;
            }

            var taskList = new TaskList([
                new JsonSaveTask(v_setup.request, directory + "/Request.json"),
                new JsonSaveTask(v_setup.viewmodels, directory + "/ViewModelInstances.json"),
                new JsonSaveTask(v_setup.views, directory + "/ViewInstances.json"),
                new JsonSaveTask(v_setup.imports, directory + "/Imports.json"),
                new JsonSaveTask(v_setup.desktop, directory + "/Desktop.json"),
                new FileSaveTask(v_setup.html, undefined, directory + "/Setup.html"),
                new FileSaveTask(v_setup.css, undefined, directory + "/Setup.css"),
            ], callback);
            taskList.taskOperation();
        }

        function gotExistance(exists) {
            if (exists) {
                directoryCreated(true);
            } else {
                v_fileHandler.createDirectory(directory, directoryCreated);
            }
        }

        this.setupExists(setupName, gotExistance);
    };

    this.deleteSetup = function(p_setupName, callback) {
        var setupName = v_setupDirectory + "/" + p_setupName;
        v_fileHandler.delDirectory(setupName, callback);
    };

    this.setupExists = function(p_setupName, callback) {
        var setupLoaction = v_setupDirectory + "/" + p_setupName;
        function setupsListed(setupDirs) {
            for (var i = 0; i < setupDirs.length; ++i) {
                if (setupLoaction === setupDirs[i].fileName) {
                    callback(true);
                    return;
                }
            }
            callback(false);
        }

        listSetupDirs(setupsListed);
    };

    this.listSetups = function(a_callback) {
        var callback = a_callback;
        function setupsDirectoryLoaded(a_directories) {
            var result = [];
            for (var i = 0; i < a_directories.length; ++i) {
                result.push(a_directories[i].fileName.substring(a_directories[i].fileName.lastIndexOf("/") + 1));
            }
            result.sort();
            callback(result);
        }
        listSetupDirs(setupsDirectoryLoaded);
    };

    this.globalSetupSearch = function(input, choices, p_callback) {
        input = input.toLowerCase();
        var indexes = [];
        var tasks = [];

        function SearchTask(fileName, index) {
            var v_fileName = fileName;
            var v_index = index;

            this.taskOperation = function(callback) {
                v_fileHandler.loadFile(v_fileName, function(ok, data) {
                    if (data.toLowerCase().indexOf(input) != -1 && indexes.indexOf(v_index) == -1) {
                        indexes.push(v_index);
                    }
                    callback(true);
                });
            }
        }

        function searchComplete() {
            p_callback(indexes);
        }

        for (var i = 0; i < choices.length; ++i) {
            tasks.push(new SearchTask(v_setupDirectory + "/" + choices[i].value + "/Request.json", i));
            tasks.push(new SearchTask(v_setupDirectory + "/" + choices[i].value + "/ViewModelInstances.json", i));
            tasks.push(new SearchTask(v_setupDirectory + "/" + choices[i].value + "/ViewInstances.json", i));
            tasks.push(new SearchTask(v_setupDirectory + "/" + choices[i].value + "/Imports.json", i));
            tasks.push(new SearchTask(v_setupDirectory + "/" + choices[i].value + "/Setup.html", i));
            tasks.push(new SearchTask(v_setupDirectory + "/" + choices[i].value + "/Setup.css", i));
        }

        var taskList = new TaskList(tasks, searchComplete);
        taskList.taskOperation();
    };

    this.setAPI = function(a_api) {
        v_fileHandler = a_api;
    };

    function replaceSetupParams(setup) {
        var setupParts = ["request", "viewmodels", "views", "imports"];
        for (var i = 0; i < setupParts.length; ++i) {
            var data = JSON.stringify(setup[setupParts[i]].getData());
            for (var key in setup.setupParams) {
                data = data.replace(key, setup.setupParams[key]);
            }
            setup[setupParts[i]].setData(JSON.parse(data));
        }

        setupParts = ["html", "css"];
        for (var i = 0; i < setupParts.length; ++i) {
            var data = setup[setupParts[i]].getData();
            for (var key in setup.setupParams) {
                data = data.replace(key, setup.setupParams[key]);
            }
            setup[setupParts[i]].setData(data);
        }
    }

    function listSetupDirs(a_callback) {
        v_fileHandler.getDirectory(v_setupDirectory, a_callback);
    }

    function resolveImports(a_callback) {
        var importResolver = new CImportResolver(v_setup, loadSetupFromFiles);
        importResolver.resolveImports(a_callback);
    }
}

function CImportResolver(a_setup, a_setupLoaderFunction) {
    var v_setup = a_setup;

    var loadSetupFromFiles = a_setupLoaderFunction;

    this.resolveImports = function(a_callback) {
        mergeSetups(v_setup.imports.getData(), [v_setup.name], a_callback);
    };

    function mergeSetups(importList, importedSetupsPath, callback) {
        if (importList.length == 0) {
            callback(true);
        } else {
            var mergeTasks = [];
            for (var i = 0; i < importList.length; ++i) {
                if (importedSetupsPath.indexOf(importList[i].setupName) != -1) {
                    alert("Circular importation of setups detected: " + importedSetupsPath + " " + importList[i].setupName);
                } else {
                    var viewIdPrefixes = "i" + importedSetupsPath.length + "_" + i + "_";
                    mergeTasks.push(new MergeTask(importList[i].setupName, importedSetupsPath, viewIdPrefixes, importList[i].parentID, importList[i].requetsPath, importList[i].setupParams));
                }
            }
            var taskList = new TaskList(mergeTasks, callback);
            taskList.taskOperation();
        }
    }

    function MergeTask(setupName, importedSetupsPath, viewIdPrefixes, parentID, requetsPath, setupParams) {
        var name = setupName;
        var path = mcopy(importedSetupsPath);
        path.push(name);
        var params = setupParams;
        var prefix = viewIdPrefixes;
        var idToCreate = parentID;

        this.taskOperation = function(callback) {
            function setupLoaded(ok, setup, setupName) {
                if (ok) {
                    mergeSetup(setup, prefix, idToCreate, requetsPath);
                    mergeSetups(setup.imports.getData(), path, callback);
                } else {
                    alert("Failed to load imported setup: " + name);
                    callback(true);
                }
            }

            loadSetupFromFiles(name, {}, params, setupLoaded);
        }
    }

    function mergeSetup(setup, viewIdPrefixes, parentID, requetsPath) {
        var viewModelIndexOffset = v_setup.viewmodels.getData().length;
        var requestPathPrefix = [];
        if (requetsPath != undefined) {
            requestPathPrefix = requetsPath;
        }
        var requestPathOffset = mergeRequest(setup.request.getData(), requestPathPrefix);
        mergeViewmodels(setup.viewmodels.getData(), requestPathPrefix, requestPathOffset);
        updateImportRequestPaths(setup.imports.getData(), requestPathPrefix, requestPathOffset);
        addPrefixToIds(setup, viewIdPrefixes);
        addViewForHtml(setup, parentID);
        mergeViews(setup.views.getData(), viewModelIndexOffset);
        mergeCss(setup.css.getData());
    }

    function mergeRequest(request, path) {
        var requestsToExpand = v_setup.request.getData();
        for (var i = 0; i < path.length; ++i) {
            if (requestsToExpand[path[i]].getData.children == undefined) {
                requestsToExpand[path[i]].getData.children = [];
            }
            requestsToExpand = requestsToExpand[path[i]].getData.children;
        }
        var requestPathOffset = requestsToExpand.length;

        for (var i = 0; i < request.length; ++i) {
            requestsToExpand.push(request[i]);
        }

        return requestPathOffset;
    }

    function mergeViewmodels(viewmodels, prefix, offset) {
        var origViewmodels = v_setup.viewmodels.getData();
        for (var i = 0; i < viewmodels.length; ++i) {
            for (var j = 0; j < viewmodels[i].dataPathList.length; ++j) {
                updatePath(viewmodels[i].dataPathList[j], prefix, offset);
            }
            for (var j = 0; j < viewmodels[i].selectionToControlPathList.length; ++j) {
                updatePath(viewmodels[i].selectionToControlPathList[j], prefix, offset);
            }
            origViewmodels.push(viewmodels[i]);
        }
    }

    function updatePath(path, prefix, offset) {
        path[0] += offset;
        for (var i = prefix.length - 1; i >= 0; --i) {
            path.unshift(prefix[i]);
        }
    }

    function updateImportRequestPaths(imports, prefix, offset) {
        for (var i = 0; i < imports.length; ++i) {
            if (imports[i].requestPath != undefined) {
                updatePath(imports[i].requestPath, prefix, offset);
            }
        }
    }

    function addPrefixToIds(setup, viewIdPrefixes) {
        var views = setup.views.getData();
        var imports = setup.imports.getData();
        var html = setup.html.getData();

        for (var i = 0; i < views.length; ++i) {
            views[i].parentID = viewIdPrefixes + views[i].parentID;
            for (var j = 0; j < views[i].idsCreating.length; ++j) {
                views[i].idsCreating[j] = viewIdPrefixes + views[i].idsCreating[j];
            }
        }

        for (var i = 0; i < imports.length; ++i) {
            imports[i].parentID = viewIdPrefixes + imports[i].parentID;
        }

        html = html.replace(/id="([^"]+)"/gm, function(matchedString, matchedPart, index) {
            return 'id="' + viewIdPrefixes + matchedPart + '"';
        });
        setup.html.setData(html);
    }

    function addViewForHtml(setup, parentID) {
        var html = setup.html.getData();
        var idsCreating = [];

        var pattern = new RegExp(/id="([^"]+)"/gm);
        var str = html;
        var match = pattern.exec(str);
        while (match != undefined) {
            idsCreating.push(match[1]);
            match = pattern.exec(str);
        }

        var viewDescriptor = {
            "class": "CView_Div",
            "viewModelIndexes": [],
            "parentID": parentID,
            "idsCreating": idsCreating,
            "customData": {
                "text": html
            }
        }
        setup.views.getData().unshift(viewDescriptor);
    }

    function mergeViews(views, offset) {
        var origViews = v_setup.views.getData();
        for (var i = 0; i < views.length; ++i) {
            for (var j = 0; j < views[i].viewModelIndexes.length; ++j) {
                views[i].viewModelIndexes[j] += offset;
            }
            origViews.push(views[i]);
        }
    }

    function mergeCss(css) {
        v_setup.css.setData(v_setup.css.getData() + "\n" + css);
    }
}