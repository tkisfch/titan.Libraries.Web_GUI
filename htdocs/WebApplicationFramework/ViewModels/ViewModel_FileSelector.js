// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_FileSelector(p_viewmodel, p_options) {
    "use strict";

    var v_fileHandler = p_viewmodel.getFileHandler();
    var v_binder;
    var v_options = p_options;
    
    var currentDir;
    var currentDirContents;
    var selection;
    
    var undoStack = [];
    var redoStack = [];
    var currentFilter;
    
    var v_this = this;
    
    if (v_options == undefined) {
        v_options = {};
    }
    
    if (v_options["targetType"] == undefined) {
        v_options["targetType"] = "file";
    }
    
    if (v_options["fileTypeList"] == undefined || v_options["fileTypeList"].length == 0) {
        v_options["fileTypeList"] = ["*.*", "*.js", "*.html", "*.css", "*.cfg"];
    }
    
    if (v_options["defaultDir"] == undefined) {
        v_options["defaultDir"] = "/";
    }
    
    if (v_options["multipleSelection"] == undefined) {
        v_options["multipleSelection"] = false;
    }
    
    currentDir = v_options["defaultDir"];
    currentFilter = v_options["fileTypeList"][0];
    
    /** public functions - interface for parent */ 

    this.setSelectionToControl = function() {};
    this.setReponseDataPath = function() {};
    this.setBinder = function(p_binder) {
        v_binder = p_binder;
    };
    
    /** public functions - interface for views */ 
    
    this.filesSelected = function() {
        alert(JSON.stringify(selection, null, 2));
    };
    
    this.fileSelectionCanceled = function() {
        alert("Selection canceled");
    };

    this.getCurrentDirElements = function() {
        var content = [{
            "type" : "-d",
            "name" : "..",
            "displayName" : "..",
            "size" : 0,
            "timestamp" : 0,
            "id" : -1
        }];
        
        var filter = currentFilter;
        filter = filter.replace(".*", "");
        filter = filter.replace("*", "");
        
        for (var i = 0; i < currentDirContents.length; ++i) {
            var name = currentDirContents[i]["fileName"];
            if (currentDirContents[i]["contentType"].endsWith("d") || name.endsWith(filter)) {
                var displayName = getLastName(name);
                content.push({
                    "type" : currentDirContents[i]["contentType"],
                    "name" : name,
                    "displayName" : displayName,
                    "size" : currentDirContents[i]["size"],
                    "timestamp" : currentDirContents[i]["timestamp"],
                    "id" : i
                });
            }
        }
        
        return content;
    };
    
    this.getCurrentDir = function() {
        return currentDir;
    };
    
    this.action = function(id, name, type, refresh) {
        if (type == "doubleclick") {
            if (id == -1 || currentDirContents[id]["contentType"] == "-d" || currentDirContents[id]["contentType"] == "ld") {
                changeDir(name, refresh);
            } else {
                select(currentDirContents[id], name, false, refresh);
            }
        } else if (type == "click" && id != -1) {
            select(currentDirContents[id], name, false, refresh);
        } else if (type == "ctrlclick" && id != -1) {
            select(currentDirContents[id], name, true, refresh);
        }
    };
    

    
    this.back = function(refresh) {
        if (!v_this.allowBack()) {
            return;
        }
        
        redoStack.push(currentDir);
        currentDir = undoStack.pop();
        
        v_fileHandler.getDirectory(currentDir, function(content) {
            currentDirContents = content;
            selection = [];
            refresh(true);
        });
    };
    
    this.forward = function(refresh) {
        if (!v_this.allowForward()) {
            return;
        }
        
        undoStack.push(currentDir);
        currentDir = redoStack.pop();
        
        v_fileHandler.getDirectory(currentDir, function(content) {
            currentDirContents = content;
            selection = [];
            refresh(true);
        });
    }
    
    this.allowBack = function() {
        return undoStack.length != 0;
    };
    
    this.allowForward = function() {
        return redoStack.length != 0;
    };
    
    this.home = function(refresh) {
        undoStack.push(currentDir);
        redoStack = [];
        currentDir = v_options["defaultDir"];
        
        v_fileHandler.getDirectory(currentDir, function(content) {
            currentDirContents = content;
            selection = [];
            refresh(true);
        });
    };
    
    this.createDirectory = function(name, refresh) {
        var path;
        if (currentDir[currentDir.length - 1] == "/") {
            path = currentDir + name;
        } else {
            path = currentDir + "/" + name;
        }
        
        v_fileHandler.createDirectory(path, function(ok) {
            if (ok) {
                v_fileHandler.getDirectory(currentDir, function(content) {
                    currentDirContents = content;
                    selection = [];
                    refresh(true);
                });
            } else {
                alert("Error creating directory: " + path);
            }
        });
    };

    this.getSelection = function() {
        return selection;
    };
    
    this.setSelectionManually = function(name) {
        var path;
        if (currentDir[currentDir.length - 1] == "/") {
            path = currentDir + name;
        } else {
            path = currentDir + "/" + name;
        }
        
        var obj = {
            "text": name,
            "value" : path
        }
        
        selection = [obj];
    };
    
    this.getFileTypeList = function() {
        var filters = v_options["fileTypeList"];
        var list = [];
        for (var i = 0; i < filters.length; ++i) {
            list.push({
                "value": filters[i],
                "text": filters[i]
            })
        }
        
        return list;
    }
    
    this.setFilter = function(text, refresh) {
        currentFilter = text;
        selection = [];
        refresh(true);
    };
    
    this.exists = function(path, callback) {
        if (path.length > 1 && path[path.length - 1] == "/") {
            path = path.substr(0, path.length - 1);
        }
        var name = getLastName(path);
        if (name.length == 0 || name == "..") {
            callback(true);
        } else {
            path = path.substr(0, path.length - name.length);
            v_fileHandler.getDirectory(path, function(content) {
                var found = false;
                for (var i = 0; i < content.length; ++i) {
                    if (content[i]["fileName"].endsWith(name)) {
                        found = true;
                        break;
                    }
                }
                callback(found);
            });
        }
    }
    
    this.loadFile = function(url, callback) {
        return v_fileHandler.loadFile(url, callback);
    };
    
    /** private functions */
    
    function changeDir(name, refresh) {
        undoStack.push(currentDir);
        redoStack = [];
        
        if (name == "..") {
            if (currentDir != "/" && getLastName(currentDir) != "..") {
                currentDir = currentDir.substr(0, currentDir.lastIndexOf("/"));
                if (currentDir.length == 0 || currentDir[currentDir.length -1] == ".") {
                    currentDir += "/";
                }
            } else {
                if (currentDir == "/") {
                    currentDir = ".." + currentDir;
                } else {
                    currentDir = "../" + currentDir;
                }
            }
        } else {
            if (currentDir[currentDir.length - 1] == "/") {
                currentDir += name.substr(name.lastIndexOf("/") + 1);
            } else {
                currentDir += "/" + name.substr(name.lastIndexOf("/") + 1);
            }
        }
        
        v_fileHandler.getDirectory(currentDir, function(content) {
            currentDirContents = content;
            selection = [];
            refresh(true);
        });
    }
    
    function getLastName(dir) {
        var dirs = dir.split("/");
        if (dir[dir.length - 1] == "/") {
            return dirs[dirs.length - 2];
        } else {
            return dirs[dirs.length - 1];
        }
    }
    
    function select(content, name, multiple, refresh) {
        if (name == "..") {
            return;
        }
        
        var targetType = v_options["targetType"];
        var multipleSelectionAllowed = v_options["multipleSelection"];
        
        var selectionObject = getSelectionObject(content);
        
        if (
            (targetType == "file" && (content["contentType"] == "-f" || content["contentType"] == "lf")) ||
            (targetType == "dir" && (content["contentType"] == "-d" || content["contentType"] == "ld"))
        ) {
            if (multiple && multipleSelectionAllowed) {
                multipleSelection(selectionObject, refresh);
            } else {
                singleSelection(selectionObject, refresh);
            }
        } else if (!multiple) {
            selection = [];
            refresh();
        }
    }
    
    function getSelectionObject(content) {
        var value = content["fileName"];
        var text = getLastName(value);
        return {
            "text" : text,
            "value" : value
        };
    }
    
    function singleSelection(selectionObject, refresh) {
        selection = [selectionObject];
        refresh();
    }
    
    function multipleSelection(selectionObject, refresh) {
        var found = -1;
        for (var i = 0; i < selection.length; ++i) {
            if (selection[i].value == selectionObject.value) {
                found = i;
                break;
            }
        }
        if (found != -1) {
            selection.splice(i, 1);
        } else {
            selection.push(selectionObject);
        }
        refresh();
    }
}

CViewModel_FileSelector.getHelp = function() {
    return "A file selector viewmodel. Override its filesSelected and fileSelectionCanceled functions to create something useful.";
};

CViewModel_FileSelector.providesInterface = function() {
    return ["filesSelected", "fileSelectionCanceled", "getCurrentDirElements", "getCurrentDir", "action",
        "back", "forward", "allowBack", "allowForward", "home",
        "createDirectory", "getSelection", "setSelectionManually", "getFileTypeList", "setFilter", "exists"];
};

CViewModel_FileSelector.getCustomDataSchema = function() {
    return {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CViewModel_FileSelector",
        "type": "object",
        "properties": {
            "targetType": {
                "description": "The type of selection",
                "type": "string",
                "enum": ["file", "dir"]
            },
            "fileTypeList": {
                "description": "The file types that can be used to filter the shown results.",
                "type": "array",
                "format": "table",
                "items": {
                    "type": "string",
                    "title": "file type",
                    "default": "*.*",
                    "pattern": "\\*\\.[A-Za-z0-9*]+$"
                },
                "uniqueItems": true,
                "default": ["*.*"]
            },
            "defaultDir": {
                "description": "The default directory that will first appear and that can be navigated to with the home button.",
                "type": "string",
                "default": "/"
            },
            "multipleSelection": {
                "description": "Whether we allow multiple selection (default false).",
                "type": "boolean",
                "format": "checkbox",
                "default": true
            }
        },
        "additionalProperties": false
    };
};

CViewModel_FileSelector.expectsConnection = function() {
    return {
        "dataConnections": [],
        "selectionConnections": []
    };
};

//# sourceURL=WebApplicationFramework\ViewModels\FileSelector_ViewModel.js
