// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_Aligner(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";

    /** constructor */

    var v_mainId = p_mainId;
    var v_parentId = p_parentId;
    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_Aligner)[0];
    var v_conditionViewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_Aligner)[1];
    var v_customData = p_data;

    var v_subTotalSize;

    var v_this = this;

    /** public functions */

    this.applicationCreated = function() {
        var orientation = getOrientation();
        if (!v_customData.existing) {
            $("#" + v_parentId).append(getHtml(orientation));
        }
        setupCallbacks(orientation);
    };

    this.refresh = function() {
        ViewUtils.checkVisibility(v_conditionViewmodel, v_mainId);
    };

    /** private functions */

    function getOrientation() {
        if (v_customData.orientation != undefined && v_customData.orientation.toLowerCase() == "vertical") {
            return "Vertical";
        } else {
            return "Horizontal";
        }
    }

    function getHtml(orientation) {
        var html = '<div class="Aligner" id="' + v_mainId + '">';
        var lengths = getLengths();
        for (var i = 0; i < v_customData.idsCreating.length; ++i) {
            html += getHtmlOfContent(i, orientation, lengths);
        }
        html += '</div>';
        return html;
    }

    function getHtmlOfContent(contentIndex, orientation, lengths) {
        var html = '';
        html += '<div id="' + v_customData.idsCreating[contentIndex] + '" class="Aligner_' + orientation + '_Content"';
        if (orientation == "Vertical" && lengths[contentIndex] != "") {
            html += ' style="height: ' + lengths[contentIndex] + ';"';
        } else if (lengths[contentIndex] != "") {
            html += ' style="width: ' + lengths[contentIndex] + ';"';
        }
        html += '></div>';
        return html;
    }

    /** protected functions */

    this.getLenghtsFromViewmodel = function() {
        if (v_viewmodel != undefined && v_viewmodel.getChildSizes != undefined) {
            return v_viewmodel.getChildSizes(v_customData.idsCreating);;
        } else {
            return undefined;
        }
    };

    /** private functions */

    function getLengths() {
        var lengths;
        if (v_customData.flexCalculation == true || v_customData.resizable == true) {
            lengths = v_this.getLenghtsFromViewmodel();
            if (lengths == undefined) {
                lengths = [];
                var unitPercent = (100 / v_customData.idsCreating.length) + "%";
                for (var i = 0; i < v_customData.idsCreating.length; ++i) {
                    lengths.push(unitPercent);
                }
            }
            return lengths;
        } else {
            lengths = [];
            for (var i = 0; i < v_customData.idsCreating.length; ++i) {
                lengths.push("");
            }

        }
        return lengths;
    };

    function setupCallbacks(orientation) {
        if (v_customData.resizable == true) {
            var resizables = $("#" + p_mainId + " > div");
            var options;
            var cssToSet;
            if (orientation == "Vertical") {
                cssToSet = "height";
                options = {
                    handles: "s",
                    start: function(event, ui){
                        v_subTotalSize = ui.originalSize.height + ui.originalElement.next().outerHeight();
                    },
                    stop: function(event, ui){
                        var cellPercentHeight = 100 * ui.originalElement.outerHeight(true) / $("#" + v_mainId).height();
                        ui.originalElement.height(cellPercentHeight + '%');
                        var nextCell = ui.originalElement.next();
                        var nextPercentHeight = 100 * ($("#" + v_mainId).outerHeight() - getHeight(ui)) / $("#" + v_mainId).height();
                        nextCell.height(nextPercentHeight + '%');
                        ui.originalElement.trigger("resize");
                        ui.originalElement.next().trigger("resize");
                    },
                    resize: function(event, ui){
                        if (v_subTotalSize - ui.size.height < 10) {
                            ui.size.height = v_subTotalSize - 10;
                        }
                        ui.originalElement.next().height($("#" + v_mainId).height() - getHeight(ui));
                        // the resize triggers a resize event automatically on the original element here, but not in the stop function
                        ui.originalElement.next().trigger("resize");
                    }
                };
            } else {
                cssToSet = "width";
                options = {
                    handles: "e",
                    start: function(event, ui){
                        v_subTotalSize = ui.originalSize.width + ui.originalElement.next().outerWidth();
                    },
                    stop: function(event, ui){
                        var cellPercentWidth = 100 * ui.originalElement.outerWidth(true) / $("#" + v_mainId).width();
                        ui.originalElement.width(cellPercentWidth + '%');
                        var nextCell = ui.originalElement.next();
                        var nextPercentWidth = 100 * ($("#" + v_mainId).width() - getWidth(ui)) / $("#" + v_mainId).width();
                        nextCell.width(nextPercentWidth + '%');
                        ui.originalElement.trigger("resize");
                        ui.originalElement.next().trigger("resize");
                    },
                    resize: function(event, ui){
                        if (v_subTotalSize - ui.size.width < 10) {
                            ui.size.width = v_subTotalSize - 10;
                        }
                        ui.originalElement.next().width($("#" + v_mainId).width() - getWidth(ui));
                        ui.originalElement.next().trigger("resize");
                    }
                };
            }

            for (var i = 0; i < resizables.length - 1; ++i) {
                $(resizables[i]).resizable(options);
            }
        }
    };

    function getWidth(ui) {
        var width = 0;
        $("#" + v_mainId + " > div").each(function() {
            if ($(this)[0] == ui.originalElement[0]) {
                width += ui.size.width;
            }
            else if ($(this)[0] != ui.originalElement.next()[0]) {
                width += $(this).outerWidth(true);
            }
        });
        return width;
    }

    function getHeight(ui) {
        var height = 0;
        $("#" + v_mainId + " > div").each(function() {
            if ($(this)[0] == ui.originalElement[0]) {
                height += ui.size.height;
            }
            else if ($(this)[0] != ui.originalElement.next()[0]) {
                height += $(this).outerHeight(true);
            }
        });
        return height;
    }
}

CView_Aligner.getHelp = function() {
    return "An aligner that aligns connected views either horizontally or vertically.\n" +
        "It can calculate the width or height of its child views from flex if a flexAligner viewmodel is connected.\n";
};

CView_Aligner.expectsInterface = function() {
    return [
        {
            "optional": ["getChildSizes"]
        },
        {
            "optional": ["getState"]
        }
    ];
};

CView_Aligner.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_Aligner",
        "type": "object",
        "properties": {
            "orientation": {
                "description": "The orientation of the aligner (default horizontal)",
                "type": "string",
                "enum": ["horizontal", "vertical"],
                "default": "vertical"
            },
            "flexCalculation": {
                "description": "Whether the aligner uses the subviews' flex to calculate the width / height of them",
                "type": "boolean",
                "format": "checkbox",
                "default": true
            },
            "resizable": {
                "description": "Whether the subviews can be resized",
                "type": "boolean",
                "format": "checkbox",
                "default": true
            },
            "existing": {
                "description": "Whether the html for the aligner already exists and does not need to be created",
                "type": "boolean",
                "format": "checkbox",
                "default": true
            }
        },
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema);
    return schema;
};

//# sourceURL=WebApplicationFramework\Views\View_Aligner.js