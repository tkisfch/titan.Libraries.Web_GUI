// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
/** Functionality to help building some GUI elements automatically. If used, should belong to application's view part. */

function CAutoGuiView(aViewModels, aID, aParentID)
{
    "use strict";
    var mID = aID;
    var mParentID = aParentID;
    var mViewModel = aViewModels[0];

    /** public functions */
    this.applicationCreated = function() {
        $("#" + mParentID).append('<div id="' + mID + '"></div>');
        
        $("#" + mID).on('click', action);
    };

    this.refresh = function(aFullRefresh)
    {
        var aData = mViewModel.getResponseElement();
        if (aFullRefresh)
        {
            var lMainDivObj = document.getElementById(mID);
            if (lMainDivObj)
            {
                lMainDivObj.innerHTML = null;
                buildElementFromData(lMainDivObj, aData, 1, '');
            }
        }
        else
        {
            buildElementFromData(null, aData, 1, '', true);
        }
    };

    /** private functions */
    
    function action(e)
    {
        var lCklickedExpandedGetData = e.target.getData;
        if (lCklickedExpandedGetData != null)
        {
            var lOrigGetDataRef = lCklickedExpandedGetData.origRq;
            var lIsContainer = e.target.id.search("_elem") > 0;
            var lIsText = e.target.id.search("_txt") > 0;
            mViewModel.command(lCklickedExpandedGetData, lOrigGetDataRef, e.target.innerHTML, lIsContainer, lIsText);
        }
    }
    
    function buildElementFromData(p_htmlDiv, p_responseMergedRequest, p_nestingLevel, p_childDivId, p_reuseObjects, p_index)
    {
        var lIdStr;
        var v_reuseObjects = (p_reuseObjects === true);
        
        if (p_responseMergedRequest && p_responseMergedRequest.length) // childVals or root list
        {
            for (var i = 0; i < p_responseMergedRequest.length; i++)
            {
                lIdStr = p_childDivId + "_" + i;
                if (p_childDivId === "")
                    lIdStr = i;
                buildElementFromData(p_htmlDiv, p_responseMergedRequest[i], p_nestingLevel + 1, lIdStr, v_reuseObjects);
                if (!v_reuseObjects)
                    addSeparatorLine(p_htmlDiv);
            }
        }
        else if (p_responseMergedRequest && p_responseMergedRequest.node)
        {
            var lObj;
            var lNodeText = p_responseMergedRequest.node.val /*+ " / " + p_childDivId*/;
            if (v_reuseObjects)
            {
                lObj = document.getElementById(p_childDivId + "_txt");
                if (lObj)
                    lObj.innerHTML = lNodeText;
            }
            else if (p_responseMergedRequest.getData)
            {
                var lTooltip = "Source: " + p_responseMergedRequest.getData.source + "\nElement: " + p_responseMergedRequest.getData.element;
                if (p_responseMergedRequest.getData.ptcname && p_responseMergedRequest.getData.length > 0)
                    lTooltip += "\nPtcName: " + p_responseMergedRequest.getData.ptcname;
                if (p_responseMergedRequest.getData.params)
                    for (var i = 0; i < p_responseMergedRequest.getData.params.length; i++)
                        lTooltip += "\nParam " + i + " - " + p_responseMergedRequest.getData.params[i].paramName + ": " + p_responseMergedRequest.getData.params[i].paramValue;
                var lElementText;
                if (!isvalue(p_index) || p_index === 0)
                    lElementText = p_responseMergedRequest.getData.element + ":";
                lObj = addElementToObj(p_htmlDiv, lNodeText /*+ "  " + p_childDivId*/, p_childDivId, p_nestingLevel, lTooltip, lElementText, p_responseMergedRequest.getData);
            }
            if (p_responseMergedRequest.node.childVals)
                buildElementFromData(lObj, p_responseMergedRequest.node.childVals, p_nestingLevel, p_childDivId, v_reuseObjects);
        }
        else if (p_responseMergedRequest && p_responseMergedRequest.list)
        {
            for (var i = 0; i < p_responseMergedRequest.list.length; i++)
            {
                lIdStr = p_childDivId + "_" + i;
                buildElementFromData(p_htmlDiv, p_responseMergedRequest.list[i], p_nestingLevel + 1, lIdStr, v_reuseObjects, i);
            }
        }
        /*
        else
        {
            //console.log("unhandled:", p_responseMergedRequest);
        }
        */
    }

	/** private functions */
    function getRandomColor(aN) {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++)
            color += letters[Math.floor((aN * 167654387673 * i + 984) % 8) + 6];
        return color;
    }

    function addElementToObj(p_htmlDiv, aText, aID, aNestingLevel, aTooltip, aElement, aGetData)
    {
        var lDivDiv = document.createElement("DIV");
        lDivDiv.getData = aGetData;
        if (aElement)
        {
            var lElementDiv = document.createElement("DIV");
            lElementDiv.innerHTML = aElement;
            lElementDiv.id = aID + "_elem";
            lElementDiv.setAttribute("class", "leftDiv");
            lElementDiv.getData = aGetData;
            lDivDiv.appendChild(lElementDiv);
        }
        var lTextDiv = document.createElement("DIV");
        lTextDiv.innerHTML = aText;
        lTextDiv.id = aID + "_txt";
        lTextDiv.getData = aGetData;
        lDivDiv.appendChild(lTextDiv);
        lDivDiv.id = aID;
        if (aTooltip)
            lDivDiv.title = aTooltip;
        lDivDiv.setAttribute("class", "coloredDiv");
        lDivDiv.style.backgroundColor = getRandomColor(aNestingLevel);
        p_htmlDiv.appendChild(lDivDiv);
        return p_htmlDiv.childNodes[p_htmlDiv.childNodes.length - 1];
    }

    function addSeparatorLine(p_htmlDiv)
    {
        var lDiv = document.createElement("DIV");
        lDiv.setAttribute("class", "line");
        if (p_htmlDiv) p_htmlDiv.appendChild(lDiv);
    }    
}

CAutoGuiView.getHelp = function() {
    return "This view can be used to visualize the whole response tree.";
};

CAutoGuiView.expectsInterface = function() {
    return [
        {
            "mandatory": ["getResponseElement", "command"]
        }
    ];
};

CAutoGuiView.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CAutoGuiView",
        "type": "object",
        "properties": {},
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema);
    return schema;
};

//# sourceURL=WebApplicationFramework\Views\View_AutoGui.js
