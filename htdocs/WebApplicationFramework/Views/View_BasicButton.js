// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_BasicButton(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";

    /** constructor */

    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_BasicButton)[0];
    var v_conditionViewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_BasicButton)[1];
    var v_mainId = p_mainId;
    var v_parentId = p_parentId;
    var v_customData = p_data;

    var v_label;
    var v_value = -1;

    var v_this = this;

    /** public functions */

    this.applicationCreated = function() {
        if (v_viewmodel != undefined) {
            $("#" + v_parentId).append(getHtml());
            if (v_customData.isElementLabelPresent) {
                v_label = ViewUtils.addLabel(v_mainId, v_customData.elementText);
            }
            if (v_customData.class != undefined) {
                $("#" + v_mainId).addClass(v_customData.class);
            } else {
                $("#" + v_mainId).addClass("BasicButton");
            }
            setupCallbacks();
            ViewUtils.addTooltip(v_mainId, v_customData);
        }
    };

    this.refresh = function(p_fullRefresh) {
        if (v_viewmodel != undefined) {
            if (ViewUtils.checkVisibility(v_conditionViewmodel, v_mainId)) {
                var dataObj = v_viewmodel.getListWithElementInfo();
                if (dataObj == undefined || dataObj.values[0] == undefined) {
                    return;
                }

                v_value = parseInt(dataObj.values[0].val) == NaN ? v_value : parseInt(dataObj.values[0].val);

                if (v_customData.isElementLabelPresent && v_customData.elementText == undefined) {
                    v_label.text(dataObj.values[0].element);
                }

                if (v_customData.text == undefined && v_customData.showLabel !== false) {
                    $("#" + v_mainId + " > button > span > label").text(dataObj.values[0].element);
                }

                if (v_customData.disabled !== true && dataObj.values[0] != undefined && dataObj.values[0].isWritable) {
                    $("#" + v_mainId + " > button").prop('disabled', false);
                } else {
                    $("#" + v_mainId + " > button").prop('disabled', true);
                }
            }
        }
    };

    /** protected functions */

    this.onClick = function() {
        v_viewmodel.setValue(0, ((v_customData.incrementValue) ? v_value + 1 : v_value));
    };

    this.onMiddleClick = function() {};

    /** private functions */

    function getHtml() {
        var html = '<div id="' + v_mainId + '"><button><span>';
        var img = '';
        var label = '';

        if (v_customData.img != undefined) {
            img += '<img ';
            img += 'src="' + v_customData.img + '" ';
            img += '>';
        }

        if (v_customData.showLabel !== false) {
            label += '<label>';
            if (v_customData.text != undefined) {
                label += v_customData.text;
            }
            label += '</label>';
        }

        if (v_customData.imagePosition == "right") {
            html += label;
            html += img;
        } else {
            html += img;
            html += label;
        }

        html += '</span></button></div>';
        return html;
    }

    function setupCallbacks() {
        $("#" + v_mainId + " > button").click(v_this.onClick).mouseup(v_this.onMiddleClick);
    }
}

CView_BasicButton.getHelp = function() {
    return "A simple button view that can contain a text and an image.";
};

CView_BasicButton.expectsInterface = function() {
    return [
        {
            "mandatory": ["setValue", "getListWithElementInfo"]
        },
        {
            "optional": ["getState"]
        }
    ];
};

CView_BasicButton.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_BasicButton",
        "type": "object",
        "properties": {
            "text": {
                "description": "The button text (if it is undefined, the request element will be used)",
                "type": "string"
            },
            "showLabel": {
                "description": "Whether the text is visible on the button (default true)",
                "type": "boolean",
                "format": "checkbox",
                "default": false
            },
            "img": {
                "description": "The button image",
                "type": "string",
                "default": "CustomizableContent/CustomizableApp/res/start.png"
            },
            "imagePosition": {
                "description": "The position of the image relative to the text (default left)",
                "type": "string",
                "enum": ["left", "right"],
                "default": "right"
            },
            "incrementValue": {
                "description": "Increment the value of the connected data element (default false)",
                "type": "boolean",
                "format": "checkbox",
                "default": true
            },
            "class": {
                "description": "The css class of the button",
                "type": "string",
                "default": "BasicButton"
            }
        },
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema, ViewUtils.commonElementSchema);
    return schema;
};

//# sourceURL=WebApplicationFramework\Views\View_BasicButton.js