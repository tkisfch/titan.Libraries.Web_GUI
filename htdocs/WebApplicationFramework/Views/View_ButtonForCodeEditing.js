// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_ButtonForCodeEditing(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";

    /** constructor */

    var v_id = p_mainId;
    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_ButtonForCodeEditing)[0];
    var v_data = p_data;
    if (v_data.tooltip == undefined && v_data.schema != undefined) {
        v_data.tooltip = "Middle click to edit with json editor.";
    }

    var base = new CView_BasicButton(p_viewmodels, p_mainId, p_parentId, p_data);

    this.applicationCreated = base.applicationCreated;
    this.refresh = base.refresh;

    base.onClick = function() {
        if (v_data.jsonEditor) {
            openEditor(CView_JSONEditor);
        } else {
            openEditor(CView_CodeEditor);
        }
    }

    base.onMiddleClick = function(e) {
        function schemaArrived(response) {
            v_data.schema = JSON.parse(response[0]['node']['val']);
            openTheEditor();
        }

        function openTheEditor() {
            if (v_data.schema == undefined || v_data.jsonEditor) {
                openEditor(CView_CodeEditor);
            } else {
                openEditor(CView_JSONEditor);
            }
        }

        e.preventDefault();
        e.stopPropagation();

        if (e.which == 2) {
            if (v_data.schema == undefined) {
                v_viewmodel.getDataList(v_data.schemaRequest, schemaArrived);
            } else {
                openTheEditor();
            } 
        }
    }

    function openEditor(EditorClass) {
        $('#' + v_id + '_Editor').remove();

        var params = [];
        var data = v_viewmodel.getListWithElementInfo();
        if (data != undefined && data.values[0] != undefined) {
            for (var i = 0; i < data.values.length; ++i) {
                params.push(data.values[i].val)
            }
        }
        var viewmodel = new ViewmodelForView(params);
        var customData = {
            'editorType': 'json',
            'manualSaveRequired': true,
            'closeable': true,
            'draggable': true,
            'headerText': 'Editor',
            'formatAllowed': true,
            'css': {
                'height': '600px',
                'width': '1200px',
                'z-index': 20
            },
            'offset': {
                'left': $(window).width() / 2 - 400,
                'top': 100,
            }
        }

        var editor = new EditorClass(
            [viewmodel],
            v_id + '_Editor',
            'TSGuiFrameworkMain',
            customData
        );

        editor.applicationCreated();
        ViewUtils.applyCss(customData, v_id + '_Editor');
        editor.refresh(true);
    }

    function ViewmodelForView(params) {
        var v_request = JSON.stringify(v_data.request);
        for (var i = 0; i < params.length; ++i) {
            v_request = v_request.replace('%Param_' + i + '%', params[i]);
        }
        v_request = JSON.parse(v_request);

        function createSetData(content) {
            var setData = mcopy(v_request);
            setData[0]['setData'] = setData[0]['getData'];
            setData[0]['getData'] = undefined;
            setData[0]['setData']['content'] = content;
            setData[0]['setData']['tp'] = 4;
            return setData;
        }

        this.getJSONData = function(callback) {
            function responseArrived(response) {
                if (response.length > 0 && response[0]['node'] != undefined) {
                    callback(JSON.parse(response[0]['node']['val']));
                } else {
                    callback('');
                }
            }

            v_viewmodel.getDataList(v_request, responseArrived);
        };

        this.setJSONData = function(data) {
            v_viewmodel.getDataList(createSetData(JSON.stringify(data, null, 4)), function(){});
        };

        this.getSchema = function() {
            return v_data.schema;
        };

        this.getTextData = function(callback) {
            function responseArrived(response) {
                if (response.length > 0 && response[0]['node'] != undefined) {
                    callback(response[0]['node']['val']);
                } else {
                    callback('');
                }
            }

            v_viewmodel.getDataList(v_request, responseArrived);
        };

        this.setTextData = function(data) {
            v_viewmodel.getDataList(createSetData(JSON.stringify(JSON.parse(data), null, 4)), function(){});
        };
    }

}

CView_ButtonForCodeEditing.getHelp = function() {
    return "A button view that can be used to create an editor view.";
};

CView_ButtonForCodeEditing.expectsInterface = function() {
    return CView_BasicButton.expectsInterface();
};

CView_ButtonForCodeEditing.getCustomDataSchema = function() {
    var baseSchema = CView_BasicButton.getCustomDataSchema();
    var schema = {
        "properties": {
            "request": {
                "description": "The request the editor can use to get and set its data. %ParamX%, where X is a number will be replaced by data received from the connected viewmodel.",
                "type": "array",
                "format": "tabs",
                "items": {
                    "type": "object"
                }
            },
            "schema": {
                "description": "The json schema that the json editor will use.",
                "type": "object"
            },
            "schemaRequest": {
                "description": "The request list to get the json schema that the json editor will use.",
                "type": "array"
            },
            "jsonEditor": {
                "description": "Whether we use json editor by default.",
                "type": "boolean",
                "format": "checkbox"
            }
        }
    };
    $.extend(true, baseSchema, schema);
    return baseSchema;
};

//# sourceURL=WebApplicationFramework\Views\View_ButtonForCodeEditing.js
