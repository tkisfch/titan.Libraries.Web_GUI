// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_ButtonForDownload(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";

    /** constructor */

    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_ButtonForDownload)[0];
    var v_data = p_data;
    var base = new CView_BasicButton(p_viewmodels, p_mainId, p_parentId, p_data);

    this.applicationCreated = base.applicationCreated;
    this.refresh = base.refresh;

    function responseArrived(response) {
        var data = v_viewmodel.getListWithElementInfo();
        if (data == undefined || data.values[0] == undefined) {
            return;
        }

        var fileName = data.values[0].val;
        window.open(fileName, '_blank');
    }

    base.onClick = function() {
        v_viewmodel.setValue(0, 0, undefined, undefined, responseArrived);
    }
}

CView_ButtonForDownload.getHelp = function() {
    return "A button view that can be used to download data to a file.";
};

CView_ButtonForDownload.expectsInterface = function() {
    return CView_BasicButton.expectsInterface();
};

CView_ButtonForDownload.getCustomDataSchema = function() {
    return CView_BasicButton.getCustomDataSchema();
};

//# sourceURL=WebApplicationFramework\Views\View_ButtonForDownload.js