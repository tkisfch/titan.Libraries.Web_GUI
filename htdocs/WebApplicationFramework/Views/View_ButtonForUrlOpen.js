// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_ButtonForUrlOpen(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";

    /** constructor */

    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_ButtonForUrlOpen)[0];
    var v_data = p_data;
    var v_midleClickOpensTab = false;
    if (v_data.tooltip == undefined) {
        v_data.tooltip = "Middle click to open in a new tab.";
    }
    if (v_data.paramKey == undefined) {
        v_data.paramKey = "location";
    }
    if (v_data.paramKey == "location") {
        v_midleClickOpensTab = true;
    }

    var base = new CView_BasicButton(p_viewmodels, p_mainId, p_parentId, p_data);

    this.applicationCreated = base.applicationCreated;
    this.refresh = base.refresh;

    base.onClick = function() {
        var data = v_viewmodel.getListWithElementInfo();
        var value;
        if (data != undefined && data.values[0] != undefined) {
            value = data.values[0].val;
        }

        var additionalData = {
            "setAppParams": {
                "appName": v_data.appName,
                "params": [
                    {
                        "key": v_data.paramKey,
                        "value": value
                    }
                ]
            }
        }
        v_viewmodel.setValue(undefined, undefined, undefined, undefined, undefined, additionalData);

        additionalData = {
            "loadApp": v_data.appName
        }
        v_viewmodel.setValue(undefined, undefined, undefined, undefined, undefined, additionalData);
    }

    base.onMiddleClick = function(e) {
        if (e.which != 2 || !v_midleClickOpensTab) {
            return;
        }

        var data = v_viewmodel.getListWithElementInfo();
        if (data == undefined || data.values[0] == undefined) {
            return;
        }
        window.open(data.values[0].val, '_blank');
    }
}

CView_ButtonForUrlOpen.getHelp = function() {
    return "A button view that can be used to set the location of a WebpageFrame application and load that app.";
};

CView_ButtonForUrlOpen.expectsInterface = function() {
    return CView_BasicButton.expectsInterface();
};

CView_ButtonForUrlOpen.getCustomDataSchema = function() {
    var baseSchema = CView_BasicButton.getCustomDataSchema();
    var schema = {
        "properties": {
            "appName": {
                "description": "The name of the application",
                "type": "string"
            },
            "paramKey": {
                "description": "The name of the parameter key",
                "type": "string"
            }
        }
    };
    $.extend(true, baseSchema, schema);
    return baseSchema;
};

//# sourceURL=WebApplicationFramework\Views\View_ButtonForUrlOpen.js