// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_CheckboxOrSwitch(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";
    /** constructor */
    
    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_CheckboxOrSwitch)[0];
    var v_conditionViewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_CheckboxOrSwitch)[1];
    var v_mainId = p_mainId;
    var v_parentId = p_parentId;
    var v_customData = p_data;
    
    var selector = -1;
    var v_label;
    var v_currentValue;
    var v_switchDisabled = false;
    
    var v_this = this;
    
    this.applicationCreated = function() {
        if (v_viewmodel != undefined) {
            $("#" + v_parentId).append(getHtml());
            if (v_customData.isElementLabelPresent) {
                v_label = ViewUtils.addLabel(v_mainId, v_customData.elementText);
            }
            $("#" + v_mainId).addClass("CheckBoxOrSwitch");
            
            if (v_customData.isSwitch == true) {
                $("#" + v_mainId + '_switchImg').click(function() {
                    if (!v_switchDisabled) { v_viewmodel.setValue(0, !v_currentValue); }
                });
            } else {
                $("#" + v_mainId + "_checkBox").on('change', function() {
                    v_viewmodel.setValue(0, !v_currentValue);
                });
            }
            
            ViewUtils.addTooltip(v_mainId, v_customData);
        }
    };
    
    this.refresh = function(p_fullRefresh) {
        if (v_viewmodel != undefined) {
            if (ViewUtils.checkVisibility(v_conditionViewmodel, v_mainId)) {
                var dataObject = v_viewmodel.getListWithElementInfo();
                if (dataObject == undefined || dataObject.values[0] == undefined) {
                    return;
                }
                
                if (dataObject.values[0].val == undefined) {
                    $("#" + v_mainId + " > *").css("display", "none");
                    $("#" + v_mainId).addClass("NoData");
                } else {
                    $("#" + v_mainId + " > *").css("display", "");
                    $("#" + v_mainId).removeClass("NoData");
                }
                
                v_currentValue = dataObject.values[0].val == "true";
                
                if (p_fullRefresh && v_customData.isElementLabelPresent && v_customData.elementText == undefined) {
                    v_label.text(dataObject.values[0].element);
                }
                
                if (v_customData.isSwitch == true) {
                    var temp = 0;
                    if (dataObject.values[0].val == "true") { temp = temp + 1; }
                    if (!dataObject.values[0].isWritable || v_customData.disabled === true) { temp = temp + 2; v_switchDisabled = true; } else { v_switchDisabled = false; }
                    if (selector != temp) {
                        selector = temp;
                        $("#" + v_mainId + "_switchImg").attr("src", getImg(selector));
                    }
                } else {
                    document.getElementById(v_mainId + "_checkBox").checked = ( (dataObject.values[0].val == "true") ? true : false);
                    document.getElementById(v_mainId + "_checkBox").disabled = ( (!dataObject.values[0].isWritable || v_customData.disabled === true) ? true : false);
                }
                
                $("#" + v_mainId + "_checkBoxLabel").text( (v_customData.labelText) ? v_customData.labelText : dataObject.values[0].element);
            }
        }
    };

    function getHtml() {
        var label = '';
        if (v_customData.isLabelPresent == true) {
            label = '<label id="' + v_mainId + '_checkBoxLabel"></label>';
        }
        
        var input;
        if (v_customData.isSwitch == true) {
            input = '<img id="' + v_mainId + '_switchImg"/>';
        } else {
            input = '<input type="checkbox" id="' + v_mainId + "_checkBox" + '">';
        }
        
        return '<div id="' + v_mainId + '"><div></div>' + ((v_customData.alignment == "right") ? (label + input) : (input + label)) + '</div>';
        
    }
    
    function getImg(selector) {
        var img = "";
        switch (selector) {
            case 0:
                img = ( (v_customData.ImgOff) ? v_customData.ImgOff : "WebApplicationFramework/Res/switchOff.png" );
                break;
            case 1:
                img = ( (v_customData.ImgOn) ? v_customData.ImgOn : "WebApplicationFramework/Res/switchOn.png" );
                break;
            case 2:
                img = ( (v_customData.ImgOffD) ? v_customData.ImgOffD : "WebApplicationFramework/Res/switchOff_disabled.png" );
                break;
            case 3:
                img = ( (v_customData.ImgOnD) ? v_customData.ImgOnD : "WebApplicationFramework/Res/switchOn_disabled.png" );
                break;            
        }
        return img;
    }
}

CView_CheckboxOrSwitch.getHelp = function() {
    return "A checkbox view that can also look like a switch.";
}

CView_CheckboxOrSwitch.expectsInterface = function() {
    return [
        {
            "mandatory": ["setValue", "getListWithElementInfo"]
        },
        {
            "optional": ["getState"]
        }
    ];
};

CView_CheckboxOrSwitch.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_CheckboxOrSwitch",
        "type": "object",
        "properties": {
            "isSwitch": {
                "type": "boolean",
                "format": "checkbox",
                "description": "Are we a flippable switch? (by default we are a checkbox)",
                "default": true
            },
            "isLabelPresent": {
                "type": "boolean",
                "format": "checkbox",
                "description": "Show label next to the checkbox / switch (default false)",
                "default": true
            },
            "labelText": {
                "type": "string",
                "description": "The text of the label next to the checkbox / switch"
            },
            "alignment": {
                "type": "string",
                "description": "The position of the label relative to the checkbox / switch (default left)",
                "enum": ["left", "right"],
                "default": "right"
            },
            "ImgOff": {
                "type": "string",
                "title": "Off state image's URL"
            },
            "ImgOffD": {
                "type": "string",
                "title": "Disabled off state image's URL"
            },
            "ImgOn": {
                "type": "string",
                "title": "On state image's URL"
            },
            "ImgOnD": {
                "type": "string",
                "title": "Disabled on state image's URL"
            }
        },
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema, ViewUtils.commonElementSchema);
    return schema;
};
//# sourceURL=WebApplicationFramework\Views\View_CheckboxOrSwitch.js
