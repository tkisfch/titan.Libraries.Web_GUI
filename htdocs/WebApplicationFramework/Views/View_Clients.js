// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_Clients(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";
    
    /** constructor */
    
    var v_mainId = p_mainId;
    var v_parentId = p_parentId;
    var v_tableId = v_mainId + "_table";
    var v_customData = p_data;
    var v_subviews = [];
    
    var v_scrollId = v_mainId + "_Scroll";
    var scrollBar = new CView_Scroll(p_viewmodels, v_scrollId, v_mainId, p_data);
    
    var v_viewmodels = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_Clients);
    var v_viewmodel = v_viewmodels[0];
    var v_streamingViewmodel = v_viewmodels[1];
    var v_conditionViewmodel = v_viewmodels[2];
    var v_scrollInitialised = false;
    
    var v_class = "ClientsView";
    if (v_customData.class != undefined) {
        v_class = v_customData.class;
    }
    
    /** public functions */

    this.applicationCreated = function() {
        if (v_viewmodel != undefined) {
            $("#" + v_parentId).append(getHtml());
            $("#" + v_mainId).css("position", "relative");
        }
    };
    
    this.refresh = function() {
        if (v_viewmodel != undefined && ViewUtils.checkVisibility(v_conditionViewmodel, v_mainId)) {
            var data = v_viewmodel.getViewmodelBundle();
            createSubviews(data);
            refreshSubviews(data);
            refreshScroll();
        }
    };
    
    /* private functions */
    
    function createSubviews(data) {
        var table = $("#" + v_tableId);
        var existingGroups = $("#" + v_tableId + " > tr").length / 3;
        var numberOfGroupsToCreate = data.viewmodels.length - existingGroups;
        if (numberOfGroupsToCreate > 0) {
            for (var i = 0; i < numberOfGroupsToCreate; ++i) {
                var aligner1Id = v_mainId + '_aligner1_' + (existingGroups + i);
                var aligner2Id = v_mainId + '_aligner2_' + (existingGroups + i);
                var tableId = v_mainId + '_subTable_' + (existingGroups + i);
                table.append(
                    '<tr><td id="' + aligner1Id + '"></td><td id="' + aligner2Id + '"></td></tr>' + 
                    '<tr><td colspan=2 id="' + tableId + '"></td></tr>' + 
                    '<tr><td colspan=2><div class="line"></div></td></tr>'
                );
                
                var aligner1 = new CView_ElementAligner([data.viewmodels[existingGroups + i].aligner1], aligner1Id + "_view", aligner1Id, v_customData.customDataForFirstAligner);
                var aligner2 = new CView_ElementAligner([data.viewmodels[existingGroups + i].aligner2], aligner2Id + "_view", aligner2Id, v_customData.customDataForSecondAligner);
                var tableSubview = new CView_ElementTable([data.viewmodels[existingGroups + i].table], tableId + "_view", tableId, v_customData.customDataForTable);
                
                v_subviews.push(aligner1);
                v_subviews.push(aligner2);
                v_subviews.push(tableSubview);
                
                aligner1.applicationCreated();
                aligner2.applicationCreated();
                tableSubview.applicationCreated();
                
                ViewUtils.applyCss(v_customData.customDataForFirstAligner, aligner1Id + "_view");
                ViewUtils.processCss(v_customData.customDataForFirstAligner, aligner1Id);
                ViewUtils.applyCss(v_customData.customDataForSecondAligner, aligner2Id + "_view");
                ViewUtils.processCss(v_customData.customDataForSecondAligner, aligner2Id);
                ViewUtils.applyCss(v_customData.customDataForTable, tableId + "_view");
                ViewUtils.processCss(v_customData.customDataForTable, tableId);
            }
        } else if (numberOfGroupsToCreate < 0) {
            $("#" + v_tableId + " > tr").slice((existingGroups + numberOfGroupsToCreate) * 3).remove();
            for (var i = 0; i > 3 * numberOfGroupsToCreate; --i) {
                v_subviews.pop();
            }
        }
        
        if (data.firstSeparatorExists) {
            $($("#" + v_tableId + " > tr")[0]).css("display", "");
        } else {
            $($("#" + v_tableId + " > tr")[0]).css("display", "none");
        }
    }
    
    function refreshSubviews(data) {
        for (var i = 0; i < v_subviews.length; ++i) {
            v_subviews[i].refresh(data.fullRefresh);
        }
    }
    
    function refreshScroll() {
        var isStreaming = false;
        if (v_streamingViewmodel.isStreaming != undefined) {
            isStreaming = v_streamingViewmodel.isStreaming();
        }
        if (v_scrollInitialised && !isStreaming) {
            $("#" + v_scrollId).remove();
            v_scrollInitialised = false;
            $("#" + v_mainId).scrollTop(0).css("overflow-y", "auto");
        } else if (!v_scrollInitialised && isStreaming) {
            scrollBar.applicationCreated();
            v_scrollInitialised = true;
            $("#" + v_mainId).scrollTop(0).css("overflow-y", "hidden");
        }
        
        if (isStreaming) {
            scrollBar.refresh();
        }
    }
    
    function getHtml() {
        return '' +
            '<div id="' + v_mainId + '" class="' + v_class + '">' + 
                '<table id="' + v_tableId + '">' +
                    '<col width="50%">' +
                    '<col width="50%">' +
                '</table>' +
            '</div>';
    }
}

CView_Clients.getHelp = function() {
    return "The view for the ExecCtrl clients tab. It creates 2 aligners and a table per group.";
};

CView_Clients.expectsInterface = function() {
    var expectedInterface = CView_Scroll.expectsInterface();
    expectedInterface.unshift({
        "optional": ["getState"]
    });
    expectedInterface.unshift({
        "optional": ["isStreaming"]
    });
    expectedInterface.unshift({
        "mandatory": ["getViewmodelBundle"]
    });
    
    return expectedInterface;
};

CView_Clients.getCustomDataSchema = function() {
    var tableSchema = CView_ElementTable.getCustomDataSchema();
    var firstAlignerSchema = CView_ElementAligner.getCustomDataSchema();
    // yes they are the same, but ajv does not like it, when the same object can be found multiple times in the same schema...
    var secondAlignerSchema = CView_ElementAligner.getCustomDataSchema();
    var scrollSchema = CView_Scroll.getCustomDataSchema();
    
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_Clients",
        "type": "object",
        "properties": {
            "class": {
                "description": "The css class of the view",
                "type": "string",
                "default": "ClientsView"
            },
            "hideFirstAligner": {
                "description": "Hide the first aligner when not all of its data is visible creating a more scroll like experience (default true).",
                "type": "boolean",
                "default": "false"
            },
            "customDataForFirstAligner": firstAlignerSchema,
            "customDataForSecondAligner": secondAlignerSchema,
            "customDataForTable": tableSchema
        },
        "additionalProperties": false,
        "required": ["customDataForFirstAligner", "customDataForSecondAligner", "customDataForTable"]
    };
    
    $.extend(true, schema, scrollSchema, ViewUtils.commonViewSchema);
    return schema;
};

//# sourceURL=WebApplicationFramework\Views\View_Clients.js