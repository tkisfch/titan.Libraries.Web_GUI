// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_ContextMenu(p_viewmodels, p_viewId, p_parentId, p_options) {
    "use strict";
    
    var v_parentId = p_parentId;
    var v_viewId = p_viewId;
    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_ContextMenu)[0];
    var v_options = p_options;
    
    var currentList;

    function close() {
        $("#" + v_viewId).remove();
    }
    
    this.applicationCreated = function() {
        currentList = v_viewmodel.getMenuElements();
        $("#" + v_parentId).append('<ul class="ContextMenu" id="' + v_viewId + '"></ul>');
        var ul = $("#" + v_viewId);
        for (var i = 0; i < currentList.length; ++i) {
            ul.append('<li>' + currentList[i].text + '</li>');
        }
        var elements = ul.find("li");
        for (var i = 0; i < elements.length; ++i) {
            $(elements[i]).click(function() {
                currentList[$(this).index()].callback();
                close();
            });
        }
        
        if (v_options.offset != undefined) {
            ul.offset(v_options.offset);
            ul.offset(v_options.offset);
        }
        
        ul.mouseleave(close);
    }
}

CView_ContextMenu.getHelp = function() {
    return "A context menu view that closes when the mouse leaves it.";
};

CView_ContextMenu.expectsInterface = function() {
    return [
        {
            "mandatory": ["getMenuElements"]
        }
    ];
};

CView_ContextMenu.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_ContextMenu",
        "type": "object",
        "properties": {
            "offset": {
                "description": "The offset of the context menu.",
                "type": "object",
                "properties": {
                    "top": {
                        "type": "integer"
                    },
                    "left": {
                        "type": "integer"
                    }
                }
            }
        },
        "additionalProperties": false
    };
    return schema;
};

//# sourceURL=WebApplicationFramework\Views\View_ContextMenu.js