// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_Paging(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";
    
    /** constructor */
    
    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_Paging)[0];
    var v_mainId = p_mainId;
    var v_parentId = p_parentId;
    var v_customData = p_data;
    
    var v_this = this;
    
    /** public functions */

    this.applicationCreated = function() {
        $("#" + v_parentId).append(getHtml());
        if (v_customData.class != undefined) {
            $("#" + v_mainId).addClass(v_customData.class);
        } else {
            $("#" + v_mainId).addClass("BasicPaging");
        }
        setupCallbacks();
    };

    this.refresh = function(p_fullRefresh) {
        var filter = v_viewmodel.getRangeFilter();
        var size = v_viewmodel.getSize();
        var offset;
        if (filter.offset != undefined) {
            offset = filter.offset;
        } else {
            offset = 0;
        }
        var count;
        if (filter.count != undefined) {
            count = filter.count;
        } else {
            count = size - offset;
        }
        var text = "" + offset + " - " + (offset + count) + " / " + size;
        $('#' + v_mainId + ' > label').html(text);
    };

    /** private functions */
    
    function getHtml() {
        var html = '<div id="' + v_mainId + '">' +
            '<button id="' + v_mainId + '_lt3">&lt&lt&lt</button>' +
            '<button id="' + v_mainId + '_lt2">&lt&lt</button>' +
            '<button id="' + v_mainId + '_lt1">&lt</button>' +
            '<label id="' + v_mainId + '_label"></label>' +
            '<button id="' + v_mainId + '_gt1">&gt</button>' +
            '<button id="' + v_mainId + '_gt2">&gt&gt</button>' +
            '<button id="' + v_mainId + '_gt3">&gt&gt&gt</button>' +
        '</div>';
        return html;
    }
    
    function setupCallbacks() {
        $('#' + v_mainId + '_lt3').click(function() {
            v_viewmodel.changeFilter({"offset" : {"to": 0}});
        });
        $('#' + v_mainId + '_lt2').click(function() {
            var filter = v_viewmodel.getRangeFilter();
            v_viewmodel.changeFilter({"offset" : {"by": -filter.count}});
        });
        $('#' + v_mainId + '_lt1').click(function() {
            v_viewmodel.changeFilter({"offset" : {"by": -1}});
        });
        $('#' + v_mainId + '_gt1').click(function() {
            v_viewmodel.changeFilter({"offset" : {"by": 1}});
        });
        $('#' + v_mainId + '_gt2').click(function() {
            var filter = v_viewmodel.getRangeFilter();
            v_viewmodel.changeFilter({"offset" : {"by": filter.count}});
        });
        $('#' + v_mainId + '_gt3').click(function() {
            var size = v_viewmodel.getSize();
            var filter = v_viewmodel.getRangeFilter();
            v_viewmodel.changeFilter({"offset" : {"to": size - filter.count}});
        });
    }
}

CView_Paging.getHelp = function() {
    return "A small paging view that can control the viewmodel's range filter.";
}

CView_Paging.expectsInterface = function() {
    return [
        {
            "mandatory": ["getSize", "getRangeFilter", "changeFilter"]
        }
    ];
}

CView_Paging.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_Paging",
        "type": "object",
        "properties": {
            "class": {
                "description": "The css class of the pager",
                "type": "string",
                "default": "BasicPaging"
            }
        },
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema);
    return schema;
}

//# sourceURL=WebApplicationFramework\Views\View_Paging.js