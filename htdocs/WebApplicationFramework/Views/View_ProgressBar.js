// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_ProgressBar(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";
    
    /** constructor */
    
    var v_mainId = p_mainId;
    var v_parentId = p_parentId;
    var v_customData = p_data;
    if (v_customData.stroke == undefined) {
        v_customData.stroke = "#4CAF50";
    }
    if (v_customData.strokeWidth == undefined) {
        v_customData.strokeWidth = 20;
    }
    if (v_customData.radius == undefined) {
        v_customData.radius = 100;
    }
    
    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_ProgressBar)[0];
    var v_this = this;
    
    /** public functions */

    this.applicationCreated = function() {
        $("#" + v_parentId).append(getHtml());
        if (v_customData.isElementLabelPresent) {
            ViewUtils.addLabel(v_mainId, v_customData.elementText);
        }
        if (v_customData.class != undefined) {
            $("#" + v_mainId).addClass(v_customData.class);
        } else {
            $("#" + v_mainId).addClass("ProgressBar");
        }
    };

    this.refresh = function() {
        if (v_viewmodel != undefined) {
            var list = v_viewmodel.getList();
            updateCircle(list.values[0][0] != "" ? list.values[0][0] : 100.0, list.values[1][0]);
        }
    };

    /** private functions */
    
    function getHtml() {
        return '<div id="' + v_mainId + '"><svg><path fill="none" stroke="' + v_customData.stroke + '" stroke-width="' + v_customData.strokeWidth + '"></path></svg><label class="progressText"></label><label class="progressPercentage"></label></div>';
    }
    
    function updateCircle(percent, text) {
        $("#" + v_mainId + " path").attr("d", getArc(percent));
        if (text != undefined) {
            $("#" + v_mainId + " .progressText").text(text);
            $("#" + v_mainId + " .progressPercentage").text(percent + "%");
        }
    }
    
    function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
        var angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;

        return {
            x: centerX + (radius * Math.cos(angleInRadians)),
            y: centerY + (radius * Math.sin(angleInRadians))
        };
    }

    function getArc(percent){
        if (percent == 100.0) {
            percent = 99.99;
        }
        
        var radius = v_customData.radius;
        var startAngle = 0;
        var endAngle = 360.0 * percent / 100.0;
        
        var centerY = $("#" + v_mainId).height() / 3;
        var centerX = $("#" + v_mainId).width() / 2;
                
        var start = polarToCartesian(centerX, centerY, radius, endAngle);
        var end = polarToCartesian(centerX, centerY, radius, startAngle);

        var arcSweep = endAngle - startAngle <= 180 ? "0" : "1";

        var d = [
            "M", start.x, start.y, 
            "A", radius, radius, 0, arcSweep, 0, end.x, end.y
        ].join(" ");
                
        return d;       
    }
}

CView_ProgressBar.getHelp = function() {
    return "A progreesbar view. Requires a list whose first element is a percentage and the second is an optional text.";
}

CView_ProgressBar.expectsInterface = function() {
    return [
        {
            "mandatory": ["getList"]
        }
    ];
};

CView_ProgressBar.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_ProgressBar",
        "type": "object",
        "properties": {
            "class": {
                "type": "string",
                "description": "the css class of the progress bar"
            },
            "stroke": {
                "type": "string",
                "description": "the color of the progress circle in hex format",
                "pattern": "^#(?:[0-9a-fA-F]{3}){1,2}$",
                "default": "#4CAF50"
            },
            "strokeWidth": {
                "type": "integer",
                "description": "the width of the circle's ring",
                "default": 20
            },
            "radius": {
                "type": "integer",
                "description": "the radius of the circle in pixels",
                "default": 100
            }
        },
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema);
    return schema;
};
//# sourceURL=WebApplicationFramework\Views\View_ProgressBar.js