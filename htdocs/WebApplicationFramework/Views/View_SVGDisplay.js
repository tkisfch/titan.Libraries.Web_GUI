// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                        //
///////////////////////////////////////////////////////////////////////////////////////////////////////

function CView_SVGDisplay(p_viewmodels, p_objId, p_parentId, p_data) {
    "use strict";
    
    /** constructor */
    var v_viewmodels = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_SVGDisplay);
    var v_viewmodel = v_viewmodels[0];
    var v_conditionViewmodel = v_viewmodels[1];
    var v_writableViewmodel = v_viewmodels[2];
    
    var v_objId = p_objId;
    var v_parentId = p_parentId;
    var v_customData = p_data;
    
    var v_SVGDisplayLabel;
    var v_currentSVG = "";
    
    /** public functions */
    this.applicationCreated = function() {
        $("#" + v_parentId).append(getHtml());
        if (v_customData.isElementLabelPresent) {
            v_SVGDisplayLabel = ViewUtils.addLabel(v_objId, v_customData.elementText);
        }
        if (v_customData.class != undefined) {
            $("#" + v_objId).addClass(v_customData.class);
        } else {
            $("#" + v_objId).addClass("BasicSVGDisplay");
        }

        ViewUtils.addTooltip(v_objId, v_customData);
    };

    this.refresh = function(p_fullRefresh) {
        if (ViewUtils.checkVisibility(v_conditionViewmodel, v_objId) && v_viewmodel != undefined) {
            var dataObject = v_viewmodel.getListWithElementInfo();
            if (v_customData.text != undefined || dataObject == undefined || dataObject.values[0] == undefined) {
                return;
            }

            if (p_fullRefresh && v_customData.isElementLabelPresent && v_customData.elementText == undefined) {
                v_SVGDisplayLabel.text(dataObject.values[0].element);
            }
            
            if (v_customData.text == undefined) {
                var text = "";
                //console.log(dataObject)
                if(dataObject.values[0].val != undefined) {
                    $("#" + v_objId + " > *").css("display", "");
                    $("#" + v_objId).removeClass("NoData");
                    text = dataObject.values[0].val;
                } else {
                    $("#" + v_objId + " > *").css("display", "none");
                    $("#" + v_objId).addClass("NoData");
                }
                if (text != v_currentSVG)
                {
                    v_currentSVG = text;
                    try {
                        var dectext = window.atob(text);
                        dectext = dectext.replace("style=\"", "style=\"transform:scale(0.75);");
                        $("#" + v_objId).html(dectext);
                    } catch (e) {
                        text = text.replace("style=\"", "style=\"transform:scale(0.75);");
                        $("#" + v_objId).html(text);
                    }
                }
            }

            if (v_customData.disabled !== true && dataObject.values[0] != undefined && dataObject.values[0].isWritable && (v_writableViewmodel == undefined || v_writableViewmodel.isWritable())) {
            } else {
            }
        }
    };

    /** private functions */
    function getHtml() {
        return '<div id="' + v_objId + '"></div>';
    }
}

CView_SVGDisplay.getHelp = function() {
    return "Simple SVG display. Expected interface: \n" + JSON.stringify(CView_SVGDisplay.expectsInterface(), null, 4);
}

CView_SVGDisplay.expectsInterface = function() {
    return [
        {
            "optional": ["getListWithElementInfo"]
        },
        {
            "optional": ["getState"]
        },
        {
            "optional": ["isWritable"]
        }
    ];
};

CView_SVGDisplay.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_SVGDisplay",
        "type": "object",
        "properties": {
            "class": {
                "type": "string",
                "description": "the css class of the view",
                "default": "BasicSVGDisplay"
            },
            "text": {
                "type": "string",
                "description": "the SVGDisplay text"
            }
        },
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema, ViewUtils.commonElementSchema);
    return schema;
};

//# sourceURL=WebApplicationFramework\Views\View_SVGDisplay.js