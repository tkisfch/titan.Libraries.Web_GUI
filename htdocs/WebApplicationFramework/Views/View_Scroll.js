// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_Scroll(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";
    
    /** constructor */
    
    var v_viewmodels = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_Scroll);
    var v_viewmodel = v_viewmodels[0];
    var v_viewPortSizeViewmodel = v_viewmodels[1];
    
    var v_mainId = p_mainId;
    var v_parentId = p_parentId;
    var v_customData = p_data;
    
    if (v_customData.clickOnScrollBarBehaviour == undefined) {
        v_customData.clickOnScrollBarBehaviour = "bigstep";
    }
    
    if (v_customData.bigStep == undefined) {
        v_customData.bigStep = 10;
    }
    
    var v_refreshEnabled = true;
    
    var v_numberOfValues;
    var v_currentValue;
    var v_viewportSize = 1;
    
    var v_height;
    var v_handleOffset;
    var v_handleHeight;
    
    var BUTTON_HEIGHT = 14;
    
    var v_this = this;
    
    /** public functions */

    this.applicationCreated = function() {
        $("#" + v_parentId).append(getHtml());
        $("#" + v_mainId).css("right", - $("#" + v_parentId)[0].scrollLeft);
        setupCallbacks();
    };

    this.refresh = function(p_fullRefresh) {
        if (v_refreshEnabled) {
            var range = v_viewmodel.getRange();
            v_numberOfValues = range.max - range.min;
            v_currentValue = v_viewmodel.getPosition();
            
            if (v_viewPortSizeViewmodel.getViewportSize != undefined) {
                v_viewportSize = v_viewPortSizeViewmodel.getViewportSize();
            }
            
            //$("#" + v_mainId).css("right", $("#" + v_parentId).width() - $("#" + v_parentId)[0].scrollWidth);

            v_height = $('#' + v_mainId + '_bar').height() - 2 * BUTTON_HEIGHT;
            setHandleHeight();
            v_height = $('#' + v_mainId + '_bar').height() - 2 * BUTTON_HEIGHT - v_handleHeight;
            setOffset();
        }
    };

    /** private functions */
    
    function getHtml() {
        var html = '<div id="' + v_mainId + '" class="View_Scroll">' +
            '<div id="' + v_mainId + '_top" class="View_Scroll_Top"></div>' +
            '<div id="' + v_mainId + '_bar" class="View_Scroll_Bar">' +
                '<div id="' + v_mainId + '_handle" class="View_Scroll_Handle"></div>' +
            '</div>' +
            '<div id="' + v_mainId + '_bottom" class="View_Scroll_Bottom"></div>' +
        '</div>';
        return html;
    }
    
    function setOffset() {
        var proportion = v_currentValue / (v_numberOfValues - 1);
        v_handleOffset = Math.ceil((v_height * proportion) + BUTTON_HEIGHT); // +BUTTON_HEIGHT for the top "button"
        if (v_handleOffset > v_height + BUTTON_HEIGHT) {
            v_handleOffset = v_height + BUTTON_HEIGHT;
        }
        $('#' + v_mainId + '_handle').css("top", v_handleOffset);
    }
    
    function setHandleHeight() {
        v_handleHeight = Math.ceil((v_viewportSize * v_height) / (v_viewportSize + v_numberOfValues));
        if (v_handleHeight < BUTTON_HEIGHT) {
            v_handleHeight = BUTTON_HEIGHT; // the min handle height
        }
        $('#' + v_mainId + '_handle').css("height", v_handleHeight);
    }
    
    function onScroll(event) {
        var up = false;
        var down = false;
        if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
            v_currentValue = v_viewmodel.getPosition();
            v_viewmodel.setPosition(v_currentValue - 1);
            up = true;
        } else {
            v_currentValue = v_viewmodel.getPosition();
            v_viewmodel.setPosition(v_currentValue + 1);
            down = true;
        }
        
        if ((v_currentValue != 0 || down) && (v_currentValue != v_numberOfValues - 1 || up)) {
            event.stopPropagation();
            event.preventDefault();
        }
    }
    
    function onHorizontalScroll() {
        $("#" + v_mainId).css("right", - $("#" + v_parentId)[0].scrollLeft);
    }
    
    function setupCallbacks() {
        $('#' + v_mainId + '_top').click(function(event) {
            v_currentValue = v_viewmodel.getPosition();
            v_viewmodel.setPosition(v_currentValue - 1);
            event.stopPropagation();
        });
        
        $('#' + v_mainId + '_bottom').click(function(event) {
            v_currentValue = v_viewmodel.getPosition();
            v_viewmodel.setPosition(v_currentValue + 1);
            event.stopPropagation();
        });
        
        $('#' + v_mainId + '_bar').click(function(e) {
            var barTop = $(this).offset().top;
            var clickTop = e.pageY;
            if (v_customData.clickOnScrollBarBehaviour == "bigstep") {
                v_currentValue = v_viewmodel.getPosition();
                if (clickTop - barTop - BUTTON_HEIGHT > v_handleOffset) {
                    v_viewmodel.setPosition(v_currentValue + v_customData.bigStep);
                } else {
                    v_viewmodel.setPosition(v_currentValue - v_customData.bigStep);
                }
            } else {
                var proportion = (clickTop - barTop - BUTTON_HEIGHT) / v_height;
                v_viewmodel.setPosition(Math.floor((v_numberOfValues - 1) * proportion));
            }
        });
        
        $('#' + v_mainId + '_handle').click(function(e) {
            if (v_customData.clickOnScrollBarBehaviour == "bigstep") {
                e.stopPropagation();
            }
        });
        
        $('#' + v_parentId).on('mousewheel', onScroll);
        $('#' + v_parentId).on('scroll', onHorizontalScroll);
        
        $('#' + v_mainId + '_handle').draggable({
            "start": function() {
                v_refreshEnabled = false;
            },
            "stop": function() {
                v_refreshEnabled = true;
            },
            "drag": function(event, ui) {
                if (ui.position.top < BUTTON_HEIGHT) {
                    ui.position.top = BUTTON_HEIGHT;
                } else if (ui.position.top > v_height + BUTTON_HEIGHT) {
                    ui.position.top = v_height + BUTTON_HEIGHT;
                }
                
                var proportion = (ui.position.top - BUTTON_HEIGHT) / v_height;
                v_viewmodel.setPosition(Math.floor((v_numberOfValues - 1) * proportion));
            },
            "axis": "y",
            "scroll": false
        });
        
        $('#' + v_mainId).on("remove", function () {
            $('#' + v_parentId).off('mousewheel', onScroll);
            $('#' + v_parentId).off('scroll', onHorizontalScroll);
        });
    }
}

CView_Scroll.getHelp = function() {
    return "A scroll view that can be used to scroll data. It will appear on the right side of its parent.";
}

CView_Scroll.expectsInterface = function() {
    return [
        {
            "mandatory": ["getRange", "getPosition", "setPosition"]
        },
        {
            "optional": ["getViewportSize"]
        }
    ];
}

CView_Scroll.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_Scroll",
        "type": "object",
        "properties": {
            "clickOnScrollBarBehaviour": {
                "description": "The behaviour of the scrollbar when clicking on it (default bigstep).",
                "type": "string",
                "enum": ["bigstep", "jumpto"],
                "default": "jumpto"
            },
            "bigStep": {
                "description": "How many lines does the bigstep scroll. If the value is negative, de scroll will be reversed (only when clicking on the scrollbar).",
                "type": "integer",
                "default": 10
            }
        },
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema);
    return schema;
}

//# sourceURL=WebApplicationFramework\Views\View_Scroll.js