// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CView_StatusLED(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";
    /** constructor */
    
    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_StatusLED)[0];
    var v_conditionViewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_StatusLED)[1];
    var v_mainId = p_mainId;
    var v_parentId = p_parentId;
    var v_customData = p_data;
    
    var v_label;
    var v_led = "";
    
    var v_this = this;
        
    this.applicationCreated = function() {
        $("#" + v_parentId).append(getHtml());
        if (v_customData.isElementLabelPresent) {
            v_label = ViewUtils.addLabel(v_mainId, v_customData.elementText);
        }
        $("#" + v_mainId).addClass("StatusLED");
    };
        
    this.refresh = function(p_fullRefresh) {
        if (v_viewmodel != undefined && ViewUtils.checkVisibility(v_conditionViewmodel, v_mainId)) {
            var dataObject = v_viewmodel.getListWithElementInfo();
            if (dataObject == undefined || dataObject.values[0] == undefined) {
                return;
            }
            
            if (p_fullRefresh && v_customData.isElementLabelPresent && v_customData.elementText == undefined) {
                v_label.text(dataObject.values[0].element);
            }
            
            var text = "";
            var str = dataObject.values[0].val;
            
            if (str == undefined) {
                $("#" + v_mainId + " > span").css("display", "none");
                $("#" + v_mainId).addClass("NoData");
            } else {
                $("#" + v_mainId + " > span").css("display", "");
                $("#" + v_mainId).removeClass("NoData");
            }
            
            if (str != undefined && str.substring != undefined && str.indexOf != undefined) {
                var strpos = str.indexOf("]"); 
                text = str.substring(strpos+1);
                var color = str.substring(str.indexOf(":")+1, strpos);
                if (v_led != color) {
                    v_led = color;
                    $("#" + v_mainId + " img").attr("src", getImg(v_led));
                }
            }
            
            if (v_customData.isLabelPresent !== false) {
                $("#" + v_mainId + "_label").text(v_customData.labelText == undefined ? text : v_customData.labelText);
            }
            
            ViewUtils.addTooltip(v_mainId, v_customData);
        }
    };

    function getHtml() {
        var html = '<div id="' + v_mainId + '"><span>';
        var img = '<img src="">';
        var label = '';
        
        if (v_customData.isLabelPresent !== false) {
            label += '<label id="' + v_mainId + '_label">';
            if (v_customData.labelText != undefined) {
                label += v_customData.labelText;
            }
            label += '</label>';
        }
        
        if (v_customData.alignment == "right") {
            html += label;
            html += img;
        } else {
            html += img;
            html += label;
        }
        
        html += '</span></div>';
        return html;
    }
    
    function getImg(ledColor) {
        var img = "";
        switch (ledColor) {
            case "blue":
                img = "WebApplicationFramework/Res/blueLED.png";
                break;
            case "black":
                img = "WebApplicationFramework/Res/blackLED.png";
                break;
            case "yellow":
                img = "WebApplicationFramework/Res/yellowLED.png";
                break;
            case "green":
                img = "WebApplicationFramework/Res/greenLED.png";
                break;
            case "red":
                img = "WebApplicationFramework/Res/redLED.png";
                break;
            case "spinning":
                img = "WebApplicationFramework/Res/waiting.gif";
                break;
        }
        return img;
    }
}

CView_StatusLED.getHelp = function() {
    return "Visual element for status LEDs";
};

CView_StatusLED.expectsInterface = function() {
    return [
        {
            "mandatory": ["getListWithElementInfo"]
        },
        {
            "optional": ["getState"]
        }
    ];
};

CView_StatusLED.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_StatusLED",
        "type": "object",
        "properties": {
            "isLabelPresent": {
                "type": "boolean",
                "format": "checkbox",
                "default": false,
                "description": "show status led as text (default true)"
            },
            "labelText": {
                "type": "string",
                "description": "show this text istead of the status led text"
            },
            "alignment": {
                "type": "string",
                "enum": ["left", "right"],
                "default": "right",
                "description": "the position of the image relative to the label (default left)"
            }
        },
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema, ViewUtils.commonElementSchema);
    return schema;
};
//# sourceURL=WebApplicationFramework\Views\View_StatusLED.js
