function CView_ValueScale(p_viewmodels, p_mainId, p_parentId, p_data) {
    "use strict";
    
    /** constructor */
    
    var v_mainId = p_mainId;
    var v_parentId = p_parentId;
    var v_customData = p_data;

    var defaultData = {
		stroke : "#4CAF50",
		strokeWidth : 7,
		radius : 28
    }
    if (v_customData.minimumValue == undefined) {
    	v_customData.minimumValue = 0;
    }
    if (v_customData.maximumValue == undefined) {
    	v_customData.maximumValue = 100;
    }
    if (v_customData.yellowZone == undefined) {
    	v_customData.yellowZone = 60;
    }
    if (v_customData.redZone == undefined) {
    	v_customData.redZone = 80;
    }

    var v_viewmodel = ViewUtils.getViewmodelsFromExpectedInterface(p_viewmodels, CView_ValueScale)[0];
    var v_this = this;

    /** public functions */

    this.applicationCreated = function() {
        $("#" + v_parentId).append(getHtml());
        if (v_customData.isElementLabelPresent) {
            ViewUtils.addLabel(v_mainId, v_customData.elementText);
        }
        if (v_customData.class != undefined) {
            $("#" + v_mainId).addClass(v_customData.class);
        } else {
            $("#" + v_mainId).addClass("ValueScale");
        }

    };

    this.refresh = function() {
        if (v_viewmodel != undefined) {
            var list = v_viewmodel.getList();
        	list.values[0][0] = (list.values[0][0]) / 100 * Math.random() + 20;
            updateCircle(list.values[0][0]);
        }
    };

    /** private functions */

    function getHtml() {
        return '<div id="' + v_mainId + '"><svg><path fill="none"' + defaultData.stroke + '" stroke-width="' + defaultData.strokeWidth + '"></path><text class="scalePercentage" x="0" y="21" fill="black"></text></svg><label class="scaleText"></label></div>';
    }

    function updateCircle(value) {
    	var percent = getPercetageFromValue(value);
        $("#" + v_mainId + " path").attr("d", getArc(percent));
        $("#" + v_mainId + " path").attr("stroke", calculateColor(percent));
        if (isNaN(percent)) {
        	$("#" + v_mainId + " .scalePercentage").text("--%");
        } else {
        	$("#" + v_mainId + " .scalePercentage").text(Math.floor(percent) + "%");
        	var currentWidth = $("#" + v_mainId).width() / 2 - 12;
          $("#" + v_mainId + " text").attr("x", currentWidth);
        }
    }

    function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
        var angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;

        return {
            x: centerX + (radius * Math.cos(angleInRadians)),
            y: centerY + (radius * Math.sin(angleInRadians))
        };
    }
    
    function getPercetageFromValue(value) {
    	var maxValue = v_customData.maximumValue;
    	if (v_customData.maximumValue > v_customData.minimumValue && v_customData.minimumValue >= 0 && v_customData.maximumValue >= 0 && v_customData.maximumValue >= value && v_customData.minimumValue <= value) {
    		value = value - v_customData.minimumValue;
    		maxValue = maxValue - v_customData.minimumValue
    	return (value / maxValue) * 100;
    	}
    }

    function getArc(percent){
        if (percent == 100.0) {
            percent = 99.99;
        }
        
        var radius = defaultData.radius;
        var startAngle = -60;
        var endAngle = 240.0 * percent / 100.0 / 2 - 60;
        
        var centerY = 33;
        var centerX = $("#" + v_mainId).width() / 2;
                
        var start = polarToCartesian(centerX, centerY, radius, endAngle);
        var end = polarToCartesian(centerX, centerY, radius, startAngle);

        var arcSweep = endAngle - startAngle <= 180 ? "0" : "1";
        var d = [
            "M", start.x, start.y, 
            "A", radius, radius, 0, arcSweep, 0, end.x, end.y
        ].join(" ");

        return d;       
    }
    
    function calculateColor(percent){
        var color;
        if (percent != undefined) {
        	color = "#4CAF50";
            if (percent > v_customData.redZone) {
            	color = "#DC143C";
            } else if (percent > v_customData.yellowZone) {
            	color = "#FFD700";
            }
        }
        return color;
    }
}

CView_ValueScale.getHelp = function() {
    return "A value scale view. Requires a list whose first element is a percentage. Optionally, non percentage numbers can be given, but minimum and maximum range should be defined in custom data. Defaults are minimum 0 and maximum 100.";
}

CView_ValueScale.expectsInterface = function() {
    return [
        {
            "mandatory": ["getList"]
        }
    ];
};

CView_ValueScale.getCustomDataSchema = function() {
    var schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CView_ValueScale",
        "type": "object",
        "properties": {
            "class": {
                "type": "string",
                "description": "the css class of the value scale bar"
            },
            "minimumValue": {
            	"type": "integer",
            	"description": "the value that will serve as the minimum on the scale",
            	"default": 0
            },
            "maximumValue": {
            	"type": "integer",
            	"description": "the value that will serve as the maximum on the scale",
            	"default": 100
            },
            "yellowZone": {
            	"type": "integer",
            	"description": "value in percent where the scale will turn yellow",
            	"default": 60
            },
            "redZone": {
            	"type": "integer",
            	"description": "value in percent where the scale will turn red",
            	"default": 80
            }
        },
        "additionalProperties": false
    };
    $.extend(true, schema, ViewUtils.commonViewSchema);
    return schema;
};
//# sourceURL=WebApplicationFramework\Views\View_ValueScale.js