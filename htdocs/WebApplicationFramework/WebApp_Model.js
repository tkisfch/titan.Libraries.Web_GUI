// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CWebApp_Model(p_fileHandler)
{
    "use strict";

    var v_fileHandler = p_fileHandler;
    var v_setupModel = new CSetup_Model(v_fileHandler);
    var v_mainConfig = new JsonLoader("CustomizableContent/MainConfig.json", v_fileHandler, {});
    var v_appConfig;
    var v_this = this;

    this.getFileHandler = function() {
        return v_fileHandler;
    };

    this.getSetupModel = function() {
        return v_setupModel;
    };

    this.loadMainConfig = function(callback) {
        loadConfig(v_mainConfig, callback);
    };

    this.saveMainConfig = function(data, callback) {
        saveConfig(v_mainConfig, data, callback);
    };

    this.getMainConfig = function() {
        return getConfig(v_mainConfig);
    };

    this.loadAppConfig = function(filename, callback) {
        v_appConfig = new JsonLoader(filename, v_fileHandler, {});
        loadConfig(v_appConfig, callback);
    };

    this.saveAppConfig = function(data, callback) {
        saveConfig(v_appConfig, data, callback);
    };

    this.getAppConfig = function() {
        return getConfig(v_appConfig);
    };

    function loadConfig(config, callback) {
        config.taskOperation(function(ok, data) {
            if (ok) {
                callback(data);
            } else {
                alert('Failed to load config: ' + config.getUrl());
                callback({});
            }
        });
    }

    function saveConfig(config, data, callback) {
        if (data) {
            config.setData(data);
        }
        config.save(undefined, callback);
    }

    function getConfig(config) {
        if (config != undefined) {
            return config.getData();
        } else {
            return {};
        }
    }
}