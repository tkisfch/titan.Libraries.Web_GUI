// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_Chart(p_viewmodel, p_options) {
    "use strict";

    /** constructor */
    
    var v_viewmodel = p_viewmodel;
    var v_options = p_options;
    
    var v_dataPaths = [];
    var v_visible = [];
        
    /** public functions - interface for parent */
    
    this.setSelectionToControl = function(p_selection) {};
    this.setBinder = function(p_binder) {};
    this.setReponseDataPath = function(p_index, p_path) {
        v_dataPaths[p_index] = p_path;
    };
	
    /** public functions - interface for views */
    
    this.getChartData = function() {
        return getDatasetsAndNavigationData();
    };
    
    this.toggleSeries = function(index) {
        if (v_visible[index] == undefined) {
            v_visible[index] = false;
        } else {
            v_visible[index] = !v_visible[index];
        }
    };
    
    this.isSeriesVisible = function(index) {
        return v_visible[index] == undefined || v_visible[index] == true;
    }
    
    /** private functions */
    
    function getTimelineContent(tp, stringContent) {
        if (tp == 1) {
            return parseInt(stringContent);
        } else if (tp == 2) {
            return parseFloat(stringContent);
        } else if (tp == 3) {
            return stringContent == "true";
        } else {
            return stringContent
        }
    }
    
    function addTimelineData(data, timeline) {
        try {
            timeline = JSON.parse(timeline);
            if (timeline.x.length != timeline.y.length) {
                return false
            } else {
                for (var i = 0; i < timeline.x.length; ++i) {
                    data.push([timeline.x[i] * 1000, getTimelineContent(timeline.tp, timeline.y[i])]);
                }
                return true;
            }
        } catch (e) {
            return false;
        }
    }
    
    function getDatasetsAndNavigationData() {
        var navigationData = {x: {pan: {min: -1, max: 1}, zoom: {min: 1, max: null}}, y: {pan: {min: -1, max: 1}, zoom: {min: 1, max: null}}};
        var dataSets = [];
        
        var counter = 0;
        for (var i = 0; i < v_dataPaths.length; ++i) {
            var response = v_viewmodel.getResponseElement(v_dataPaths[i]);
            if (response != undefined) {
                var request = v_viewmodel.getRequestFromPath(v_dataPaths[i]);
                if (request.getData.timeline != undefined && response.node != undefined) {
                    var dataSet = getEmptyDataSet(counter, request != undefined ? request.getData.element : "Series" + counter);
                    if (addTimelineData(dataSet.data, response.node.val)) {
                        dataSets.push(dataSet);
                        ++counter;
                    }
                } else if (response != undefined && response.list != undefined && request.getData.children != undefined) {
                    for (var j = 0; j < request.getData.children.length; ++j) {
                        if (request.getData.children[j].getData.timeline != undefined) {
                            for (var k = 0; k < response.list.length; ++k) {
                                if (response.list[k].node.childVals != undefined && response.list[k].node.childVals[j] != undefined) {
                                    var childResponse = response.list[k].node.childVals[j];
                                    var dataSet = getEmptyDataSet(counter, response.list[k].node.val);
                                    if (addTimelineData(dataSet.data, childResponse.node.val)) {
                                        dataSets.push(dataSet);
                                        ++counter;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if (dataSets.length != 0 && dataSets[0].data[0] != undefined) {
            navigationData.x.pan.min = dataSets[0].data[0][0] + 1;
            navigationData.y.pan.min = dataSets[0].data[0][1] + 1;
        }
        
        for (var i = 0; i < dataSets.length; ++i) {
            for (var j = 0; j < dataSets[i].data.length; ++j) {
                navigationData.x.pan.max = Math.max(navigationData.x.pan.max, dataSets[i].data[j][0] + 1);
                navigationData.x.pan.min = Math.min(navigationData.x.pan.min, dataSets[i].data[j][0] - 1);
                navigationData.y.pan.max = Math.max(navigationData.y.pan.max, dataSets[i].data[j][1] + 1);
                navigationData.y.pan.min = Math.min(navigationData.y.pan.min, dataSets[i].data[j][1] - 1);
            }
        }
        
        return {
            "dataSets": dataSets,
            "navigationRanges": navigationData
        }
    }
    
    function getEmptyDataSet(counter, altLabel) {
        var data;
        if (v_options.dataSet == undefined || v_options.dataSet[counter] == undefined) {
            data = {
                "label": altLabel,
                "data": [],
                "lines": {
                    "show": v_visible[counter] == undefined || v_visible[counter] == true
                }
            }
        } else {
            data = {
                "label": v_options.dataSet[counter].label,
                "data": [],
                "color": v_options.dataSet[counter].color,
                "highlightColor": v_options.dataSet[counter].highlightColor,
                "lines": {
                    "show": v_visible[counter] == undefined || v_visible[counter] == true
                }
            }
        }
        
        return data;
    }
    
}

CViewModel_Chart.getHelp = function() {
    return "The viewmodel creates the data for a timeseries chart.";
};

CViewModel_Chart.providesInterface = function() {
    return ["getChartData", "toggleSeries", "isSeriesVisible"];
};

CViewModel_Chart.getCustomDataSchema = function() {
    return {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CViewModel_Chart",
        "type": "object",
        "properties": {
            "dataSet": {
                "type": "array",
                "format": "tabs",
                "items": {
                    "type": "object",
                    "properties": {
                        "label": {
                            "type": "string",
                            "description": "If present, it will be used as the label of the data."
                        },
                        "color": {
                            "type": "string",
                            "description": "If present, it will be used as the color of the data.",
                            "pattern": "^#(?:[0-9a-fA-F]{3}){1,2}$"
                        },
                        "highlightColor": {
                            "type": "string",
                            "description": "If present, it will be used as the highlight color of the data.",
                            "pattern": "^#(?:[0-9a-fA-F]{3}){1,2}$"
                        }
                    },
                    "additionalProperties": false,
                    "required": ["label"]
                }
            }
        },
        "additionalProperties": false
    };
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_Chart.js