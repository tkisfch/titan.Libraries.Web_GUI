// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_Clients(p_viewmodel, p_options) {
    "use strict";

    /** constructor */
    
    var v_options = p_options;
    var base = new CViewModel_TableForLargeData(p_viewmodel, p_options);
    
    var v_flexAligner = new CViewModel_FlexAligner();
    
    var v_tableData = {
        "table": []
    };
    var v_header = [];
    var v_viewmodels = [];
    var v_firstSeparatorExists;
    
    /** public functions - interface for parent */ 

    this.setSelectionToControl = base.setSelectionToControl;
    this.setReponseDataPath = base.setReponseDataPath;
    this.setBinder = base.setBinder;
	
	/** public functions - interface for views */ 

    this.getViewmodelBundle = function() {
        var newTableData = base.getTable();
        var newHeader = base.getHeader();
        
        // in the check we use both v_tableData and newTableData
        var fullRefresh = checkIfFullRefreshNeeded(newTableData);
        v_tableData = newTableData;
        v_header = newHeader;
        updateViewmodels();
        
        return {
            "viewmodels": v_viewmodels,
            "fullRefresh": fullRefresh,
            "firstSeparatorExists": v_firstSeparatorExists
        };
    };
    
    this.getRange = base.getRange;
    this.getPosition = base.getPosition;
    this.setPosition = base.setPosition;
    this.getViewportSize = base.getViewportSize;
    this.isStreaming = base.isStreaming;

    /** private functions */
    
    function checkIfFullRefreshNeeded(newTableData) {
        if ((newTableData.separators == undefined && v_tableData.separators == undefined) || 
            (newTableData.separators != undefined && v_tableData.separators != undefined && newTableData.separators.length == v_tableData.separators.length)) {
            
            if (newTableData.separators != undefined && v_tableData.separators != undefined) {
                var columnIndexThatContainsTheSize = v_options.numberOfElementsInFirstAligner + v_options.numberOfElementsInSecondAligner;
                var limitForFirstSeparatorExistance;
                if (newTableData.separators.length > 1) {
                    limitForFirstSeparatorExistance = newTableData.separators[1].index;
                } else {
                    limitForFirstSeparatorExistance = newTableData.table.length;
                }
                var firstSeparatorExists = true;
                if (base.getPosition() != 0) {
                    for (var i = 0; i < limitForFirstSeparatorExistance; ++i) {
                        if (parseInt(newTableData.table[i][columnIndexThatContainsTheSize].val) != limitForFirstSeparatorExistance) {
                            firstSeparatorExists = false;
                            break;
                        }
                    }
                }
                if (firstSeparatorExists != v_firstSeparatorExists) {
                    v_firstSeparatorExists = firstSeparatorExists;
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    
    function updateViewmodels() {
        // create or delete viewmodels so we have the correct number of them
        var numberOfAdditionalViewmodels = v_tableData.separators.length - v_viewmodels.length;
        if (numberOfAdditionalViewmodels > 0) {
            for (var i = 0; i < numberOfAdditionalViewmodels; ++i) {
                v_viewmodels.push({
                    "aligner1": new AlignerViewmodel(),
                    "aligner2": new AlignerViewmodel(),
                    "table": new TableViewmodel()
                });
            }
        } else if (numberOfAdditionalViewmodels < 0) {
            for (var i = 0; i > numberOfAdditionalViewmodels; --i) {
                v_viewmodels.pop();
            }
        }
        
        // update the viewmodels, we now know that v_viewmodels.length == v_tableData.separators.length
        v_tableData.separators.push({"index": v_tableData.table.length}); // a small trick so we avoid conditions below
        for (var i = 0; i < v_viewmodels.length; ++i) {
            v_viewmodels[i].aligner1.setData(v_tableData.separators[i].index, 0, v_options.numberOfElementsInFirstAligner);
            v_viewmodels[i].aligner2.setData(v_tableData.separators[i].index, v_options.numberOfElementsInFirstAligner, v_options.numberOfElementsInSecondAligner);
            v_viewmodels[i].table.setData(v_tableData.separators[i].index, v_options.numberOfElementsInFirstAligner + v_options.numberOfElementsInSecondAligner + 1, v_tableData.separators[i + 1].index - v_tableData.separators[i].index);
        }
        v_tableData.separators.pop();
    }
    
    /** private Classes */
    
    // The mocked viewmodel for ElementRelay used in the element aligners
    function AlignerViewmodel() {
        var v_row;
        var v_col;
        var v_columnCount;
        
        this.setData = function(p_row, p_col, p_columnCount) {
            v_row = p_row;
            v_col = p_col;
            v_columnCount = p_columnCount;
        };
        
        this.select = function() {};
        
        this.getListWithElementInfo = function() {
            var list = [];
            for (var i = 0; i < v_columnCount; ++i) {
                list.push({
                    "element": v_header[v_col + i].heading,
                    "val": v_tableData.table[v_row][v_col + i].val,
                    "isWritable": v_tableData.table[v_row][v_col + i].isWritable,
                });
            }
            return {
                "values": list,
                "selections": []
            }
        };
        
        this.getList = function() {
            var list = [];
            for (var i = 0; i < v_columnCount; ++i) {
                list.push([
                    [v_tableData.table[v_row][v_col + i].val, undefined, v_tableData.table[v_row][v_col + i].isWritable]
                ]);
            }
            return list;
        };
        
        this.setValue = function(dataPathIndex, value) {
            base.setValue(v_row, v_col + dataPathIndex, value);
        };
        
        this.getChildPercentagesFromFlex = v_flexAligner.getChildPercentagesFromFlex;
    }
    
    // The mocked viewmodel for DynamicTable used in the tables
    function TableViewmodel() {
        var v_row;
        var v_col;
        var v_rowCount;
        
        this.setData = function(p_row, p_col, p_rowCount) {
            v_row = p_row;
            v_col = p_col;
            v_rowCount = p_rowCount;
        };
        
        this.select = function() {};
        
        this.getHeader = function() {
            var header = mcopy(v_header.slice(v_col));
            for (var i = 0; i < header.length; ++i) {
                header[i].elementIndex = i;
            }
            return header;
        };
        
        this.getName = function() {
            return v_header[v_col].heading;
        };
        
        this.getTable = function() {
            var tableData = [];
            for (var i = 0; i < v_rowCount; ++i) {
                tableData.push(v_tableData.table[v_row + i].slice(v_col));
            }
            return {"table": tableData};
        };
        
        this.setValue = function(row, col, value) {
            base.setValue(v_row + row, v_col + col, value);
        };
        
        this.getChildPercentagesFromFlex = v_flexAligner.getChildPercentagesFromFlex;
    }
}

CViewModel_Clients.getHelp = function() {
    return "Viewmodel for the clients tab. Will return a list whose elements contain 3 viewmodels: 2 for aligners and one for a table.\n" + 
        "A data connection for the possible size of the tables must precede the connection representing the 'iterator' of the table.";
};

CViewModel_Clients.providesInterface = function() {
    return ["getViewmodelBundle", "getRange", "getPosition", "setPosition", "getViewportSize", "isStreaming"];
};

CViewModel_Clients.getCustomDataSchema = function() {
    var outerTableSchema = CViewModel_TableForLargeData.getCustomDataSchema();
    
    outerTableSchema.title = "Custom data for CViewModel_Clients";
    outerTableSchema.properties["numberOfElementsInFirstAligner"] = {
        "type": "integer"
    };
    outerTableSchema.properties["numberOfElementsInSecondAligner"] = {
        "type": "integer"
    };
    // Why does outerTableSchema.properties.name = undefined not work???
    delete outerTableSchema.properties.name;
    
    if (outerTableSchema.required == undefined) {
        outerTableSchema.required = []
    }
    outerTableSchema.required.splice(0, 0, "separateBy", "numberOfElementsInFirstAligner", "numberOfElementsInSecondAligner");
    
    return outerTableSchema;
};

CViewModel_Clients.expectsConnection = function() {
    return CViewModel_TableForLargeData.expectsConnection();
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_Clients.js