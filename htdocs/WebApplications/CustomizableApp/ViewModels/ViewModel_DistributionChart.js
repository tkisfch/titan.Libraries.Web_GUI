// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_DistributionChart(p_viewmodel, p_options) {
    "use strict";

    /** constructor */
    
    var v_viewmodel = p_viewmodel;
    var v_options = p_options;
    
    var v_dataPaths = [];
    var v_visible = [];
        
    /** public functions - interface for parent */
    
    this.setSelectionToControl = function(p_selection) {};
    this.setBinder = function(p_binder) {};
    this.setReponseDataPath = function(p_index, p_path) {
        v_dataPaths[p_index] = p_path;
    };
	
    /** public functions - interface for views */
    
    this.getChartData = function() {
        var data = getDatasetsAndNavigationData();
        data.ticks = getTicks();
        return data;
    };
    
    this.toggleSeries = function(index) {
        if (v_visible[index] == undefined) {
            v_visible[index] = false;
        } else {
            v_visible[index] = !v_visible[index];
        }
    };
    
    this.isSeriesVisible = function(index) {
        return v_visible[index] == undefined || v_visible[index] == true;
    }
    
    /** private functions */
    
    function getTicks() {
        var list = [];
        if (v_options.intervallimits != undefined) {
            list = v_options.intervallimits;
        } else {
            var response = v_viewmodel.getResponseElement(v_dataPaths[0]);
            if (response != undefined && response.list != undefined) {
                for (var i = 0; i < response.list.length; ++i) {
                    list[i] = response.list[i].node.val;
                }
            }
        }
        
        var listToReturn = [];
        if (list.length > 0) {
            listToReturn[0] = [1, "< " + list[0]];
            listToReturn[list.length] = [list.length + 1, list[list.length - 1] + " <"];
            for (var i = 0; i < list.length - 1; ++i) {
                listToReturn[i + 1] = [i + 2, list[i] + " - " + list[i + 1]];
            }
        }
        
        return listToReturn;
    }
    
    function getDatasetsAndNavigationData() {
        var navigationData = {x: {pan: {min: -1, max: 1}, zoom: {min: 1, max: null}}, y: {pan: {min: -1, max: 1}, zoom: {min: 1, max: null}}};
        var dataSets = [];
        
        var dataPathIndexOfValue = 0;
        if (v_options.intervallimits == undefined && v_dataPaths.length > 1) {
             dataPathIndexOfValue = 1;
        }
        
        var counter = 0;
        var response = v_viewmodel.getResponseElement(v_dataPaths[dataPathIndexOfValue]);
        if (response != undefined && response.list != undefined) {
            var request = v_viewmodel.getRequestFromPath(v_dataPaths[dataPathIndexOfValue]);
            var dataSet = getEmptyDataSet(counter, request != undefined ? request.getData.element : "Series" + counter);

            for (var j = 0; j < response.list.length; ++j) {
                dataSet.data.push([j + 1, response.list[j].node.val]);
            }
            dataSets.push(dataSet);
        }
        
        for (var i = 0; i < dataSets.length; ++i) {
            navigationData.x.pan.max = Math.max(navigationData.x.pan.max, dataSets[i].data.length + 1);
            for (var j = 0; j < dataSets[i].data.length; ++j) {
                navigationData.y.pan.max = Math.max(navigationData.y.pan.max, dataSets[i].data[j][1] + 1);
            }
            dataSets[i].bars.barWidth = 0.8 / dataSets.length;
        }
        
        return {
            "dataSets": dataSets,
            "navigationRanges": navigationData
        }
    }
    
    function getEmptyDataSet(counter, altLabel) {
        var data;
        if (v_options.dataSet == undefined || v_options.dataSet[counter] == undefined) {
            data = {
                "label": altLabel,
                "data": [],
                "lines": {
                    "show": false
                },
                "bars": {
                    "show": v_visible[counter] == undefined || v_visible[counter] == true,
                    "barWidth": 0
                }
            }
        } else {
            data = {
                "label": v_options.dataSet[counter].label,
                "data": [],
                "color": v_options.dataSet[counter].color,
                "highlightColor": v_options.dataSet[counter].highlightColor,
                "lines": {
                    "show": false
                },
                "bars": {
                    "show": v_visible[counter] == undefined || v_visible[counter] == true,
                    "barWidth": 0
                }
            }
        }
        
        return data;
    }
    
}

CViewModel_DistributionChart.getHelp = function() {
    return "The viewmodel creates the data and labels for a distribution chart.\n" +
        "Both the labels and values can come from a dataElement.\n" +
        "If the custom data does not contain the intervallimits, the first data connection will be used to determine them.";
};

CViewModel_DistributionChart.providesInterface = function() {
    return ["getChartData", "toggleSeries", "isSeriesVisible"];
};

CViewModel_DistributionChart.getCustomDataSchema = function() {
    return {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CViewModel_DistributionChart",
        "type": "object",
        "properties": {
            "intervallimits": {
                "description": "The predefined values for the interval limits of the distribution chart.",
                "type": "array",
                "format": "table",
                "items": {
                    "type": "number",
                    "title": "limit"
                }
            },
            "dataSet": {
                "type": "array",
                "format": "tabs",
                "items": {
                    "type": "object",
                    "properties": {
                        "label": {
                            "type": "string",
                            "description": "If present, it will be used as the label of the data."
                        },
                        "color": {
                            "type": "string",
                            "description": "If present, it will be used as the color of the data.",
                            "pattern": "^#(?:[0-9a-fA-F]{3}){1,2}$"
                        },
                        "highlightColor": {
                            "type": "string",
                            "description": "If present, it will be used as the highlight color of the data.",
                            "pattern": "^#(?:[0-9a-fA-F]{3}){1,2}$"
                        }
                    },
                    "additionalProperties": false,
                    "required": ["label"]
                }
            }
        },
        "additionalProperties": false
    };
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_DistributionChart.js