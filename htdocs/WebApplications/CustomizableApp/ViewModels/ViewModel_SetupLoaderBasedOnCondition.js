// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_SetupLoaderBasedOnCondition(p_viewmodel, p_options) {
    "use strict";

    /** constructor */
    
    var base = new CViewModel_Condition(p_viewmodel, p_options);
    var v_viewmodel = p_viewmodel;
    var v_options = p_options;
    
    var v_dataPaths = [];
    
    /** public functions - interface for parent */
    
    this.setSelectionToControl = base.setSelectionToControl;
    this.setReponseDataPath = base.setReponseDataPath;
    this.setBinder = base.setBinder;
	
    /** private functions - interface for views */ 
    
    function loadSetup() {
        if (v_viewmodel.isRunning()) {
            var value = base.getState();
            if (value) {
                clearInterval(timer);
                v_viewmodel.loadSetup(v_options.setupToLoad, undefined, true);
            }
        } else {
            clearInterval(timer);
        }
    };
    
    var timer = setInterval(loadSetup, 50);
}

CViewModel_SetupLoaderBasedOnCondition.getHelp = function() {
    return "A viewmodel that loads a setup given in its custom data when a condition is met.";
}

CViewModel_SetupLoaderBasedOnCondition.providesInterface = function() {
    return [];
};

CViewModel_SetupLoaderBasedOnCondition.getCustomDataSchema = function() {
    var schema = CViewModel_Condition.getCustomDataSchema();
    schema.properties.setupToLoad = {
        "description": "The name of the setup to load",
        "type": "string"
    };
    schema.required = ["setupToLoad"];
    return schema;
};

//# sourceURL=CustomizableApp\ViewModels\CViewModel_SetupLoaderBasedOnCondition.js