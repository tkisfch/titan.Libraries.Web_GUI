// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_UnionTable(aViewModel, aOptions) {
    "use strict";

    /** private members */
    var mViewModel = aViewModel;
    var mOptions = aOptions;
    //var mRq = mViewModel.getRequest();
    var mBinder;
    var mReponseDataPaths = [];
    var mSelections = []; // mSelectionss is a list of references to objects, each containing a field named "selection"
    var mThis = this;
    var mEnlistElementName = mOptions.enlistElementName;
    if (mEnlistElementName === undefined)
        mEnlistElementName = true;

    /** public functions - Interface for parent */

    this.setSelectionToControl = function (aSelection) {
        /** aSelection is a reference to an object containing a "selection" field */
        mSelections.push(aSelection);
    };

    this.setReponseDataPath = function (aExpectedReponseDataIndex, aReponseDataPath) {
        mReponseDataPaths[aExpectedReponseDataIndex] = aReponseDataPath;
    };

    this.setBinder = function (aBinder) {
        mBinder = aBinder;
    };

    /** public functions - Interface for views */
    this.select = function (aIndex) {
        for (var i = 0; i < mSelections.length; ++i)
            mViewModel.select(mSelections[i], aIndex);
        mBinder.notifyChange();
    };

    this.getHeader = function () {
        var header = [];
        if (mOptions.header) {
            header = mcopy(mOptions.header);
        } else {
            var request = (mViewModel.getRequestFromPath())[mReponseDataPaths[0][0]];
            if (mEnlistElementName) {
                for(var i = 1; i < mReponseDataPaths[0].length; i++) { //for parents
                   header.push(request.getData.element);
                   request = request.getData.children[mReponseDataPaths[0][i]];
                }
                header.push(request.getData.element); //for connected node (data connection)
            }
            if (request.getData.children) { // for children
                for (var i = 0; i < request.getData.children.length; ++i) {
                    header.push(request.getData.children[i].getData.element);
                }
            }
        }
        
        for (var i = 0; i < header.length; ++i) {
            header[i] = {
                "heading": header[i],
                "elementIndex": i
            };
        }
        return header;
    };

    this.getName = function () {
        return mViewModel.getRequestFromPath(mReponseDataPaths[0]).getData.element;
    };

    this.getResponseElement = function (aReponseDataPath, aFullTableSelection) {
        var lElement;
        if (aFullTableSelection) {
            aFullTableSelection.parents = [];
            aFullTableSelection.depth = aReponseDataPath.length;
            aFullTableSelection.path = aReponseDataPath;
        }
        if (aReponseDataPath) {
            lElement = mViewModel.getResponseElement();
            if (lElement != undefined) {
                lElement = lElement[aReponseDataPath[0]];
            }
            if (lElement) {
                var lRq = mViewModel.getRequest()[aReponseDataPath[0]];
                for (var i = 1; i < aReponseDataPath.length && lElement; ++i) {
                    if (lElement.list && lRq.getData.selection && lRq.getData.selection.length > 0) {
                        if (aFullTableSelection) {
                            aFullTableSelection.depth--;
                            if (lElement.list[lRq.getData.selection[0]] != undefined) {
                                aFullTableSelection.parents.push(lElement.list[lRq.getData.selection[0]].node.val);
                            } else {
                                aFullTableSelection.parents.push(undefined);
                            }
                            aFullTableSelection.path = aFullTableSelection.path.slice(1);
                        }
                        if (lElement.list[lRq.getData.selection[0]] && lElement.list[lRq.getData.selection[0]].node.childVals) {
                            lElement = lElement.list[lRq.getData.selection[0]].node.childVals[aReponseDataPath[i]];
                        } else {
                            lElement = undefined;
                            break;
                        }
                    } else if (!lRq.getData.selection || lRq.getData.selection.length === 0) {
                        /**/
                    } else if (lElement.node && lElement.node.childVals) {
                        lElement = lElement.node.childVals[aReponseDataPath[i]];
                    } else
                        lElement = {
                            "error" : "cannot determine node"
                        };
                    if (lRq.getData.children)
                        lRq = lRq.getData.children[aReponseDataPath[i]];
                }
            }
        }
        return lElement;
    };

    this.getTable = function () {
        var aFullTableSelection = {};
        var response = mThis.getResponseElement(mReponseDataPaths[0], aFullTableSelection);
        return {
            selection : mSelections[0] ? mSelections[0].selection : undefined,
            table :  (response === undefined) ? [] : flattenResponseElement(response.list, aFullTableSelection.depth, 0, aFullTableSelection.parents, aFullTableSelection.path.slice(1))
        };
    };
    
    this.setValue = function() {};

    /** private functions */

    function flattenResponseElement(arr, limit, depth, parents, path) {
        if (arr) {
        limit = limit || 0;
        depth = depth || 0;
        parents = parents || [];
        path = path || [];
        return arr.reduce(function _reduce(flat, toFlatten) {
            if (limit === depth && toFlatten.node)
                return flat.concat({"val": toFlatten.node.val});
            else if (limit === depth && toFlatten.list)
                return flat.concat([flattenResponseElement(toFlatten.list, limit, depth, parents, path)].join());
            else if (limit === depth + 1 && toFlatten.node) {
                if (mEnlistElementName)
                    return flat.concat([parents.concat([{"val": toFlatten.node.val}].concat(flattenResponseElement(toFlatten.node.childVals, limit, depth + 1, [], path)))]);
                else
                    return flat.concat([flattenResponseElement(toFlatten.node.childVals, limit, depth + 1, [], path)]);
            } else if (Array.isArray(toFlatten))
                return flat.concat(flattenResponseElement(toFlatten, limit, depth, parents, path));
            else if (toFlatten.node && toFlatten.node.childVals && limit !== depth)
                return flat.concat(flattenResponseElement([toFlatten.node.childVals[path[depth]]], limit, depth + 1, parents.concat([{"val": toFlatten.node.val}]), path));
            else if (toFlatten.node && toFlatten.node.childVals)
                return flat.concat(flattenResponseElement(toFlatten.node.childVals, limit, depth + 1, parents.concat([{"val": toFlatten.node.val}]), path));
            else if (toFlatten.list)
                return flat.concat(flattenResponseElement(toFlatten.list, limit, depth, parents, path));
            else
                console.error("flattenResponseElement met an unexpected condition. Reason: The response object is nonconformant OR not all parents have a selection!");
        }, []);
        }
    }
}

CViewModel_UnionTable.getHelp = function() {
    return "A viewmodel that creates a merged table when there are iterators above the normal table-like request structure.\n" +
        "The select and setValue methods are not implemented.";
}

CViewModel_UnionTable.providesInterface = function () {
    return ["select", "getName", "getHeader", "getTable", "setValue"];
};

CViewModel_UnionTable.getCustomDataSchema = function () {
    return {
        "$schema" : "http://json-schema.org/draft-04/schema#",
        "title" : "Custom data for CViewModel_UnionTable",
        "type" : "object",
        "properties" : {
            "header": {
                "description": "The header of the table. It should have as many elements as the number of columns.",
                "type": "array",
                "format": "table",
                "items": {
                    "type": "string",
                    "title": "heading"
                }
            },
            "enlistElementName": {
                "description": "Whether we add the parents to the table.",
                "type": "boolean",
                "format": "checkbox",
                "default": false
            }
        }
    };
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_UnionTable.js
