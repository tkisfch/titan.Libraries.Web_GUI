// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_WritableCondition(p_viewmodel, p_options) {
    "use strict";

    var base = new CViewModel_Condition(p_viewmodel, p_options);
    base.isWritable = base.getState;
    base.getState = undefined;
    return base;
}

CViewModel_WritableCondition.getHelp = function() {
    return "This viewmodel is the same as the condition viewmodel, but its interface is called differently.";
}

CViewModel_WritableCondition.providesInterface = function() {
    return ["isWritable"];
};

CViewModel_WritableCondition.getCustomDataSchema = function() {
    return CViewModel_Condition.getCustomDataSchema();
};

//# sourceURL=CustomizableApp\ViewModels\CViewModel_WritableCondition.js