// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_ScrollForRangeFilter(p_viewmodel, p_options) {
    "use strict";

    /** constructor */
    
    var base = new CViewModel_Paging(p_viewmodel, p_options);
    
    var v_viewmodel = p_viewmodel;
    var v_options = p_options;
    
    /** public functions - interface for parent */
    
    this.setSelectionToControl = base.setSelectionToControl;
    this.setReponseDataPath = base.setReponseDataPath;
    this.setBinder = base.setBinder;
	
	/** public functions - interface for views */
    
    this.getRange = function() {
        var size = base.getSize();
        var max = size;
        if (v_options.allowScrollBelow == false) {
            max = max - base.getRangeFilter().count + 1;
        }
        return {
            "min": 0,
            "max": max
        };
    };
    
    this.getPosition = function() {
        return base.getRangeFilter().offset;
    };
    
    this.setPosition = function(position) {
        return base.changeFilter({"offset": {"to": position}});
    };
    
    this.getViewportSize = function() {
        return base.getRangeFilter().count;
    };
}

CViewModel_ScrollForRangeFilter.getHelp = function() {
    return "A viewmodel that can be used with a scrollbar to alter the rangeFilter of a request.";
}

CViewModel_ScrollForRangeFilter.providesInterface = function() {
    return ["getRange", "getPosition", "setPosition", "getViewportSize"];
};

CViewModel_ScrollForRangeFilter.getCustomDataSchema = function() {
    var schema = CViewModel_Paging.getCustomDataSchema();
    schema.title = "Custom data for CViewModel_ScrollForRangeFilter";
    return schema;
};

CViewModel_ScrollForRangeFilter.expectsConnection = function() {
    return CViewModel_Paging.expectsConnection();
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_ScrollForRangeFilter.js
