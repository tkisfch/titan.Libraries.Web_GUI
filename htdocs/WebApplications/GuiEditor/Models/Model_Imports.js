// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_Imports_Model(parsedData) {
    "use strict";

    var v_imports = parsedData;

    this.getSetupName = function() {
        return v_imports["setupName"];
    };

    this.setSetup = function(p_setup) {
        v_imports["setupName"] = p_setup;
    };

    this.setRequestPath = function(p_path) {
        v_imports["requetsPath"] = p_path;
    };

    this.getRequestPath = function() {
        return v_imports["requetsPath"];
    };

    this.getParentId = function() {
        return v_imports.parentID;
    };

    this.setParentId = function(p_id) {
        v_imports.parentID = p_id;
    };

    this.getSetupParams = function() {
        if (v_imports.setupParams == undefined) {
            return {};
        } else {
            return v_imports.setupParams;
        }
    };

    this.addSetupParam = function(name, value) {
        if (v_imports.setupParams == undefined) {
            v_imports.setupParams = {};
        }
        v_imports.setupParams[name] = value;
    };

    this.removeSetupParam = function(name) {
        if (v_imports.setupParams != undefined) {
            v_imports.setupParams[name] = undefined;
        }

        if (Object.keys(v_imports.setupParams).length == 0) {
            v_imports.setupParams = undefined;
        }
    };

    this.getDescriptorCopy = function() {
        return mcopy(v_imports);
    };

    this.updatePaths = function(prefix, amount) {
        var connection = v_imports["requetsPath"];
        if (connection != undefined) {
            if (amount == -1 && hasPrefix(connection, prefix)) {
                v_imports["requetsPath"] = undefined;
                return true;
            }
            var pos = prefix.pop();
            updateConnection(connection, prefix, pos, amount);
            return false;
        } else {
            return false;
        }
    };

    function updateConnection(connection, prefix, pos, amount) {
        if (hasPrefix(connection, prefix) && connection[prefix.length] != undefined && connection[prefix.length] >= pos) {
            connection[prefix.length] += amount;
        }
    }

    this.pathsMoved = function(fromPrefix, fromPrefixStr, toPrefix, toPrefixStr) {
        var connection = v_imports["requetsPath"];
        if (connection != undefined) {
            var origSiblingPrefix = mcopy(fromPrefix);
            var origPosition = origSiblingPrefix.pop();

            // This is the trick... we have to update the toPrefix, since we removed the node in fromPrefix
            updateConnection(toPrefix, origSiblingPrefix, origPosition, -1);

            var newSiblingPrefix = mcopy(toPrefix);
            var newPosition = newSiblingPrefix.pop();
            
            if (hasPrefix(connection, fromPrefix)) {
                connection.splice(0, fromPrefix.length);
                for (var i = toPrefix.length - 1; i >= 0; --i) {
                    connection.unshift(toPrefix[i]);
                }
            } else {
                updateConnection(connection, origSiblingPrefix, origPosition, -1);
                updateConnection(connection, newSiblingPrefix, newPosition, 1);
            }
        }
    };
}
//# sourceURL=GuiEditor\Models\Model_Imports.js
