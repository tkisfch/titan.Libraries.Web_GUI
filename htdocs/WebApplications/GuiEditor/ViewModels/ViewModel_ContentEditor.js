// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_ContentEditor_ViewModel(p_model, p_sanityChecker) {

    var v_model = p_model;
    var v_sanityChecker = p_sanityChecker;
    var v_setup;

    // we will store the opened FileLoaders in a hashmap whose keys will be the div ids
    var v_fileLoaders = {};

    // we also store the name-id pairs of the editors, so we can easily check whether a file is already open
    var v_nameIds = {};

    var v_viewmodelCounter = -1;
    var v_viewCounter = -1;

    this.setSetup = function(setup) {
        v_setup = setup;
    };

    this.getFileName = function(id) {
        return v_fileLoaders[id]["name"];
    };

    this.isSaveable = function(id) {
        return v_fileLoaders[id]["saveable"];
    };

    this.isEdited = function(id) {
        return v_fileLoaders[id]["edited"];
    };

    this.save = function(p_id, callback) {
        var id = p_id;
        function saved(ok) {
            if (ok) {
                v_fileLoaders[id]["edited"] = false;
                var url = v_fileLoaders[id]["loader"].getUrl();
                if (url.endsWith(".js")) {
                    v_sanityChecker.addFile(url);
                }
            }
            callback(ok, id);
        }

        v_fileLoaders[id]["loader"].save(undefined, undefined, saved);
    };

    function saveAs(p_id, name, url, p_callback) {
        var id = p_id;
        function saved(ok) {
            if (ok) {
                v_fileLoaders[id]["saveable"] = true;
                v_fileLoaders[id]["edited"] = false;
                v_fileLoaders[id]["name"] = name;
                if (url.endsWith(".js")) {
                    v_sanityChecker.addFile(url);
                }
            }
            p_callback(ok, id);
        }

        v_fileLoaders[id]["loader"].save(undefined, url, saved);
    }

    function switchId(name, id) {
        for (var key in v_nameIds) {
            if (v_nameIds.hasOwnProperty(key) && v_nameIds[key] == id) {
                v_nameIds[key] = undefined;
                v_nameIds[name] = id;
                break;
            }
        }
    }

    this.saveViewAs = function(id, name, callback) {

        var existingId = v_nameIds[name];
        if (existingId == id) {
            existingId = undefied;
        }

        function saved(ok) {
            if (ok) {
                switchId(name, id);
                callback(ok, id, existingId);
            } else {
                callback(ok);
            }
        }

        function SaveTask(p_id, p_url) {
            var id = p_id;
            var url = p_url;
            this.taskOperation = function(p_callback) {
                saveAs(id, name, url, p_callback);
            }
        }

        var taskList = new TaskList([
            new SaveTask(id + "_Html", v_model.getViewUrl(name + ".html")),
            new SaveTask(id + "_Css", v_model.getViewUrl(name + ".css")),
            new SaveTask(id + "_Js", v_model.getViewUrl(name + ".js"))
        ], saved);
        taskList.taskOperation();
    };

    this.saveViewmodelAs = function(id, name, callback) {

        if (!name.endsWith(".js")) {
            name = name + ".js";
        }
        var url = v_model.getViewmodelUrl(name);

        var existingId = v_nameIds[name];
        if (existingId == id) {
            existingId = undefied;
        }

        function saved(ok) {
            if (ok) {
                switchId(name, id);
                callback(ok, id, existingId);
            } else {
                callback(ok);
            }
        }

        saveAs(id, name, url, saved);
    };

    this.viewExists = function(name, callback) {
        v_model.viewExists(name, callback);
    };

    this.viewmodelExists = function(name, callback) {
        v_model.viewmodelExists(name + ".js", callback);
    };

    this.editorClosed = function(id) {
        for (var key in v_fileLoaders) {
            if (v_fileLoaders.hasOwnProperty(key) && key.startsWith(id)) {
                v_fileLoaders[key] = undefined;
            }
        }

        for (var name in v_nameIds) {
            if (v_nameIds.hasOwnProperty(name) && v_nameIds[name] != undefined && v_nameIds[name].startsWith(id)) {
                v_nameIds[name] = undefined;
            }
        }
    };

    this.loadViewContent = function(viewName, callback) {
        loadView(false, viewName, callback);
    };

    this.loadViewTemplate = function(viewName, callback) {
        loadView(true, viewName, callback);
    }

    function loadView(template, viewName, callback) {
        if (v_nameIds[viewName] != undefined) {
            callback(v_nameIds[viewName], viewName, "exists");
            return;
        }

        var url = viewName;
        if (!template) {
            url = v_model.getViewUrl(viewName);
        }

        ++v_viewCounter;
        var id = "GuiEditor_ViewContentEditor_File_" + v_viewCounter;

        var saveable = !template;

        function LoadTask(id, name, url) {
            var v_id = id;
            var v_name = name;
            var v_url = url;

            this.taskOperation = function(callback) {
                loadContent(v_id, v_name, saveable, v_url, callback);
            }
        }

        function contentAdded() {
            v_nameIds[viewName] = id;
            callback(id, viewName, "load");
        }

        var taskList = new TaskList([
            new LoadTask(id + "_Html", viewName + ".html", url + ".html"),
            new LoadTask(id + "_Css", viewName + ".css", url + ".css"),
            new LoadTask(id + "_Js", viewName + ".js", url + ".js")
        ], contentAdded);
        taskList.taskOperation();
    }

    this.loadViewmodelContent = function(viewmodelName, callback) {
        loadViewmodel(false, viewmodelName, callback);
    };

    this.loadViewmodelTemplate = function(viewmodelName, callback) {
        loadViewmodel(true, viewmodelName, callback);
    };

    function loadViewmodel(template, viewmodelName, callback) {
        if (!viewmodelName.endsWith(".js")) {
            viewmodelName += ".js";
        }

        if (v_nameIds[viewmodelName] != undefined) {
            callback(v_nameIds[viewmodelName], viewmodelName, "exists");
            return;
        }

        var url = viewmodelName;
        if (!template) {
            url = v_model.getViewmodelUrl(viewmodelName);
        }

        ++v_viewmodelCounter;
        var id = "GuiEditor_ViewmodelContentEditor_File_" + v_viewmodelCounter + "_Js";

        var saveable = !template;

        function contentAdded() {
            v_nameIds[viewmodelName] = id;
            callback(id, viewmodelName, "load");
        }

        loadContent(id, name, saveable, url, contentAdded);
    }

    function createContentOptionsFromList(list) {
        var result = [];
        for (var i = 0; i < list.length; ++i) {
            result.push({
                "value": list[i],
                "text": list[i]
            })
        }
        v_model.sortOptions(result);
        return result;
    }

    function createTemplateOptionsFromList(list) {
        var result = [];
        for (var i = 0; i < list.length; ++i) {
            result.push({
                "value": list[i].substring(0, list[i].lastIndexOf(".")),
                "text": list[i].substring(list[i].lastIndexOf("/") + 1, list[i].lastIndexOf("."))
            })
        }
        v_model.sortOptions(result);
        return result;
    }

    this.listViewTemplates = function(callback) {
        v_model.listViews(function(files) {
            callback(createTemplateOptionsFromList(files));
        })
    };

    this.listViewmodelTemplates = function(callback) {
        v_model.listViewmodels(function(files) {
            callback(createTemplateOptionsFromList(files));
        })
    };

    this.listViews = function(callback) {
        v_model.listCustomViews(function(views) {
            callback(createContentOptionsFromList(views));
        });
    };

    this.listViewmodels = function(callback) {
        v_model.listCustomViewmodels(function(viewmodels) {
            callback(createContentOptionsFromList(viewmodels));
        });
    };

    function addNewContent(id, name) {
        v_fileLoaders[id] = {
            "loader": new FileLoader(undefined, v_model.getFileHandler()),
            "saveable": false,
            "edited": true,
            "name": name
        }
    }

    function loadContent(id, name, saveable, url, callback) {
        v_fileLoaders[id] = {
            "loader": new FileLoader(url, v_model.getFileHandler()),
            "saveable": saveable,
            "edited": false,
            "name": name
        }
        v_fileLoaders[id]["loader"].taskOperation(callback);
    }

    this.deleteView = function(name, callback) {
        function deleted(ok) {
            var id = v_nameIds[name];
            if (ok && id != undefined) {
                v_fileLoaders[id + "_Html"]["saveable"] = false;
                v_fileLoaders[id + "_Css"]["saveable"] = false;
                v_fileLoaders[id + "_Js"]["saveable"] = false;
            }

            callback(ok, id);
        }

        function DeleteTask(p_name) {
            var v_name = p_name;
            this.taskOperation = function(p_callback) {
                v_model.deleteFile(v_name, p_callback);
            }
        }

        var taskList = new TaskList([
            new DeleteTask(v_model.getViewUrl(name + ".html")),
            new DeleteTask(v_model.getViewUrl(name + ".css")),
            new DeleteTask(v_model.getViewUrl(name + ".js"))
        ], deleted);
        taskList.taskOperation();
    };

    this.deleteViewmodel = function(name, callback) {
        function deleted(ok) {
            var id = v_nameIds[name];
            if (ok && id != undefined) {
                v_fileLoaders[id]["saveable"] = false;
            }
            callback(ok, id);
        }
        v_model.deleteFile(v_model.getViewmodelUrl(name), deleted);
    };

    this.newViewContent = function(callback) {
        ++v_viewCounter;

        var id = "GuiEditor_ViewContentEditor_File_" + v_viewCounter;
        var name = "View_" + v_viewCounter;

        addNewContent(id + "_Html", name + ".html");
        addNewContent(id + "_Css", name + ".css");
        addNewContent(id + "_Js", name + ".js");

        v_nameIds[name] = id;

        callback(id, name, "new");
    };

    this.newViewmodelContent = function(callback) {
        ++v_viewmodelCounter;

        var id = "GuiEditor_ViewmodelContentEditor_File_" + v_viewmodelCounter + "_Js";
        var name = "Viewmodel_" + v_viewmodelCounter;

        addNewContent(id, name + ".js");

        v_nameIds[name] = id;

        callback(id, name, "new");
    };

    this.getContent = function(id) {
        var content = {};
        content.text = v_fileLoaders[id]["loader"].getData();
        if (id.endsWith("Html")) {
            content.mode = "xml";
        } else if (id.endsWith("Css")) {
            content.mode = "css";
        } else if (id.endsWith("Js")) {
            content.mode = "javascript";
        }
        return content;
    };

    this.setContent = function(id, content) {
        v_fileLoaders[id]["loader"].setData(content);
        v_fileLoaders[id]["edited"] = true;
    };
}
//# sourceURL=GuiEditor\ViewModels\ViewModel_ContentEditor.js