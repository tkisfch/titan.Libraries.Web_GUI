// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_Imports_ViewModel(p_model, p_parent) {
    "use strict";

    var v_model = p_model;
    var v_parent = p_parent;

    var v_this = this;

    ///////////////////// GENERAL EDITOR FUNCTIONS /////////////////////

    this.getTreeData = function() {
        var data = [
            {"text": "Import into ..."},
            {"text": "Request connection"},
            {"text": "Parameters", "children": []}
        ];

        var params = v_model.getSetupParams();
        for (var name in params) {
            data[2].children.push({"text": name + ": " + params[name]});
        }

        return data;
    };

    this.getName = v_model.getSetupName;
    this.getTooltip = function() {
        return JSON.stringify(v_model.getDescriptorCopy(), null, 4);
    };

    this.matches = function(string) {
        return JSON.stringify(v_model.getDescriptorCopy()).toLowerCase().indexOf(string) != -1;
    };

    ///////////////////// GENERAL VIEW EDITOR FUNCTIONS /////////////////////

    this.getParentId = v_model.getParentId;

    this.setParentId = function(p_id) {
        v_model.setParentId(p_id);
        v_parent.setupChanged("Parent id changed");
    };

    this.removeAllChildren = function() {};

    ///////////////////// IMPORT EDITOR SPECIFIC FUNCTIONS /////////////////////

    this.setupNameChanged = function(setupName) {
        v_model.setSetup(setupName);
        v_parent.setupChanged("Setup name changed");
    };

    this.setRequestPath = function(requestPath) {
        v_model.setRequestPath(requestPath);
        v_parent.setupChanged("Import request path changed");
    };

    this.getRequestPath = v_model.getRequestPath;
    this.updatePaths = v_model.updatePaths;
    this.pathsMoved = v_model.pathsMoved;

    this.addSetupParam = function(name, value) {
        v_model.addSetupParam(name, value);
        v_parent.setupChanged("Import param added");
    };

    this.removeSetupParam = function(name) {
        v_model.removeSetupParam(name);
        v_parent.setupChanged("Import param removed");
    };
}
//# sourceURL=GuiEditor\ViewModels\ViewModel_Imports.js