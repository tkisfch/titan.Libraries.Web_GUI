// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_View(p_viewModel, p_parentId, p_viewId) {
    "use strict";

    var HTML = "WebApplications/GuiEditor/Views/View.html";

    var parentDiv = document.getElementById(p_parentId);
    var v_parentId = p_parentId;
    var v_viewId = p_viewId;
    var v_viewmodel = p_viewModel;
    var v_this = this;

    var v_requestEditorView = new GuiEditor_RequestEditor_View(v_viewmodel, v_this);
    var v_viewContentEditorView = new GuiEditor_ViewContentEditor_View(v_viewmodel, "GuiEditor_ViewContentEditor");
    var v_viewmodelContentEditorView = new GuiEditor_ViewModelContentEditor_View(v_viewmodel, "GuiEditor_ViewmodelContentEditor");
    var v_uiConfigEditorView = new GuiEditor_JSONConfigEditor_View(v_viewmodel.getUIConfigEditorViewmodel(), "GuiEditor_UIConfigEditor");

    var v_focused_obj;

    ///////////////////// GETTER FOR SUBVIEWS //////////////////////////////

    this.getRequestEditorView = function() {
        return v_requestEditorView;
    };

    ///////////////////// GENERAL VIEW FUNCTIONS //////////////////////////////

    this.init = function(p_callback) {
        var mainDiv = document.createElement("div");
        mainDiv.setAttribute("id", v_viewId);
        parentDiv.appendChild(mainDiv);

        function htmlLoaded(ok, data) {
            if (ok) {
                $("#" + v_viewId).append(data);
                $('#GuiEditor_Tabs').tabs({
                    activate: function(ev, ui) {
                        var oldId = ui.oldPanel.attr("id");
                        tabClosed(oldId);
                        var newId = ui.newPanel.attr("id");
                        tabOpened(newId);
                    }
                });

                $("#GuiEditor_WebAppStyle").load("WebApplications/GuiEditor/Views/View.css", function() {
                    v_this.toggleButtons(false);
                    p_callback(true);
                });
            } else {
                p_callback(false, "Error loading " + HTML);
            }
        }

        v_viewmodel.loadFile(HTML, htmlLoaded);
    };

    function onWindowResize(event) {
        if (event.target == window) {
            $("#GuiEditor_MainView").height(ViewUtils.getSuggestedHeight("GuiEditor_MainView"));
        }
    }

    this.applicationCreated = function() {
        v_requestEditorView.applicationCreated();
        v_viewContentEditorView.applicationCreated();
        v_viewmodelContentEditorView.applicationCreated();
        v_uiConfigEditorView.applicationCreated();
        $("#GuiEditor_Button_New").on("click", newSetup);
        $("#GuiEditor_Button_Load").on("click", switchSetup);
        $("#GuiEditor_Button_Save").on("click", saveSetup);
        $("#GuiEditor_Button_SaveAs").on("click", saveSetupAs);
        $("#GuiEditor_Button_Show").on("click", showData);
        $("#GuiEditor_Button_ExportChartData").on("click", exportChartRequest);
        $("#GuiEditor_Button_History").on("click", showHistory);
        $("#GuiEditor_Button_SetApplication").on("click", changeApplication);
        $(document).on("keydown", keyPressed);

        $(window).on("resize", onWindowResize);
        $("#GuiEditor_MainView").height(ViewUtils.getSuggestedHeight("GuiEditor_MainView"));

        /* TODO: this works if we add .sortable class to the ul elemens, but do we need it? It would not be here, however.
        $(".sortable").sortable();
        $(".sortable").disableSelection();
        */
    };

    this.destroy = function() {
        v_requestEditorView.destroy();
        $("#" + v_viewId).remove();
        $(document).off("keydown", keyPressed);
        $(window).off("resize", onWindowResize);
    };

    this.unload = function(p_callback) {
        if (v_viewmodel.isSetupChanged() == true && v_viewmodel.getAppConfig().confirmExit == true) {
            var exitDialog = new ExitDialog(v_viewId, "GuiEditor_Dialog_Exit", {
                "header": "Exit GuiEditor",
                "text": "Do you wish to save changes before leaving?",
                "callback": function(exit, save) {
                    if (save) {
                        saveSetup(function(ok) {
                            p_callback(exit);
                        });
                    } else {
                        p_callback(exit);
                    }
                }
            });
            exitDialog.open();
        } else {
            p_callback(true);
        }
    };

    ///////////////////// EVENT HANDLING FUNCTIONS //////////////////////////////

    function keyPressed(event) {
        if(event.keyCode === 46 && v_focused_obj != undefined && v_focused_obj.deletePressed != undefined) {
            v_focused_obj.deletePressed();
        }

        if(event.keyCode === 83 && event.ctrlKey == true && $("#GuiEditor_Button_Save").attr("disabled") != true) {
            saveSetup();
            event.preventDefault();
            event.stopPropagation();
        }
    }

    function tabClosed(id) {
        if (id == "GuiEditor_RequestEditorView") {
            $(document).off("keydown", keyPressed);
        } else if (id == "GuiEditor_UIConfigEditor") {
            v_uiConfigEditorView.close();
        }
    }

    function tabOpened(id) {
        if (id == "GuiEditor_InfoAreaView") {
            showData();
        } else if (id == "GuiEditor_ViewContentEditor") {
            v_viewContentEditorView.applicationFocused();
        } else if (id == "GuiEditor_ViewmodelContentEditor") {
            v_viewmodelContentEditorView.applicationFocused();
        } else if (id == "GuiEditor_RequestEditorView") {
            $("#" + v_viewId).on("keydown", keyPressed);
            $("#" + id).trigger("resize");
            v_requestEditorView.getEditorContainerView().refreshConnections();
            v_requestEditorView.getEditorContainerView().validateEditors();
        } else if (id == "GuiEditor_UIConfigEditor") {
            v_uiConfigEditorView.refresh();
        }
    }

    function newSetup() {
        v_this.toggleButtons(false);
        v_viewmodel.newSetup();
        v_this.updateSetupName();
        v_focused_obj = undefined;
    }

    function deleteSetup(value) {
        var text = "Are you sure your want to delete setup " + value + "?";
        if (v_viewmodel.isCurrentlyEdited(value)) {
            text += "<br><b>Warning! This is the currently edited setup<b>";
        }

        function setupDeleted(ok) {
            if (!ok) {
                alert("Failed to delete setup " + value);
            }
            v_this.updateSetupName();
            switchSetup();
        }

        var dialog = new ConfirmationDialog(v_viewId, "GuiEditor_Dialog_DeleteSetup", {
            "header": "Delete setup",
            "text": text,
            "callback": function() {
                v_viewmodel.deleteSetup(value, setupDeleted);
            }
        });
        dialog.open();
    }

    function switchSetup() {
        function gotSetupName(p_setup) {
            v_this.toggleButtons(false);
            v_viewmodel.switchSetup(p_setup,
                function callback(ok) {
                    if (!ok) {
                        alert("Loading setup failed");
                    }
                    v_this.updateSetupName();
                    v_focused_obj = undefined;
                    v_viewContentEditorView.applicationFocused();
                }
            );
        }

        function optionsArrived(options) {
            var dialog = new ChoiceDialogWithButton(v_viewId, "GuiEditor_Dialog_LoadSetup", {
                "header": "Select setup",
                "text": "Please select a setup from the table below.",
                "choices": options,
                "callback": gotSetupName,
                "buttonHandler": deleteSetup,
                "buttonText": "X",
                "buttonStyle": "color: red;",
                "closeOnButtonPress": true,
                "searchFunction": v_viewmodel.globalSetupSearch
            });
            dialog.open();
        }

        v_viewmodel.listSetups(optionsArrived);
    }

    function saveSetup(p_callback) {
        function setupSaved(ok) {
            if (!ok) {
                alert("Failed to save setup " + v_viewmodel.getSetupName());
            }
            if (p_callback != undefined && typeof p_callback == "function") {
                p_callback(ok);
            }
            v_this.toggleButtons(true);
        }

        if (v_viewmodel.isSaveable()) {
            v_this.toggleButtons(false);
            v_viewmodel.saveSetup(setupSaved);
        } else {
            saveSetupAs(p_callback);
        }
    }

    function saveSetupAs(p_callback) {
        var newSetupName;

        function setupSaved(ok) {
            if (!ok) {
                alert("Failed to save setup " + newSetupName);
            }
            v_this.updateSetupName();
            if (p_callback != undefined && typeof p_callback == "function") {
                p_callback(ok);
            }
            v_this.toggleButtons(true);
        }

        function gotSetupName(value) {
            newSetupName = value;
            v_viewmodel.setupExists(newSetupName, function(exists) {
                if (exists) {
                    var confirmDialog = new ConfirmationDialog(v_viewId, "GuiEditor_Dialog_OverWrite", {
                        "header": "Already exists",
                        "text": "Overwrite directory?",
                        "callback": function() {
                            v_this.toggleButtons(false);
                            v_viewmodel.saveSetupAs(newSetupName, setupSaved);
                        }
                    });
                    confirmDialog.open();
                } else {
                    v_this.toggleButtons(false);
                    v_viewmodel.saveSetupAs(newSetupName, setupSaved);
                }
            });
        }

        var date = new Date();
        var dialog = new InputDialog(v_viewId, "GuiEditor_Dialog_SaveSetupAs", {
            "header": "Save Setup As...",
            "text": "Please enter the name of the setup.<br/>The name should only con&shy;tain upper and lower case eng&shy;lish let&shy;ters, num&shy;bers, under&shy;scores, dots and dashes. The length is also capped at max 32 char&shy;ac&shy;ters.",
            "defaultValue": "Setup_" + date.getFullYear() + "-" + mpad((date.getMonth() + 1), 2) + "-" + mpad(date.getDate(), 2) + "_" + mpad(date.getHours(), 2) + "-" + mpad(date.getMinutes(), 2) + "-" + mpad(date.getSeconds(), 2),
            "validator": function(value) {
                var error = "";
                var pattern = /^([A-Za-z0-9-_.])+$/;
                if (!pattern.test(value))
                    error += "Setup name can only con&shy;tain upper and lower case eng&shy;lish let&shy;ters, num&shy;bers, under&shy;scores, dots and dashes!";
                if (error.length > 0)
                  error += "<br/>";
                if (value.length > 132)
                    error += "Setup name is too long (max. 132 char&shy;ac&shy;ters allo&shy;wed)!";
                return error;
            },
            "callback": gotSetupName
        });

        dialog.open();
    }

    function showData() {
        $("#GuiEditor_InfoArea").val(v_viewmodel.getJsonRepresentation());
    }

    function exportChartRequest() {
        function chartRequestExported() {
            $("#GuiEditor_Button_ExportChartData").prop("disabled", false).css("color", "");
        }

        $("#GuiEditor_Button_ExportChartData").prop("disabled", true).css("color", "red");
        v_viewmodel.exportChartRequest(chartRequestExported);
    }

    function changeApplication() {
        function gotApplicationName(p_application) {
            v_viewmodel.setEditedApp(p_application, function() {
                newSetup();
            });
        }

        function optionsArrived(options) {
            var dialog = new ChoiceDialog(v_viewId, "GuiEditor_Dialog_SetApplication", {
                "header": "Select application",
                "text": "Please select an application from the table below.",
                "choices": options,
                "callback": gotApplicationName
            });
            dialog.open();
        }

        v_viewmodel.listEditableApps(optionsArrived);
    }

    ///////////////////// USEFUL FUNCTION FOR VIEWS //////////////////////////////

    this.toggleButtons = function(on) {
        $(".GuiEditor_Button_Left").prop("disabled", !on);
        $(".GuiEditor_Button_Right").prop("disabled", !on);
    };

    this.setFocusedObj = function(p_object) {
        if (v_focused_obj != undefined) {
            v_focused_obj.setDefaultZidx();
        }
        if (p_object != undefined) {
            p_object.setZidx();
        }
        v_focused_obj = p_object;
    };

    this.updateSetupName = function() {
        var config = v_viewmodel.getAppConfig();
        if (config.lastEditedApp != undefined) {
            document.getElementById("GuiEditor_AppNameLabel").innerHTML = config.lastEditedApp;
        } else {
            document.getElementById("GuiEditor_AppNameLabel").innerHTML = "... undefined application ...";
        }
        if (config.lastEditedSetup != undefined) {
            document.getElementById("GuiEditor_SetupNameLabel").innerHTML = config.lastEditedSetup;
        } else {
            document.getElementById("GuiEditor_SetupNameLabel").innerHTML = "... unsaved setup ...";
        }
    };

    ///////////////////// HISTORY //////////////////////////////

    function HistoryElement(index, element) {
        var v_index = index;
        this.text = element.text;
        this.callback = function() {
            if (v_index != v_viewmodel.getCurrentPositionInHistory()) {
                v_viewmodel.rewind(v_index);
            }
        };
    }

    function HistoryClass(history) {
        var v_history = history;
        this.getMenuElements = function() {
            var contextItems = [];
            for (var i = 0; i < v_history.length; ++i) {
                contextItems.unshift(new HistoryElement(i, v_history[i]));
            }
            return contextItems;
        };
    }

    function showHistory(event) {
        v_viewmodel.historyEnabled(false);
        var history = v_viewmodel.getHistory();
        var contextMenuViewmodel = new HistoryClass(history);
        var contextParentId = "GuiEditor_Buttonbar";
        var contextId = "GuiEditor_History";
        var customDataForContextMenu = {
            "offset": $(this).offset()
        };
        var contextMenu = new CView_ContextMenu([contextMenuViewmodel], contextId, contextParentId, customDataForContextMenu);
        contextMenu.applicationCreated();
        $("#" + contextId).on("remove", function() {
            v_viewmodel.historyEnabled(true);
        });
        $($("#" + contextId + " li")[history.length - v_viewmodel.getCurrentPositionInHistory() - 1]).css("color", "green");
        $("#" + contextId + " li").css("padding-left", "65px");
        $("#" + contextId + " li").css("background-color", "rgba(197,197,197,0.9)");
    }
}
//# sourceURL=GuiEditor\Views\View.js
