// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_BaseContentEditor_View(p_viewmodel, p_id) {

    var HTML_OF_TAB = '' +
        '<li>' +
            '<a {2}href="#{0}">{1}</a>' +
            '<span class="ui-icon ui-icon-close">Remove Tab</span>' +
        '</li>';

    var HTML_OF_FILETABS = '<div id="{0}"></div>';

    this.EDITOROPTIONS = {
        lineNumbers: true,
        smartIndent: true,
        indentUnit: 4,
        lineWrapping: true,
        foldGutter: true,
        gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
        matchBrackets: true,
        autoCloseBrackets: true,
        highlightSelectionMatches: true,
        styleActiveLine: true
    };

    this.viewmodel = p_viewmodel.getContentEditorViewModel();
    this.viewId = p_id;

    this.fileTabs;

    var v_this = this;

    ///////////////////// GENERAL VIEW FUNCTIONS //////////////////////////////

    this.applicationCreated = function() {
        v_this.fileTabs = $("#" + v_this.viewId + "_Tabs");
        v_this.fileTabs.tabs({
            "active": 0,
            "activate": function(ev, ui) {
                v_this.panelActivated(v_this.getPanelOnActivation(ui.newPanel));
            }
        });

        // close icon: removing the tab on click
        v_this.fileTabs.on("click", "span.ui-icon-close", function() {
            var panelId = $(this).closest("li").remove().attr("aria-controls");
            $("#" + panelId).remove();
            v_this.fileTabs.tabs("refresh");
            v_this.viewmodel.editorClosed(panelId);
        });
    };

    ///////////////////// HANDLING TABS AND PANELS //////////////////////////////

    this.getPanelOnActivation = function(panel) {
        return panel;
    };

    this.applicationFocused = function() {
        v_this.panelActivated(v_this.getPanelOnFocus());
    };

    this.panelActivated = function(panel) {
        if (panel == undefined || panel.attr("id") == undefined) {
            return;
        }

        var id = panel.attr("id");
        var content = v_this.viewmodel.getContent(panel.attr("id"));
        var text = content.text;
        v_this.EDITOROPTIONS.mode = content.mode;

        var codeMirrorInstance;

        if ($("#" + id).data("CodeMirror") == undefined) {
            panel.append('<div id="' + id + '_Editor"></div>');
            codeMirrorInstance = CodeMirror(document.getElementById(id + '_Editor'), v_this.EDITOROPTIONS);
            $("#" + id).data("CodeMirror", codeMirrorInstance);
            codeMirrorInstance.setSize("100%", "100%");

            codeMirrorInstance.on('changes', function(ev, changes) {
                if (changes[0].origin != "setValue") {
                    v_this.viewmodel.setContent(id, codeMirrorInstance.getValue());
                    v_this.setEdited(true);
                }
                // TODO maybe we can check if there is no undo stuff left: doc.historySize()
            });

        } else {
            codeMirrorInstance = $("#" + id).data("CodeMirror");
            codeMirrorInstance.refresh();
        }

        codeMirrorInstance.setValue(text);
    };

    this.getPanelOnFocus = function() {
        v_this.getCurrentlyOpenPanel();
    };

    this.getCurrentlyOpenPanel = function() {
        return $("#" + v_this.viewId + "_Tabs").children("div.ui-tabs-panel[aria-hidden='false']");
    };

    this.setEdited = function(edited, id) {
        var tab;
        if (id != undefined) {
            tab = $('a[href$="' + id + '"]');
        } else {
            tab = v_this.getEditedTab();
        }

        if (edited) {
            tab.addClass("file-changed");
        } else {
            tab.removeClass("file-changed");
        }
    };

    this.getEditedTab = function() {
        return $("#" + v_this.viewId + "_Tabs").find(".ui-tabs-active a");
    };

    this.changeTabName = function(newName) {
        $("#" + v_this.viewId + "_Tabs a[href$=" + v_this.getCurrentlyOpenPanel().attr("id") + "]").text(newName);
    };

    this.getIdOfPanelContainingEditor = function() {
        return v_this.getCurrentlyOpenPanel().attr("id");
    };

    ///////////////////// FILE OPERATION HANDLING //////////////////////////////

    this.save = function(saveAsFn) {
        function saved(ok) {
            if (!ok) {
                alert("Failed to save file " + v_this.viewmodel.getFileName(v_this.getIdOfPanelContainingEditor()));
            } else {
                v_this.setEdited(false);
            }
        }

        var id = v_this.getIdOfPanelContainingEditor();
        if (id == undefined) {
            return;
        } else if (v_this.viewmodel.isSaveable(id)) {
            v_this.viewmodel.save(id, saved);
        } else {
            saveAsFn();
        }
    };

    this.saveAs = function(existsFn, saveAsFn, contentType, savedCallback) {
        var openPanelId = v_this.getCurrentlyOpenPanel().attr("id");
        if (openPanelId == undefined) {
            return;
        }

        var newName;

        function callback(ok, id, existingId) {
            savedCallback(newName, ok, id, existingId)
        }

        function gotName(value) {
            newName = value;
            existsFn(newName, function(exists) {
                if (exists) {
                    var confirmDialog = new ConfirmationDialog(v_this.viewId, "GuiEditor_Dialog_OverWrite", {
                        "header": "Already exists",
                        "text": "Overwrite {0}?".format(contentType),
                        "callback": function() {
                            saveAsFn(openPanelId, newName, callback);
                        }
                    });
                    confirmDialog.open();
                } else {
                    saveAsFn(openPanelId, newName, callback);
                }
            });
        }

        var date = new Date();
        var dialog = new InputDialog(v_this.viewId, "GuiEditor_Dialog_Save{0}As".format(contentType), {
            "header": "Save {0} As...".format(contentType),
            "text": "Please enter the name of the {0}.<br/>The name should only con&shy;tain upper and lower case eng&shy;lish let&shy;ters, num&shy;bers, under&shy;scores, dots and dashes. The length is also capped at max 32 char&shy;ac&shy;ters.".format(contentType),
            "defaultValue": "{0}_".format(contentType) + date.getFullYear() + "-" + mpad((date.getMonth() + 1), 2) + "-" + mpad(date.getDate(), 2) + "_" + mpad(date.getHours(), 2) + "-" + mpad(date.getMinutes(), 2) + "-" + mpad(date.getSeconds(), 2),
            "validator": function(value) {
                var error = "";
                var pattern = /^([A-Za-z0-9-_.])+$/;
                if (!pattern.test(value))
                    error += "{0} name can only con&shy;tain upper and lower case eng&shy;lish let&shy;ters, num&shy;bers, under&shy;scores, dots and dashes!".format(contentType);
                if (error.length > 0)
                  error += "<br/>";
                if (value.length > 32)
                    error += "{0} name is too long (max. 32 char&shy;ac&shy;ters allo&shy;wed)!".format(contentType);
                return error;
            },
            "callback": gotName
        });

        dialog.open();
    };
}
//# sourceURL=GuiEditor\Views\View_BaseContentEditor.js