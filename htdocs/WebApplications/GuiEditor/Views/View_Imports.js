// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_Imports_View(p_viewModel, p_parentId, p_viewId, p_parent, p_desktopData) {
    "use strict";

    var base = new GuiEditor_BaseEditor_View(p_viewModel, p_parentId, p_viewId, p_parent, p_desktopData, this);
    var v_requestTree = base.parent.getRequestTree();
    var v_requestConnectionPresent = false;
    var v_selectedNode;

    var v_this = this;

    ///////////////////// GENERAL VIEW FUNCTIONS //////////////////////////////

    this.init = function(callback) {
        base.init(callback, "GuiEditor_ImportEditor", 800);
        v_requestConnectionPresent = base.viewmodel.getRequestPath() != undefined;
    };

    this.destroy = base.destroy;
    this.setDefaultZidx = base.setDefaultZidx;
    this.setZidx = base.setZidx;
    this.deletePressed = function() {
        var node = base.tree.jstree("get_selected");
        if (node.length > 0) {
            deleteNode(node);
        }
    };

    ///////////////////// GENERAL EDITOR VIEW FUNCTIONS //////////////////////////////

    this.disabled = base.disabled;
    this.search = base.search;

    ///////////////////// IMPORT EDITOR SPECIFIC FUNCTIONS //////////////////////////////

    this.updatePaths = function(path, amount) {
        var connectionRemoved = base.viewmodel.updatePaths(path, amount);
        if (connectionRemoved) {
            v_requestConnectionPresent = false;
            base.parent.deleteEndpoint(requestConnectionEndpoint);
        }
    };

    this.pathsMoved = base.viewmodel.pathsMoved;

    this.importAddedInRequestTree = function(requestNodeId, parentId) {
        var parent = base.tree.jstree("get_node", parentId);
        if (base.jsTreeUtils.isNodeFromTree(parent, base.tree)) {
            var data = {
                "parent": parent,
                "position": 0,
                "original": {
                    "id": requestNodeId
                }
            };
            requestAdded(data, false);
            return true;
        } else {
            return false;
        }
    };

    ///////////////////// CREATING THE VIEW //////////////////////////////

    base.fillHeader = function(header) {
        var labelText = document.createElement("label");
        labelText.setAttribute("id", base.id + "_HeaderText");
        labelText.setAttribute("class", "GuiEditor_ImportEditorHeaderLabel");
        header.appendChild(labelText);
        base.addButtonToHeader(header, "_Button_Minimize", "GuiEditor_EditorButtonRight", " ");
    };

    base.createTree = function(p_callback) {
        var data = base.viewmodel.getTreeData();
        base.tree.jstree("destroy");
        base.tree.jstree({
            "core": {
                "data": data,
                "check_callback": function(operation, node, node_parent, node_position, more) {
                    if (operation === "copy_node" && base.jsTreeUtils.isNodeFromTree(node, v_requestTree) && !base.jsTreeUtils.isRoot(node_parent) && base.jsTreeUtils.getIndexOfNode(node_parent, base.tree) === 1) {
                        return true;
                    } else if (operation === "copy_node") {
                        return false;
                    } else {
                        return true;
                    }
                },
                "multiple": false,
                "animation": false,
                "worker": false
            },
            "plugins" : ["contextmenu", "dnd"],
            "dnd": {
                "always_copy": true
            },
            "contextmenu": {
                "items": function($node) {
                    return {
                        "Delete": {
                            "label": "Delete",
                            "action": function(data) {
                                deleteNode(data.reference);
                            }
                        },
                        "AddParam": {
                            "label": "Add Parameter",
                            "action": function(data) {
                                paramAdded(data.reference);
                            }
                        },
                        "RenameParam": {
                            "label": "Rename Parameter",
                            "action": function(data) {
                                paramRenamed(data.reference);
                            }
                        }
                    };
                },
                "select_node": false
            }
        });

        base.tree.bind("copy_node.jstree", function(e, data) {
            requestAdded(data, true);
        });

        base.tree.bind("select_node.jstree", function(e, data) {
            if (v_selectedNode == data.node.id) {
                data.instance.deselect_node(data.node);
                v_selectedNode = undefined;
            } else {
                v_selectedNode = data.node.id;
            }
        });

        base.tree.bind("redraw.jstree", function(e, data) {
            document.getElementById(base.id).style.height = "auto";
            document.getElementById(base.id).style.width = "auto";
        });

        base.tree.bind("ready.jstree", function(e, data) {
            p_callback(true);
        });

        base.openNodes();

        base.tree.bind("after_open.jstree after_close.jstree", function(e, data) {
            document.getElementById(base.id).style.height = "auto";
            document.getElementById(base.id).style.width = "auto";
            base.saveOpenNodes();
        });
    };

    ///////////////////// HANDLING EVENTS //////////////////////////////

    base.setupCallbacks = function() {
        $("#" + base.id + "_Header").on('contextmenu', function(event) {
            var list = [
                {
                    "text": "change setup",
                    "callback": changeSetup
                },
                {
                    "text": "show only this",
                    "callback": base.showOnlyThis
                },
                {
                    "text": "show all",
                    "callback": base.showAll
                },
                {
                    "text": "delete",
                    "callback": base.remove
                }
            ];

            base.parent.openContextMenu(list, $("#" + base.id).offset());

            event.preventDefault();
            return false;
        });

        base.setupMinimizedCallback();
    };

    base.editorDeleted = function() {
        base.parent.editorDeleted("Import", v_this);
    };

    ///////////////////// HANDLING OWN EVENTS //////////////////////////////

    function changeSetup() {
        function callback(setupName) {
            base.viewmodel.setupNameChanged(setupName);
            base.setHeaderText();
        }
        base.parent.getSetups(callback);
    }

    function requestAdded(data, deleteNeeded) {
        if (deleteNeeded) {
            base.tree.jstree("delete_node", data.node.id);
        }

        var requestPath = base.jsTreeUtils.getPath(v_requestTree.jstree("get_node", data.original.id)).path;
        base.viewmodel.setRequestPath(requestPath);
        if (v_requestConnectionPresent) {
            base.refreshConnections();
        } else {
            v_requestConnectionPresent = true;
            base.parent.addEndpoint(requestConnectionEndpoint);
            base.refreshConnections();
        }
    }

    function deleteNode(data) {
        var treeNode = base.tree.jstree("get_node", data);
        var path = base.jsTreeUtils.getPath(base.tree.jstree("get_node", treeNode.id)).path;
        if (path[0] == 1 && v_requestConnectionPresent) {
            v_requestConnectionPresent = false;
            base.viewmodel.setRequestPath(undefined);
            base.parent.deleteEndpoint(requestConnectionEndpoint);
        } else if (path[0] == 2 && path.length == 2) {
            base.viewmodel.removeSetupParam(treeNode.text.split(": ")[0]);
            base.tree.jstree("delete_node", treeNode);
        }
        base.saveOpenNodes();
    }

    function paramAdded(data) {
        var name = prompt("Param name: ");
        if (name != undefined) {
            var value = prompt("Param value: ");
            if (value != undefined) {
                base.viewmodel.addSetupParam(name, value);
                var id = base.jsTreeUtils.getNodeIdFromPath(base.tree, [2]);
                base.tree.jstree("create_node", id, name + ": " + value);
            }
        }
    }

    function paramRenamed(data) {
        var treeNode = base.tree.jstree("get_node", data);
        var path = base.jsTreeUtils.getPath(base.tree.jstree("get_node", treeNode.id)).path;
        if (path.length == 2) {
            var old_name = treeNode.text.split(": ")[0];
            var value = treeNode.text.split(": ")[1];
            var name = prompt("New param name: ", old_name);
            if (name != undefined) {
                value = prompt("New param value: ", value);
                if (value != undefined) {
                    base.viewmodel.removeSetupParam(old_name);
                    base.viewmodel.addSetupParam(name, value);
                    base.tree.jstree("rename_node", treeNode, name + ": " + value);
                }
            }
        }
    }

    ///////////////////// HANDLING CONNECTIONS //////////////////////////////

    var viewConnectionPoint = {
        "getOffset": function() {
            var htmlObj;

            if (!base.isVisible) {
                var id = base.id + "_Header";
                htmlObj = $("#" + id);
            } else {
                var id = base.jsTreeUtils.getLastNodeIdFromPath(base.tree, [0]);
                htmlObj = $("#" + id + "_anchor");
            }

            var offset = htmlObj.offset();
            offset.left += htmlObj.width();
            offset.top += htmlObj.height() / 2;
            return offset;
        },

        "getZIndex": function() {
            return base.zIndex + 1;
        },

        "isEnabled": function() {
            return base.isEnabled;
        },

        "object": v_this
    };

    var requestConnectionEndpoint = {
        "getOffset": function() {
            var htmlObj;

            if (!base.isVisible) {
                var id = base.id + "_Header";
                htmlObj = $("#" + id);
            } else {
                var id = base.jsTreeUtils.getLastNodeIdFromPath(base.tree, [1]);
                htmlObj = $("#" + id + "_anchor");
            }

            var offset = htmlObj.offset();
            offset.top += htmlObj.height() / 2;
            return offset;
        },

        "getZIndex": function() {
            return base.zIndex + 1;
        },

        "isEnabled": function() {
            return base.isEnabled;
        },

        "endpointType": "source",
        "connectionType": "R_I",
        "object": v_this,
        "options": {
            "stroke": "#cc9c5c"
        },
        "identifier": function() {
            return base.viewmodel.getRequestPath();
        }
    };

    this.getEndpoint = function(identifier) {
        if (identifier == base.viewmodel.getParentId()) {
            return viewConnectionPoint;
        } else {
            return undefined;
        }
    };

    this.getConnectionInformation = function(identifier) {
        var parentId = base.viewmodel.getParentId();
        if (identifier.type == "set" && base.tree.jstree("get_node", identifier.treeId)) {
            base.viewmodel.setParentId(identifier.parentId);
            return parentId;
        } else if (identifier.type == "get" && identifier.parentId == base.viewmodel.getParentId()) {
            return parentId;
        } else {
            return undefined;
        }
    };

    this.getEndpoints = function() {
        if (v_requestConnectionPresent) {
            return [requestConnectionEndpoint];
        } else {
            return [];
        }
    };
}
//# sourceURL=GuiEditor\Views\View_Imports.js