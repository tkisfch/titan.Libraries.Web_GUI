// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_JSONConfigEditor_View(p_viewmodel, p_id) {
    "use strict";

    var v_viewmodel = p_viewmodel;
    var v_id = p_id;

    var v_this = this;

    this.applicationCreated = function() {
        $('#' + v_id + '_Button_Save').on("click", save);
        $('#' + v_id + '_Button_Load').on("click", load);
        $('#' + v_id + '_Button_Save').prop("disabled", true);
    };

    function load() {
        v_this.close();
        v_this.refresh();
    }

    function save() {
        v_viewmodel.save();
    }

    function startJSONEditor() {
        var editor = new CView_JSONEditor([v_viewmodel], v_id + '_Editor', v_id + "_EditorContainer", {"headerText": v_viewmodel.getName()});
        editor.applicationCreated();
        editor.refresh(true);
    }

    function startCodeEditor() {
        var editor = new CView_CodeEditor([v_viewmodel], v_id + '_Editor', v_id + "_EditorContainer", {"editorType": "json", "formatAllowed": true, "headerText": v_viewmodel.getName()});
        editor.applicationCreated();
        editor.refresh(true);
    }

    this.refresh = function() {
        function configLoaded(ok) {
            if (ok) {
                if (v_viewmodel.isSchemaPresent()) {
                    startJSONEditor();
                } else {
                    startCodeEditor();
                }
                $('#' + v_id + '_Button_Save').prop("disabled", false);
            } else {
                alert("Loading config failed.");
            }
        }

        function gotConfig(p_config) {
            v_viewmodel.loadConfig(p_config, configLoaded);
        }

        function optionsArrived(options) {
            var dialog = new ChoiceDialog(v_id, "GuiEditor_Dialog_ChooseConfig", {
                "header": "Select config",
                "text": "Please select a config to edit from the table below.",
                "choices": options,
                "callback": gotConfig
            });
            dialog.open();
        }

        $('#' + v_id + '_Button_Save').prop("disabled", true);
        v_viewmodel.listEditableConfigs(optionsArrived);
    };

    this.close = function() {
        $('#' + v_id + '_EditorContainer').empty();
        $('#' + v_id + '_Button_Save').prop("disabled", true);
    };
}
//# sourceURL=GuiEditor\Views\View_JSONConfigEditor.js