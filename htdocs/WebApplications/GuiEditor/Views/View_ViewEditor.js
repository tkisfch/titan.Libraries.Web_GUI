// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_ViewEditor_View(p_viewModel, p_parentId, p_viewId, p_parent, p_desktopData) {
    "use strict";

    var base = new GuiEditor_BaseEditor_View(p_viewModel, p_parentId, p_viewId, p_parent, p_desktopData, this);

    var v_this = this;
    var v_selectedNode = "";

    var viewmodelConnections = [];
    var viewConnections = [];

    ///////////////////// GENERAL VIEW FUNCTIONS //////////////////////////////

    this.init = function(callback) {
        base.init(callback, "GuiEditor_ViewEditor", 610);

        var viewmodelIndexes = base.viewmodel.getViewModelIndexes();
        for (var i = 0; i < viewmodelIndexes.length; ++i) {
            var endpoint = new ViewmodelConnectionEndpoint([0, i], viewmodelIndexes[i]);
            viewmodelConnections.push(endpoint);
        }

        var childIds = base.viewmodel.getChildIds();
        for (var i = 0; i < childIds.length; ++i) {
            var endpoint = getViewEndpoint([2, i], childIds[i]);
            viewConnections.push(endpoint);
        }

        base.validate();
    };

    this.destroy = base.destroy;
    this.setDefaultZidx = base.setDefaultZidx;
    this.setZidx = base.setZidx;
    this.deletePressed = function() {
        var node = base.tree.jstree("get_selected");
        if (node.length > 0) {
            deleteNode(node);
        }
    };

    ///////////////////// GENERAL EDITOR VIEW FUNCTIONS //////////////////////////////

    this.disabled = base.disabled;
    this.isNodeFromTree = base.isNodeFromTree;
    this.search = base.search;
    this.validate = base.validate;

    ///////////////////// VIEW EDITOR SPECIFIC FUNCTIONS //////////////////////////////

    this.wouldCreateACycle = function(node, id) {
        if (base.jsTreeUtils.isNodeFromTree(node, base.tree)) {
            return base.viewmodel.wouldCreateACycle(id);
        } else {
            return false;
        }
    };

    this.removeViewConnection = function(connectionIndex) {
        base.connectionDeleted(viewConnections, connectionIndex);
        base.createTree(function() {
            base.tree.jstree("open_all");
            base.refreshConnections();
        });
        base.validate();
    };

    this.removeViewmodelConnection = function(viewmodelIndex) {
        var indexes = base.viewmodel.getViewModelIndexes();
        var index = indexes.indexOf(viewmodelIndex);

        base.viewmodel.viewModelDeleted(viewmodelIndex);

        if (index != -1) {
            base.connectionDeleted(viewmodelConnections, index);
            base.createTree(function() {
                base.tree.jstree("open_all");
                base.refreshConnections();
            });
            base.validate();
        }
    };

    ///////////////////// CREATING THE VIEW //////////////////////////////

    base.fillHeader = function(header) {
        base.addButtonToHeader(header, "_Button_Edit", "GuiEditor_EditorButtonLeft", "EDIT");
        var labelText = document.createElement("label");
        labelText.setAttribute("id", base.id + "_HeaderText");
        labelText.setAttribute("class", "GuiEditor_ViewEditorHeaderLabel");
        header.appendChild(labelText);
        base.addButtonToHeader(header, "_Button_Minimize", "GuiEditor_EditorButtonRight", " ");
    };

    base.createTree = function(p_callback) {
        var data = base.viewmodel.getTreeData();
        base.tree.jstree("destroy");
        base.tree.jstree({
            "core": {
                "data": data,
                "check_callback": function(operation, node, node_parent, node_position, more) {
                    if (operation === "copy_node") {
                        if (base.jsTreeUtils.isRoot(node_parent)) {
                            return false;
                        }
                        return isViewModelConnection(node, node_parent) || isAllowedViewConnection(node, node_parent, node_position) || isImportConnection(node, node_parent) || (base.jsTreeUtils.isNodeFromTree(node, base.tree) && base.jsTreeUtils.isTheParentOfNode(node, node_parent, base.tree));
                    } else {
                        return true;
                    }
                },
                "multiple": false,
                "animation": false,
                "worker": false
            },
            "plugins": ["contextmenu", "dnd"],
            "contextmenu": {
                "items": function($node) {
                    return {
                        "Delete": {
                            "label": "Delete",
                            "action": function(data) {
                                deleteNode(data.reference);
                            }
                        }
                    };
                },
                "select_node": false
            },
            "dnd": {
                "always_copy": true
            }
        });

        base.tree.bind("copy_node.jstree", function(e, data) {

            if (base.tree.jstree("get_node", data.original.id)) {
                connectionMoved(data);
            } else if (data.original.text == "View connection point") {
                viewModelAdded(data);
            } else if (data.original.text == "Child of ...") {
                viewAdded(data);
            } else if (data.original.text == "Import into ...") {
                viewAdded(data);
            }
        });

        base.tree.bind("select_node.jstree", function(e, data) {
            if (v_selectedNode == data.node.id) {
                data.instance.deselect_node(data.node);
                v_selectedNode = "";
            } else {
                v_selectedNode = data.node.id;
            }
        });

        base.tree.bind("redraw.jstree", function(e, data) {
            document.getElementById(base.id).style.height = "auto";
            document.getElementById(base.id).style.width = "auto";
        });

        base.openNodes();

        base.tree.bind("after_open.jstree after_close.jstree", function(e, data) {
            base.parent.refreshConnections(v_this);
            document.getElementById(base.id).style.height = "auto";
            document.getElementById(base.id).style.width = "auto";
            base.saveOpenNodes();
        });

        base.tree.bind("ready.jstree", function(e, data) {
            p_callback(true);
        });
    };

    ///////////////////// HANDLING EVENTS //////////////////////////////

    base.copyEditor = function() {
        base.parent.copyViewEditor(base.viewmodel.getClass(), base.viewmodel.getCustomData());
    };

    base.treeSlid = function() {
        if (base.isVisible) {
            for (var i = 0; i < viewConnections.length; ++i) {
                viewConnections[i].multiple = false;
                viewConnections[i].style = "horizontal";
            }
        } else {
            for (var i = 0; i < viewConnections.length; ++i) {
                viewConnections[i].multiple = true;
                viewConnections[i].style = "vertical";
            }
        }
        base.refreshConnections();
    };

    base.editorDeleted = function() {
        base.parent.editorDeleted("View", v_this);
    };

    base.getClasses = function(p_callback) {
        base.parent.getViewClasses(p_callback);
    };

    ///////////////////// CHECKING OWN EVENTS //////////////////////////////

    function isViewModelConnection(node, parent) {
        return node.text === "View connection point" && base.jsTreeUtils.getDepth(parent, base.tree) === 1 && base.jsTreeUtils.getIndexOfNode(parent, base.tree) === 0;
    }

    function isAllowedViewConnection(node, parent, pos) {
        if (node.text === "Child of ..." && !base.jsTreeUtils.isNodeFromTree(node, base.tree) && base.jsTreeUtils.getDepth(parent, base.tree) === 1 && base.jsTreeUtils.getIndexOfNode(parent, base.tree) === 2) {
            var id = base.viewmodel.wouldUseId(pos);
            return !base.parent.wouldCreateACycle(node, id);
        } else {
            return false;
        }
    }

    function isImportConnection(node, parent) {
        return node.text === "Import into ..." && base.jsTreeUtils.getDepth(parent, base.tree) === 1 && base.jsTreeUtils.getIndexOfNode(parent, base.tree) === 2;
    }

    ///////////////////// HANDLING OWN EVENTS //////////////////////////////

    function deleteNode(p_data) {
        // we have to get the jquery node instead of the jstree node
        var treeNode = base.tree.jstree("get_node", p_data);
        var jqueryNode = $("#" + treeNode.id);
        var index = jqueryNode.parent().children().index(jqueryNode[0]);

        var path = base.jsTreeUtils.getPath(base.tree.jstree("get_node", treeNode.id)).path;

        if (path.length > 1 && path[0] == 0) {
            base.viewmodel.deleteViewModelIndexFromPosition(index);
            base.tree.jstree("delete_node", p_data);
            base.connectionDeleted(viewmodelConnections, index);
            base.refreshConnections();
        } else if (path.length > 1 && path[0] == 2) {
            base.viewmodel.removeChildView(index);
            var nodeToDelete = base.jsTreeUtils.getLastNodeIdFromPath(base.tree, [2, jqueryNode.parent().children().length - 1])
            base.tree.jstree("delete_node", nodeToDelete);
            base.connectionDeleted(viewConnections, index);
            base.refreshConnections();
        }
        base.saveOpenNodes();
        base.validate();
    }

    function viewModelAdded(data) {
        base.tree.jstree("delete_node", data.node.id);

        var index = data.position;
        var viewModelIndex = base.parent.getConnectionInformation("VM_V", data.original.id);

        var text = "ViewModel " + viewModelIndex;
        base.tree.jstree("create_node", data.parent, text, data.position);
        base.tree.jstree("open_node", data.parent);

        base.viewmodel.addViewModelIndex(viewModelIndex, index);
        base.connectionAdded(viewmodelConnections, index, new ViewmodelConnectionEndpoint([0, index], viewModelIndex));
        base.refreshConnections();

        base.validate();
    }

    function viewAdded(data) {
        base.tree.jstree("delete_node", data.node.id);

        var index = data.position;
        var size = base.viewmodel.getChildIds().length;

        var parentId = base.viewmodel.addChildView(index);
        var oldParentId = base.parent.getConnectionInformation("V_V", {"treeId": data.original.id, "parentId": parentId, "type": "set"});

        var text = "Child view " + size;
        base.tree.jstree("create_node", data.parent, text, "last");
        base.tree.jstree("open_node", data.parent);

        base.connectionAdded(viewConnections, index, getViewEndpoint([2, index], parentId));
        // the next line will invalidate the parentId if the connection was moved from a parent view, so this is the safest place we can do this
        base.viewmodel.removeConnectedChild(oldParentId);
        base.refreshConnections();

        base.validate();
    }

    function connectionMoved(data) {
        var path = base.jsTreeUtils.getPath(base.tree.jstree("get_node", data.original.id)).path;
        if (path[0] == 0) {
            viewmodelConnectionMoved(data);
        } else if (path[0] == 2) {
            viewConnectionMoved(data);
        }
    }

    function viewmodelConnectionMoved(data) {
        base.tree.jstree("delete_node", data.original.id);

        var fromIndex = data.old_position;
        var toIndex = data.position;

        if (toIndex > fromIndex) {
            --toIndex;
        } else if (fromIndex > toIndex) {
            --fromIndex;
        }

        if (fromIndex == toIndex) {
            return;
        }

        base.viewmodel.connectionOrderChanged(fromIndex, toIndex);
        base.connectionMoved(viewmodelConnections, fromIndex, toIndex);
        base.refreshConnections();
    }

    function viewConnectionMoved(data) {
        base.tree.jstree("delete_node", data.node.id);

        var fromIndex = data.old_position;
        var toIndex = data.position;

        if (toIndex > fromIndex) {
            --toIndex;
        } else if (fromIndex > toIndex) {
            --fromIndex;
        }

        if (fromIndex == toIndex) {
            return;
        }

        base.viewmodel.childViewOrderChanged(fromIndex, toIndex);
        base.connectionMoved(viewConnections, fromIndex, toIndex);
        base.refreshConnections();
    }

    ///////////////////// HANDLING CONNECTIONS //////////////////////////////

    function ViewmodelConnectionEndpoint(p_path, p_identifier) {
        return new base.Endpoint(
            p_path,
            p_identifier,
            "target",
            "VM_V",
            {
                "stroke": "blue"
            },
            function(htmlObj) {
                var offset = htmlObj.offset();
                offset.top += htmlObj.height() / 2;
                return offset;
            }
        );
    }

    function ViewConnectionEndpoint(p_path, p_identifier, p_multiple, p_style) {
        var endpoint = new base.Endpoint(
            p_path,
            p_identifier,
            "target",
            "V_V",
            {
                "stroke": "green"
            },
            function(htmlObj) {
                var offset = htmlObj.offset();
                offset.top += htmlObj.height() / 2;
                return offset;
            }
        );

        endpoint.multiple = p_multiple;
        endpoint.style = p_style;

        endpoint.getOffsets = function() {
            var htmlObj = $("#" + base.id);
            var offset = htmlObj.offset();

            var upper = mcopy(offset);
            upper.left += htmlObj.width() / 2;

            var lower = mcopy(offset);
            lower.left += htmlObj.width() / 2;
            lower.top += htmlObj.height();

            return [upper, lower];
        }

        return endpoint;
    }

    var viewConnectionPoint = {
        "getOffset": function() {
            var htmlObj;

            if (!base.isVisible) {
                var id = base.id + "_Header";
                htmlObj = $("#" + id);
            } else {
                var id = base.jsTreeUtils.getLastNodeIdFromPath(base.tree, [1]);
                htmlObj = $("#" + id + "_anchor");
            }

            var offset = htmlObj.offset();
            offset.left += htmlObj.width();
            offset.top += htmlObj.height() / 2;
            return offset;
        },

        "getZIndex": function() {
            return base.zIndex + 1;
        },

        "isEnabled": function() {
            return base.isEnabled;
        },

        "object": v_this
    }

    function getViewEndpoint(p_path, p_identifier) {
        if (base.isVisible) {
            return new ViewConnectionEndpoint(p_path, p_identifier, false, "horizontal");
        } else {
            return new ViewConnectionEndpoint(p_path, p_identifier, true, "vertical");
        }
    }

    this.getEndpoint = function(identifier) {
        if (identifier == base.viewmodel.getParentId()) {
            return viewConnectionPoint;
        } else {
            return undefined;
        }
    };

    this.getConnectionInformation = function(identifier) {
        var parentId = base.viewmodel.getParentId();
        if (identifier.type == "set" && base.tree.jstree("get_node", identifier.treeId)) {
            base.viewmodel.setParentId(identifier.parentId);
            return parentId;
        } else if (identifier.type == "get" && identifier.parentId == base.viewmodel.getParentId()) {
            return parentId;
        } else {
            return undefined;
        }
    };

    this.getEndpoints = function() {
        var list = [];
        for (var i = 0; i < viewmodelConnections.length; ++i) {
            list.push(viewmodelConnections[i]);
        }
        for (var i = 0; i < viewConnections.length; ++i) {
            list.push(viewConnections[i]);
        }
        return list;
    };
}
//# sourceURL=GuiEditor\Views\View_ViewEditor.js
