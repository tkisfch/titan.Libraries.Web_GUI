// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function GuiEditor_ViewModelEditor_View(p_viewModel, p_parentId, p_viewId, p_parent, p_desktopData) {
    "use strict";

    var base = new GuiEditor_BaseEditor_View(p_viewModel, p_parentId, p_viewId, p_parent, p_desktopData, this);
    var v_requestTree = base.parent.getRequestTree();

    var v_this = this;
    var v_selectedNode = "";

    var dataConnections = [];
    var selectionConnections = [];

    ///////////////////// GENERAL VIEW FUNCTIONS //////////////////////////////

    this.init = function(callback) {
        base.init(callback, "GuiEditor_ViewmodelEditor", 250);

        var dataPaths = base.viewmodel.getDataConnections();
        for (var i = 0; i < dataPaths.length; ++i) {
            var endpoint = new DataConnectionEndpoint([1, i], i);
            dataConnections.push(endpoint);
        }

        var selectionsPaths = base.viewmodel.getSelectionConnections();
        for (var i = 0; i < selectionsPaths.length; ++i) {
            var endpoint = new SelectionConnectionEndpoint([2, i], i);
            selectionConnections.push(endpoint);
        }

        base.validate();
    };

    this.destroy = base.destroy;
    this.setDefaultZidx = base.setDefaultZidx;
    this.setZidx = base.setZidx;
    this.deletePressed = function() {
        var node = base.tree.jstree("get_selected");
        if (node.length > 0) {
            deleteNode(node);
        }
    };

    ///////////////////// GENERAL EDITOR VIEW FUNCTIONS //////////////////////////////

    this.disabled = base.disabled;
    this.isNodeFromTree = base.isNodeFromTree;
    this.validate = base.validate;
    this.search = base.search;

    ///////////////////// VIEWMODEL EDITOR SPECIFIC FUNCTIONS //////////////////////////////

    this.requestRenamed = function(p_path, name) {
        var paths = base.viewmodel.requestRenamed(p_path, name);
        for (var i = 0; i < paths.length; ++i) {
            var nodeId = base.jsTreeUtils.expandPath(base.tree, paths[i]);
            base.tree.jstree("rename_node", nodeId, name);
        }
        if (paths.length > 0) {
            base.refreshConnections();
        }
    };

    this.updatePaths = function(path, amount) {
        var connectionsRemoved = base.viewmodel.updateConnections(path, amount);
        if (connectionsRemoved.length > 0) {
            for (var i = 0; i < connectionsRemoved.length; ++i) {
                var obj = connectionsRemoved[i];
                if (obj.type == "data") {
                    base.connectionDeleted(dataConnections, obj.index);
                } else {
                    base.connectionDeleted(selectionConnections, obj.index);
                }
            }

            setIdentifiers();

            base.createTree(function() {
                base.tree.jstree("open_all");
                base.refreshConnections();
            });

            base.validate();
        }
    };

    this.pathsMoved = base.viewmodel.pathsMoved;

    this.selectionAddedInRequestTree = function(requestNodeId, parentId) {
        var parent = base.tree.jstree("get_node", parentId);
        if (base.jsTreeUtils.isNodeFromTree(parent, base.tree)) {
            var data = {
                "parent": parent,
                "position": 0,
                "original": {
                    "id": requestNodeId
                }
            };
            requestAdded(data, false);
            return true;
        } else {
            return false;
        }
    };

    ///////////////////// CREATING THE VIEW //////////////////////////////

    base.fillHeader = function(header) {
        base.addButtonToHeader(header, "_Button_Edit", "GuiEditor_EditorButtonLeft", "EDIT");
        var labelText = document.createElement("label");
        labelText.setAttribute("id", base.id + "_HeaderText");
        labelText.setAttribute("class", "GuiEditor_ViewmodelEditorHeaderLabel");
        header.appendChild(labelText);
        base.addButtonToHeader(header, "_Button_Minimize", "GuiEditor_EditorButtonRight", " ");
    };

    base.createTree = function(p_callback) {
        var data = base.viewmodel.getTreeData();
        base.tree.jstree("destroy");
        base.tree.jstree({
            "core": {
                "data": data,
                "check_callback": function(operation, node, node_parent, node_position, more) {
                    if (operation === "copy_node") {
                        if (base.jsTreeUtils.isRoot(node_parent)) {
                            return false;
                        }
                        return (base.jsTreeUtils.isNodeFromTree(node, v_requestTree) && isDataOrSelection(node_parent)) || (base.jsTreeUtils.isTheParentOfNode(node, node_parent, base.tree));
                    } else {
                        return true;
                    }
                },
                "multiple": false,
                "animation": false,
                "worker": false
            },
            "plugins": ["contextmenu", "dnd"],
            "contextmenu": {
                "items": function($node) {
                    return {
                        "Delete": {
                            "label": "Delete",
                            "action": function(data) {
                                deleteNode(data.reference)
                            }
                        }
                    };
                },
                "select_node": false
            },
            "dnd": {
                "always_copy": true
            }
        });

        base.tree.bind("copy_node.jstree", function(e, data) {
            if (v_requestTree.jstree("get_node", data.original.id)) {
                requestAdded(data, true);
            } else {
                connectionMoved(data);
            }
        });

        base.tree.bind("select_node.jstree", function(e, data) {
            if (v_selectedNode == data.node.id) {
                data.instance.deselect_node(data.node);
                v_selectedNode = "";
            } else {
                v_selectedNode = data.node.id;
            }
        });

        base.tree.bind("redraw.jstree", function(e, data) {
            document.getElementById(base.id).style.height = "auto";
            document.getElementById(base.id).style.width = "auto";
        });

        base.openNodes();

        base.tree.bind("after_open.jstree after_close.jstree", function(e, data) {
            base.refreshConnections();
            document.getElementById(base.id).style.height = "auto";
            document.getElementById(base.id).style.width = "auto";
            base.saveOpenNodes();
        });

        base.tree.bind("ready.jstree", function(e, data) {
            p_callback(true);
        });

        base.tree.bind("hover_node.jstree", function(e, data) {
            var path = base.jsTreeUtils.getPath(base.tree.jstree("get_node", data.node.id)).path;
            if (path.length == 2 && path[0] == 1) {
                $("#"+data.node.id).prop("title", base.viewmodel.getDataPathString(path[1]));
            } else if (path.length == 2 && path[0] == 2) {
                $("#"+data.node.id).prop("title", base.viewmodel.getSelectionPathString(path[1]));
            }
        });
    }

    ///////////////////// HANDLING EVENTS //////////////////////////////

    base.copyEditor = function() {
        base.parent.copyViewmodelEditor(base.viewmodel.getClass(), base.viewmodel.getCustomData());
    };

    base.editorDeleted = function() {
        base.parent.editorDeleted("Viewmodel", v_this);
    };

    base.getClasses = function(p_callback) {
        base.parent.getViewmodelClasses(p_callback);
    };

    ///////////////////// CHECKING OWN EVENTS //////////////////////////////

    function isDataOrSelection(node) {
        return base.jsTreeUtils.getDepth(node, base.tree) === 1 && base.jsTreeUtils.getIndexOfNode(node, base.tree) > 0;
    }

    ///////////////////// HANDLING OWN EVENTS //////////////////////////////

    function deleteNode(data) {
        // we have to get the jquery node instead of the jstree node
        var treeNode = base.tree.jstree("get_node", data);
        var jqueryNode = $("#" + treeNode.id);
        var index = jqueryNode.parent().children().index(jqueryNode[0]);

        var path = base.jsTreeUtils.getPath(base.tree.jstree("get_node", treeNode.id)).path;
        var parentIndex = path[0];

        if (path.length !== 1) {
            base.tree.jstree("delete_node", data);
            if (parentIndex === 1) {
                base.viewmodel.deleteDataPath(index);
                base.connectionDeleted(dataConnections, index);
            } else {
                base.viewmodel.deleteSelectionPath(index);
                base.connectionDeleted(selectionConnections, index);
            }
            setIdentifiers();
            base.refreshConnections();
            document.getElementById(base.id).style.height = "auto";
            document.getElementById(base.id).style.width = "auto";
        }
        base.saveOpenNodes();
        base.validate();
    }

    function requestAdded(data, deleteNeeded) {
        if (deleteNeeded) {
            base.tree.jstree("delete_node", data.node.id);
        }

        var index = data.position;
        var pathobj = base.jsTreeUtils.getPath(v_requestTree.jstree("get_node", data.original.id));
        var requestPath = pathobj.path;
        var requestStrPath = pathobj.strpath;

        var text;
        if (requestStrPath.lastIndexOf(".") === -1) {
            text = requestStrPath;
        } else {
            text = requestStrPath.substr(requestStrPath.lastIndexOf(".") + 1);
        }
        base.tree.jstree("create_node", data.parent, text, data.position);
        base.tree.jstree("open_node", data.parent);

        var parentIndex = base.jsTreeUtils.getPath(base.tree.jstree("get_node", data.parent)).path[0];
        if (parentIndex === 1) {
            base.viewmodel.addDataPath(requestStrPath, requestPath, index);
            base.connectionAdded(dataConnections, index, new DataConnectionEndpoint([1, index], index));
        } else {
            base.viewmodel.addSelectionPath(requestStrPath, requestPath, index);
            base.parent.selectionAdded(requestPath);
            base.connectionAdded(selectionConnections, index, new SelectionConnectionEndpoint([2, index], index));
        }
        setIdentifiers()
        base.refreshConnections();

        base.validate();
    }

    function connectionMoved(data) {
        base.tree.jstree("delete_node", data.original.id);

        var fromIndex = data.old_position;
        var toIndex = data.position;

        if (toIndex > fromIndex) {
            --toIndex;
        } else if (fromIndex > toIndex) {
            --fromIndex;
        }

        if (fromIndex == toIndex) {
            return;
        }

        var parentIndex = base.jsTreeUtils.getPath(base.tree.jstree("get_node", data.parent)).path[0];
        if (parentIndex === 1) {
            base.viewmodel.moveDataPath(fromIndex, toIndex);
            base.connectionMoved(dataConnections, fromIndex, toIndex);
        } else {
            base.viewmodel.moveSelectionPath(fromIndex, toIndex);
            base.connectionMoved(selectionConnections, fromIndex, toIndex);
        }
        setIdentifiers();
        base.refreshConnections();

        base.validate();
    }

    ///////////////////// HANDLING CONNECTIONS //////////////////////////////

    function setIdentifiers() {
        for (var i = 0; i < dataConnections.length; ++i) {
            dataConnections[i].setIdentifier(i);
        }
        for (var i = 0; i < selectionConnections.length; ++i) {
            selectionConnections[i].setIdentifier(i);
        }
    }

    function RequestEndpoint(p_path, p_identifier, p_options, p_getList, p_type) {
        var v_identifier = p_identifier;
        var v_getList = p_getList;

        var endpoint = new base.Endpoint(
            p_path,
            function() {
                return v_getList()[v_identifier];
            },
            "target",
            p_type,
            p_options,
            function(htmlObj) {
                var offset = htmlObj.offset();
                offset.top += htmlObj.height() / 2;
                return offset;
            }
        );

        endpoint.setIdentifier = function(identifier) {
            v_identifier = identifier;
        }

        return endpoint;
    }

    function DataConnectionEndpoint(p_path, p_identifier) {
        return new RequestEndpoint(
            p_path,
            p_identifier,
            {},
            base.viewmodel.getDataConnections,
            "DR_VM"
        );
    }

    function SelectionConnectionEndpoint(p_path, p_identifier) {
        return new RequestEndpoint(
            p_path,
            p_identifier,
            {
                "stroke": "red",
                "arrowHead": "source"
            },
            base.viewmodel.getSelectionConnections,
            "SR_VM"
        );
    }

    var viewConnectionPoint = {
        "getOffset": function() {
            var htmlObj;

            if (!base.isVisible) {
                var id = base.id + "_Header";
                htmlObj = $("#" + id);
            } else {
                var id = base.jsTreeUtils.getLastNodeIdFromPath(base.tree, [0]);
                htmlObj = $("#" + id + "_anchor");
            }

            var offset = htmlObj.offset();
            offset.left += htmlObj.width();
            offset.top += htmlObj.height() / 2;
            return offset;
        },

        "getZIndex": function() {
            return base.zIndex + 1;
        },

        "isEnabled": function() {
            return base.isEnabled;
        },

        "object": v_this
    }

    this.getEndpoint = function(identifier) {
        if (identifier == base.parent.getViewmodelIndex(v_this)) {
            return viewConnectionPoint;
        } else {
            return undefined;
        }
    };

    this.getConnectionInformation = function(identifier) {
        if (base.tree.jstree("get_node", identifier)) {
            return base.parent.getViewmodelIndex(v_this)
        } else {
            return undefined;
        }
    };

    this.getEndpoints = function() {
        var list = [];
        for (var i = 0; i < dataConnections.length; ++i) {
            list.push(dataConnections[i]);
        }
        for (var i = 0; i < selectionConnections.length; ++i) {
            list.push(selectionConnections[i]);
        }
        return list;
    };
}
//# sourceURL=GuiEditor\Views\View_ViewModelEditor.js
