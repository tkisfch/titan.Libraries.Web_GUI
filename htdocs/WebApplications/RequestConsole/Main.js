// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
var WebApplications = WebApplications || [];

WebApplications.push({'application': new RequestConsole_Application()});

function RequestConsole_Application() {
    "use strict";

    var v_appBase = new WebAppBase();
    var v_webAppModel;

    var jsfiles = [];

    var v_extension;
    var v_framework;
    var v_model;
    var v_viewmodel;
    var v_view;
    var v_binder;

    this.info = function() {
        return {
            defaultIcon: "WebApplications/RequestConsole/Res/main_icon.png",
            defaultName: "DsRestAPI Console"
        };
    };

    this.load = function(p_webAppModel, p_params, p_framework)  {
        v_webAppModel = p_webAppModel;
        v_framework = p_framework;

        function appConfigLoaded(config) {
            if (config.lastEditedApp != undefined) {
                new JsImportFromConfigTask(config.lastEditedApp + '/AppConfig.json', v_webAppModel.getFileHandler()).taskOperation(function(ok, extension) {
                    v_extension = extension;
                    v_appBase.load(jsfiles, [], start, v_webAppModel.getFileHandler());
                });
            } else {
                alert("Error, lastEditedApp missing from config, can't choose API.");
            }
        }

        new MultipleDirectoryListTask(
            [
                "WebApplications/RequestConsole/Models",
                "WebApplications/RequestConsole/Views",
                "WebApplications/RequestConsole/ViewModels"
            ],
            v_webAppModel.getFileHandler()
        ).taskOperation(function(ok, resources) {
            jsfiles = resources.jsfiles;
            v_webAppModel.loadAppConfig('CustomizableContent/RequestConsole/AppConfig.json', appConfigLoaded);
        });
    };

    this.unload = function(webappUnloaded) {
        function callback(exitApp) {
            if (exitApp) { v_appBase.unload(destroy); }
            webappUnloaded(exitApp);
        }

        v_view.unload(callback);
    };

    function destroy() {
        v_view.destroy();
        v_viewmodel.destroy();

        v_model = undefined;
        v_view = undefined;
        v_viewmodel = undefined;
        v_binder = undefined;
    }

    function start(p_callback) {
        v_model = new RequestConsole_Model(v_webAppModel, v_framework, v_extension);
        v_viewmodel = new RequestConsole_ViewModel(v_model);
        v_view = new RequestConsole_View(v_viewmodel, "TSGuiFrameworkMain", "RequestConsole_MainView");
        v_binder = new RequestConsole_Binder(v_viewmodel, v_view);
        v_viewmodel.setBinder(v_binder);

        function callback(ok, data) {
            if (ok) {
                v_view.applicationCreated();
                v_binder.fullRefresh();
            } else {
                alert(data);
            }
            if (typeof p_callback === "function") {
                p_callback();
            }
        }

        function setupLoaded() {
            v_viewmodel.setSetup(v_model.getSetup());
            var taskList = new SyncTaskList([new GenericTask(v_viewmodel.init), new GenericTask(v_view.init)], callback);
            taskList.taskOperation();
        }

        var config = v_webAppModel.getAppConfig();
        v_webAppModel.getSetupModel().setSetupDirectory(config.lastEditedApp + '/Setups');
        if (config.lastEditedSetup != undefined) {
            v_model.switchSetup(config.lastEditedSetup, setupLoaded);
        } else {
            v_model.newSetup();
            setupLoaded();
        }
    }
}

function RequestConsole_Binder(p_viewModel, p_view) {
    "use strict";

    var v_viewmodel = p_viewModel;
    var v_view = p_view;
    var v_this = this;

    this.fullRefresh = function() {
        v_view.getRequestEditorView().destroy();
        v_view.getRequestEditorView().fullRefresh();
        v_view.updateSetupName();
    };

    this.selectionOrFilterChanged = function(p_path, p_selection, p_filter, p_rangeFilter, p_writableInfo, p_GetOrSetData) {
        v_view.getRequestEditorView().selectionOrFilterChanged(p_path, p_selection, p_filter, p_rangeFilter, p_writableInfo, p_GetOrSetData);
    };
    
    this.modelChanged = function() {
        v_viewmodel.saveState();
        v_view.getRequestEditorView().getCodeEditorView().refresh(true);
    }
    
    this.responseArrived = function() {
        v_view.refreshResponseDisplays();
    }

    this.notifyChange = function(aChangeText) {
        function responseArrived()
        {
            v_this.fullRefresh();
            v_this.modelChanged();
        }
        v_viewmodel.saveState(aChangeText);
        v_viewmodel.sendRequest(responseArrived);
    }
}
//# sourceURL=RequestConsole\Main.js