// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function CViewModel_AutoGUI(aViewModel, aOptions)
{
    "use strict";
    /** private members */
    var mViewModel;
    var mBinder;
    var mReponseDataPaths;

    /** constructor */
    mViewModel = aViewModel;
    mReponseDataPaths = [];

    /** public functions - Interface for parent */ 
    this.setSelectionToControl = function(aSelection)
    {};
    
    this.setReponseDataPath = function(aExpectedReponseDataIndex, aReponseDataPath)
    {
        mReponseDataPaths[aExpectedReponseDataIndex] = aReponseDataPath;
    };
    
    this.setBinder = function(aBinder)
    {
        mBinder = aBinder;
    };

	/** public functions - Interface for views */ 
    this.getResponseElement = function()
    {
        return mViewModel.getResponseElement(mReponseDataPaths[0]);
    };
    
    this.command = function(aCklickedExpandedGetData, aOrigGetData, aValue, aIsContainer, aIsText)
    {
        function dataHasBeenSet(aData)
        {
            mBinder.notifyChange();
        }
        if (aCklickedExpandedGetData !== undefined && aOrigGetData.children !== undefined)
        {
            if (aOrigGetData.selection !== undefined)
            {
                if (aIsContainer)
                    aOrigGetData.selection = undefined;
                else if (aOrigGetData.selection[0] === aCklickedExpandedGetData.idxInList)
                    aOrigGetData.selection = [];
                else if (aCklickedExpandedGetData.idxInList != null)
                    aOrigGetData.selection = [aCklickedExpandedGetData.idxInList];
            }
            else
            {
                if (aIsContainer)
                    aOrigGetData.selection = [];
                else if (aCklickedExpandedGetData.idxInList != null)
                    aOrigGetData.selection = [aCklickedExpandedGetData.idxInList];
            }
            mBinder.notifyChange("Autogui changes");
        }
        else if (aCklickedExpandedGetData !== undefined && aOrigGetData.children === undefined && aIsText)
        {
            var newVal = prompt("Please enter new value", aValue);
            if (newVal != null)
            {
                var lIndxList = [];
                if (aCklickedExpandedGetData.idxInList !== undefined)
                    lIndxList[0] = aCklickedExpandedGetData.idxInList;
                mViewModel.getDsRestAPI().setData(dataHasBeenSet, aCklickedExpandedGetData.source, aCklickedExpandedGetData.element, newVal, aCklickedExpandedGetData.tp, aCklickedExpandedGetData.params, aCklickedExpandedGetData.ptcname, lIndxList);
            }
        }
    };
}

CViewModel_AutoGUI.getHelp = function() {
    return "This viewmodel is used with the same named view to visualize the response tree.";
}

CViewModel_AutoGUI.providesInterface = function() {
    return ["getResponseElement", "command"];
};

CViewModel_AutoGUI.getCustomDataSchema = function() {
    return {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Custom data for CViewModel_AutoGUI",
        "type": "object",
        "properties": {},
        "additionalProperties": false
    };
};

//# sourceURL=CustomizableApp\ViewModels\ViewModel_AutoGUI.js