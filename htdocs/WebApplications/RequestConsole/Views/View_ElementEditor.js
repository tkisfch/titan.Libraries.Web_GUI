// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function RequestConsole_ElementEditor_View(p_viewmodel, p_parentId, p_viewId, p_parent) {
    //"use strict";

    var v_id = p_viewId;
    var v_parentId = p_parentId;
    var v_editorDiv;
    var v_open = false;

    var v_parentView = p_parent;
    var v_viewmodel = p_viewmodel.getRequestEditorViewModel();
    
    

    var v_path;
    var v_request;

    var v_customDataForEditor = {
        "editorOptions": {
            "disable_array_reorder": true,
            "disable_edit_json": true,
            "disable_collapse": true,
            "no_additional_properties": true
        },
        "css": {
            "z-index": 2000
        },
        "closeable": true,
        "draggable": true,
        "headerText": "Edit request"
    }

    var v_viewmodelForJSONEditor;
    var editor;

    var v_this = this;

    function init() {
        v_viewmodelForJSONEditor = new ElementEditorViewmodel(v_viewmodel.getRequestCopy(v_path), save, v_request);
        
        editor = new CView_JSONEditor([v_viewmodelForJSONEditor], v_id, v_parentId, v_customDataForEditor);

        editor.applicationCreated();
        ViewUtils.applyCss(v_customDataForEditor, v_id);

        v_editorDiv = $("#" + v_id);
        v_editorDiv.on('click', function(){
            v_parentView.setFocusedObj(v_this);
        });
        v_editorDiv.on('dragstart', function(){
            v_parentView.setFocusedObj(v_this);
        });
        v_editorDiv.on('remove', closed);
        editor.refresh(true);

        ViewUtils.jumpToEditor(v_id);
    };

    function closed() {
        v_viewModel.findSelectionsAndFilters();
        v_open = false;
    }

    this.setDefaultZidx = function() {
        v_editorDiv.css("z-index" , 2500);
    };

    this.setZidx = function() {
        v_editorDiv.css("z-index" , 3500);
    };

    this.deletePressed = function() {
        v_parentView.deletePressed();
    };

    this.open = function(p_path, p_offset) {
        if (v_open) {
            v_this.close();
        }
        v_path = p_path;
        v_request = v_viewmodel.getRequestFromPath(v_path);
        v_customDataForEditor.offset = p_offset;
        init();
        v_open = true;
    };

    this.close = function() {
        if (v_open) {
            $("#" + v_id).remove();
        }
    };

    function save(newRequest) {
        var lGetOrSetData = "getData";
        if (v_request.setData)
            lGetOrSetData = "setData";
        v_request[lGetOrSetData].source = newRequest.source;
        v_request[lGetOrSetData].element = newRequest.element;
        v_parentView.requestRenamed(v_path, newRequest.element);
        v_request[lGetOrSetData].ptcname = newRequest.ptcname;
        v_request[lGetOrSetData].clientSideCache = newRequest.clientSideCache;
        v_request[lGetOrSetData].params = newRequest.params;
        v_request[lGetOrSetData].selection = newRequest.selection;
        v_request[lGetOrSetData].selectionValues = newRequest.selectionValues;
        v_request[lGetOrSetData].rangeFilter = newRequest.rangeFilter;
        v_request[lGetOrSetData].writableInfo = newRequest.writableInfo;
        v_request[lGetOrSetData].timeline = newRequest.timeline;
        v_request[lGetOrSetData].content = newRequest.content;
        v_request[lGetOrSetData].tp = newRequest.tp;

        v_parentView.selectionOrFilterChanged(v_path, v_request[lGetOrSetData].selection != undefined, v_request[lGetOrSetData].filter != undefined, v_request[lGetOrSetData].rangeFilter != undefined, v_request[lGetOrSetData].writableInfo != undefined, lGetOrSetData);
        v_viewmodel.modelChanged();
    }
}

function ElementEditorViewmodel(p_data, p_save, p_request) {
    var v_request = p_request;
    var SETDATA_SCHEMA = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "title": "SetData request",
        "properties": {
            "source": {
                "type": "string"
            },
            "ptcname": {
                "type": "string"
            },
            "element": {
                "type": "string"
            },
            "content": {
                "type": "string"
            },
            "tp": {
                "type": "integer",
                "enum": [
                  0,
                  1,
                  2,
                  3,
                  4,
                  5,
                  6,
                  7,
                  8,
                  9,
                  10,
                  11
                ],
              "options": {
                "enum_titles": [
                    "filtered item",
                    "integer",
                    "float",
                    "boolean",
                    "charstring",
                    "octetstring",
                    "hexstring",
                    "bitstring",
                    "integerList",
                    "floatList",
                    "charstringList",
                    "statusLED"
                ]
              }
            },
            "indxsInList": {
                "type": "array",
                "items": {
                    "title": "index",
                    "type": "integer"
                },
                "format": "table"
            },
            "params": {
                "type": "array",
                "items": {
                    "title": "param",
                    "type" : "object",
                    "additionalProperties": false,
                    "properties": {
                        "paramName": {
                            "type": "string"
                        },
                        "paramValue": {
                            "type": "string"
                        }
                    },
                    "required": [
                        "paramName",
                        "paramValue"
                    ]
                },
                "format": "table"
            }
        },
        "required": [
            "source",
            "element",
            "content",
            "tp"
        ]
    };

    var GETDATA_SCHEMA = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "title": "Edit request",
        "type": "object",
        "properties": {
            "source": {
                "description": "The source",
                "type": "string"
            },
            "element": {
                "description": "The element",
                "type": "string"
            },
            "ptcname": {
                "description": "The ptc name",
                "type": "string"
            },
            "clientSideCache": {
                "description": "Set to true, the data is get only once from the server.",
                "type": "boolean"
            },
            "params": {
                "type": "array",
                "format": "table",
                "items": {
                    "type": "object",
                    "title": "Parameter",
                    "properties": {
                        "paramName" : {
                            "type" : "string"
                        },
                        "paramValue" : {
                            "type" : "string"
                        }
                    },
                    "additionalProperties": false
                },
                "uniqueItems": true
            },
            "selection": {
                "type": "array",
                "format": "table",
                "items": {
                    "type": "integer",
                    "minimum": "0",
                    "title": "Selection item"
                },
                "uniqueItems": true
            },
            "selectionValues": {
                "type": "array",
                "format": "table",
                "items": {
                    "type": "string",
                    "title": "Selected value"
                }
            },
            "rangeFilter": {
                "type": "object",
                "properties": {
                    "offset": {
                        "type": "integer",
                        "minimum": 0
                    },
                    "count": {
                        "type": "integer",
                        "minimum": 0
                    }
                },
                "additionalProperties": false
            },
            "writableInfo": {
                "type": "boolean",
                "default": true
            },
            "timeline": {
                "type": "object",
                "properties": {
                    "period": {
                        "type": "number",
                        "minimum": 0.0
                    },
                    "maxpoints": {
                        "type": "integer",
                        "minimum": 1
                    },
                    "since": {
                        "type": "integer",
                        "minimum": 0
                    }
                },
                "additionalProperties": false,
                "default": {}
            }
        },
        "required": ["source", "element"]
    };

    var data = p_data;

    this.getJSONData = function(callback) {
        callback(data);
    };

    this.setJSONData = p_save;

    this.getSchema = function() {
        if (v_request && v_request.setData != undefined) {
            return SETDATA_SCHEMA;
        } else {
            return GETDATA_SCHEMA;
        }
    };
}
//# sourceURL=RequestConsole\Views\View_ElementEditor.js
