// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                                                       //
// All rights reserved. This program and the accompanying materials are made available under the     //
// terms of the Eclipse Public License v2.0 which accompanies this distribution, and is available at //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function RequestConsole_FilterEditor_View(p_viewmodel, p_parentId, p_viewId, p_parent, p_requestTree, p_helpTree) {

    var v_viewmodel = p_viewmodel.getRequestEditorViewModel();
    var v_parentId = p_parentId;
    var v_id = p_viewId;
    var v_parent = p_parent;
    var v_editorDiv;
    var v_open = false;

    var v_tree;
    var v_requestTree = p_requestTree;
    var v_helpTree = p_helpTree;
    var jsTreeUtils = new JsTreeUtils();

    var v_requestPath;

    var v_filterElementEditor = new RequestConsole_FilterElementEditor_View(v_viewmodel, "RequestConsole_Playground", "RequestConsole_FilterElementEditor", this);

    var v_this = this;

    ///////////////////// GENERAL VIEW FUNCTIONS //////////////////////////////

    function init() {
        var html = '' +
        '<div id="' + v_id + '" class="RequestConsole_FilterEditor">' +
            '<div id="' + v_id + '_Header' + '" class="RequestConsole_EditorHeader">' +
                '<button id="' + v_id + '_Button_Add" class="RequestConsole_EditorButtonLeft">Create</button>' +
                '<label id="' + v_id + '_HeaderText" class="RequestConsole_FilterEditorHeaderLabel">Edit filter</label>' +
                '<button id="' + v_id + '_Button_Context" class="RequestConsole_EditorButtonRight"><img src="WebApplicationFramework/Res/grip_white.png"></button>' +
                '<button id="' + v_id + '_Button_Close" class="RequestConsole_EditorButtonRight">X</button>' +
            '</div>' +
            '<div id="' + v_id + '_Tree"></div>' +
        '</div>';

        $("#" + v_parentId).append(html);
        v_tree = $("#" + v_id + "_Tree");
        createTree();

        v_editorDiv = $("#" + v_id);
        v_editorDiv.draggable({
            stop: function(event, ui) {
                document.getElementById(v_id).style.height = "auto";
                document.getElementById(v_id).style.width = "auto";
            },
            handle: "#" + v_id + "_Header",
            containment: [$("#" + v_parentId).offset().left, $("#" + v_parentId).offset().top, 20000, 20000]
        });
        v_editorDiv.on('click', function(){
            v_parent.setFocusedObj(v_this);
        });
        v_editorDiv.on('dragstart', function(){
            v_parent.setFocusedObj(v_this);
        });
    };

    this.destroy = function() {
        v_editorDiv.remove();
    };

    this.setDefaultZidx = function() {
        v_editorDiv.css("z-index", 800);
    };

    this.setFocusedObj = v_parent.setFocusedObj;

    this.setZidx = function() {
        v_editorDiv.css("z-index", 3000);
    };

    this.deletePressed = function() {
        var selected = v_tree.jstree("get_selected")[0];
        if (selected && selected.length != undefined) {
            closeEditors();

            var filterPath = jsTreeUtils.getPath(v_tree.jstree("get_node", selected)).path;
            filterPath.shift();
            v_viewmodel.deleteFilterPart(v_requestPath, filterPath);
            createTree();
        }
    };

    ///////////////////// OPENING AND CLOSING THE EDITOR //////////////////////////////

    this.open = function(p_path, p_offset) {
        if (v_open) {
            v_this.close();
        }
        v_requestPath = p_path;
        init();
        $("#" + v_id).offset(p_offset);
        $("#" + v_id).offset(p_offset);
        setupCallbacks();
        v_open = true;
    };

    this.close = function() {
        if (v_open) {
            v_open = false;
            closeEditors();
            var request = v_viewmodel.getRequestFromPath(v_requestPath);
            if (request.getData)
                v_parent.selectionOrFilterChanged(v_requestPath, request.getData.selection != undefined, request.getData.filter != undefined);
            else if (request.setData)
                v_parent.selectionOrFilterChanged(v_requestPath, request.setData.selection != undefined, request.setData.filter != undefined);
            $("#" + v_id).remove();
        }
    };

    function closeEditors() {
        v_filterElementEditor.close();
    }

    ///////////////////// CREATING AND REFRESHING THE TREE //////////////////////////////

    function refreshTree() {
        var data = v_viewmodel.getFilterAsTree(v_requestPath);
        v_tree.jstree(true).settings.core.data = data;
        v_tree.jstree("refresh");
        v_tree.jstree("open_all");
        v_viewmodel.modelChanged();
    }

    function createTree() {
        var data = v_viewmodel.getFilterAsTree(v_requestPath);
        v_tree.jstree("destroy");
        v_tree = v_tree.jstree({
            "core": {
                "data": data,
                "check_callback": function(operation, node, node_parent, node_position, more) {
                    if (operation === "copy_node" && jsTreeUtils.isNodeFromTree(node, v_helpTree) && !jsTreeUtils.isRoot(node_parent)) {
                        return dragFromHelpValidate(node.id, node_parent.id);
                    } else if (operation === "copy_node" && jsTreeUtils.isNodeFromTree(node, v_requestTree) && !jsTreeUtils.isRoot(node_parent)) {
                        var path = jsTreeUtils.getPath(v_requestTree.jstree("get_node", node.id)).path;
                        return hasPrefix(v_requestPath, path);
                    } else if (operation === "copy_node") {
                        return false;
                    } else {
                        return true;
                    }
                },
                "multiple": false,
                "animation": false,
                "worker": false
            },
            "plugins": ["contextmenu", "dnd"],
            "dnd": {
                "always_copy": true
            },
            "contextmenu": {
                "items": function($node) {
                    return {
                        "Edit": {
                            "label": "Edit filter",
                            "action": function(data) {
                                closeEditors();

                                var offset = data.reference.offset();
                                offset.left += data.reference.width();
                                var filterPath = jsTreeUtils.getPath(v_tree.jstree("get_node", data.reference)).path;
                                filterPath.shift();
                                v_filterElementEditor.open(v_requestPath, filterPath, offset, refreshTree);
                                v_parent.setFocusedObj(v_filterElementEditor);
                            },
                            "separator_after": true
                        },
                        "Add": {
                            "label": "Add param",
                            "action": function(data) {
                                closeEditors();

                                var node = v_tree.jstree("get_node", data.reference);
                                var filterPath = jsTreeUtils.getPath(node).path;
                                var position = v_tree.jstree("get_children_dom", node).length;
                                filterPath.shift();
                                filterPath.push(position);
                                if (v_viewmodel.isValidToAddParamToFilterRequest(v_requestPath, filterPath)) {
                                    var paramName = prompt("New param name: ");
                                    if (paramName != undefined) {
                                        v_viewmodel.addFilterPart(v_requestPath, filterPath, paramName);
                                        createTree();
                                    }
                                } else {
                                    alert('Cannot add param to a dataValue');
                                }
                            }
                        },
                        "RemapTo": {
                            "label": "Add remapTo",
                            "action": function(data) {
                                closeEditors();

                                var node = v_tree.jstree("get_node", data.reference);
                                var filterPath = jsTreeUtils.getPath(node).path;
                                var position = v_tree.jstree("get_children_dom", node).length;
                                filterPath.shift();
                                filterPath.push(position);
                                if (v_viewmodel.isValidToAddParamToFilterRequest(v_requestPath, filterPath)) {
                                    var paramName = "remapTo";
                                    v_viewmodel.addFilterPart(v_requestPath, filterPath, paramName);
                                    createTree();
                                } else {
                                    alert('Cannot add param to a dataValue');
                                }
                            }
                        },
                        "Delete": {
                            "label": "Delete",
                            "action": function(data) {
                                closeEditors();

                                var filterPath = jsTreeUtils.getPath(v_tree.jstree("get_node", data.reference)).path;
                                filterPath.shift();
                                v_viewmodel.deleteFilterPart(v_requestPath, filterPath);
                                createTree();
                            },
                            "separator_after": true
                        },
                        "ChangeParamName": {
                            "label": "Change param name",
                            "action": function(data) {
                                closeEditors();

                                var filterPath = jsTreeUtils.getPath(v_tree.jstree("get_node", data.reference)).path;
                                filterPath.shift();
                                var paramName = prompt("New param name: ");
                                if (paramName != undefined) {
                                    v_viewmodel.changeParamNameOfFilterRequest(v_requestPath, filterPath, paramName);
                                    createTree();
                                }
                            }
                        },
                        "ConvertToElementPresent": {
                            "label": "Convert to dataElementPresent",
                            "action": function(data) {
                                closeEditors();

                                var filterPath = jsTreeUtils.getPath(v_tree.jstree("get_node", data.reference)).path;
                                filterPath.shift();
                                if (v_viewmodel.isValidToAddParamToFilterRequest(v_requestPath, filterPath)) {
                                    v_viewmodel.convertToDataElementPresentInFilter(v_requestPath, filterPath);
                                }
                                createTree();
                            }
                        }
                    };
                },
                "select_node": true
            }
        });

        v_tree.bind("hover_node.jstree", function(e, data) {
            var filterPath = jsTreeUtils.getPath(v_tree.jstree("get_node", data.node.id)).path;
            filterPath.shift();
            var string = JSON.stringify(v_viewmodel.getFilterPartCopy(v_requestPath, filterPath), null, 4);
            $("#" + data.node.id).prop("title", string);
        });

        v_tree.bind("select_node.jstree", function (e, data) {
            closeEditors();
        });

        v_tree.bind("after_close.jstree", function (e, data) {
            v_tree.jstree("open_all");
        });

        v_tree.bind("redraw.jstree", function(e, data) {
            document.getElementById(v_id).style.height = "auto";
            document.getElementById(v_id).style.width = "auto";
        });

        v_tree.bind("copy_node.jstree", function(e, data) {
            if (jsTreeUtils.isNodeFromTree(data.original, v_helpTree)) {
                helpNodeCopied(data);
            } else if (jsTreeUtils.isNodeFromTree(data.original, v_requestTree)) {
                requestNodeCopied(data);
            }
        });

        v_tree.bind("ready.jstree", function(e, data) {
            v_tree.jstree("open_all");
        });
    }

    ///////////////////// HANDLING EVENTS //////////////////////////////

    function dragFromHelpValidate(p_helpId, p_filterId) {
        var helpPath = jsTreeUtils.getPath(v_helpTree.jstree("get_node", p_helpId)).path;
        var filterPath = jsTreeUtils.getPath(v_tree.jstree("get_node", p_filterId)).path;
        filterPath.shift();
        return v_viewmodel.isValidToConvertFilterToRequest(v_requestPath, filterPath, helpPath)
    }

    function helpNodeCopied(data) {
        closeEditors();
        v_tree.jstree("delete_node", data.node.id);
        var helpPath = jsTreeUtils.getPath(v_helpTree.jstree("get_node", data.original.id)).path;
        var filterPath = jsTreeUtils.getPath(v_tree.jstree("get_node", data.parent)).path;
        filterPath.shift();
        v_viewmodel.convertFilterPartToRequest(v_requestPath, filterPath, helpPath);
        setTimeout(createTree, 0);
    }

    function requestNodeCopied(data) {
        closeEditors();
        v_tree.jstree("delete_node", data.node.id);
        var path = jsTreeUtils.getPath(v_requestTree.jstree("get_node", data.original.id)).path;
        var dataValue = "%Parent" + (path.length - 1) + "%";
        var filterPath = jsTreeUtils.getPath(v_tree.jstree("get_node", data.parent)).path;
        filterPath.shift();
        v_viewmodel.convertFilterPartToDataValue(v_requestPath, filterPath, dataValue);
        setTimeout(createTree, 0);
    }

    function setupCallbacks() {
        $('#' + v_id + '_Button_Close').click(v_this.close);
        $('#' + v_id + '_Button_Context').click(function() {
            $("#" + jsTreeUtils.getNodeIdFromPath(v_tree, [0]) + " > a").contextmenu();
        });
        $('#' + v_id + '_Button_Add').click(function() {
            closeEditors();
            v_viewmodel.addFilterPart(v_requestPath, []);
            createTree();
        });
    }
}
//# sourceURL=RequestConsole\Views\View_FilterEditor.js