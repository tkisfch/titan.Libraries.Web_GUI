#!/usr/bin/env python
# -*- coding: utf-8 -*-
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

from BaseTestCase import *
from Framework_CommonFunctions import *
from GuiEditor_CommonFunctions import *

class GuiEditor_ConnectionTests(BaseTestCase):

    # ---------- testing connections with drag and drop ----------

    def test_helpToRequest(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        newSetup(self.driver)
        
        source = getNodeFromTreeByElements(self.driver, "GuiEditor_HelpTree", ["ExecCtrl", "EntityGroups"])
        dragAndDrop(self.driver, source.get_attribute("id"), "GuiEditor_RequestTree")
        
        self.assertIsNotNone(getNodeFromTreeByElements(self.driver, "GuiEditor_RequestTree", ["EntityGroups"]), "Dragging from help to request failed")

    def test_requestToViewmodel_dataConnection(self):
        self.test_helpToRequest()
        
        editorId = addViewModel(self.driver, "CViewModel_DynamicTable")
        source = getNodeFromTreeByElements(self.driver, "GuiEditor_RequestTree", ["EntityGroups"])
        target = getNodeFromTreeByPath(self.driver, editorId + "_Tree", [1])
        dragAndDrop(self.driver, source.get_attribute("id"), target.get_attribute("id"))
        
        self.assertIsNotNone(getNodeFromTreeByPath(self.driver, editorId + "_Tree", [1, 0]), "Dragging from request to viewmodel data connection failed")

    def test_requestToViewmodel_selectionConnection(self):
        self.test_helpToRequest()
        
        editorId = addViewModel(self.driver, "CViewModel_DynamicTable")
        source = getNodeFromTreeByElements(self.driver, "GuiEditor_RequestTree", ["EntityGroups"])
        target = getNodeFromTreeByPath(self.driver, editorId + "_Tree", [2])
        dragAndDrop(self.driver, source.get_attribute("id"), target.get_attribute("id"))
        
        self.assertIsNotNone(getNodeFromTreeByPath(self.driver, editorId + "_Tree", [2, 0]), "Dragging from request to viewmodel selection connection failed")

    def test_viewmodelToView(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        newSetup(self.driver)
        
        viewmodelEditorId = addViewModel(self.driver, "CViewModel_DynamicTable")
        source = getNodeFromTreeByPath(self.driver, viewmodelEditorId + "_Tree", [0])
        viewEditorId = addView(self.driver, "CView_ElementTable")
        target = getNodeFromTreeByPath(self.driver, viewEditorId + "_Tree", [0])
        dragAndDrop(self.driver, source.get_attribute("id"), target.get_attribute("id"))
        
        self.assertIsNotNone(getNodeFromTreeByPath(self.driver, viewEditorId + "_Tree", [0, 0]), "Dragging from viewmodel to view failed")

    # ---------- deleting connections via context menu ----------

    def test_helpToRequest_delClick(self):
        self.test_helpToRequest()
        nodeToDelete = getNodeFromTreeByPath(self.driver, "GuiEditor_RequestTree", [0])
        clickInTreeContextMenu(self.driver, nodeToDelete, "Delete")
        
        self.assertIsNone(getNodeFromTreeByPath(self.driver, "GuiEditor_RequestTree", [0]), 'Deleted request is still present');

    def test_requestToViewmodel_dataConnection_delClick(self):
        self.test_requestToViewmodel_dataConnection()
        nodeToDelete = getNodeFromTreeByPath(self.driver, "GuiEditor_ViewmodelEditor_0_Tree", [1, 0])
        clickInTreeContextMenu(self.driver, nodeToDelete, "Delete")
        
        self.assertIsNone(getNodeFromTreeByPath(self.driver, "GuiEditor_ViewmodelEditor_0_Tree", [1, 0]), 'Deleted viewmodel data connections is still present')

    def test_requestToViewmodel_selectionConnection_delClick(self):
        self.test_requestToViewmodel_selectionConnection()
        nodeToDelete = getNodeFromTreeByPath(self.driver, "GuiEditor_ViewmodelEditor_0_Tree", [2, 0])
        clickInTreeContextMenu(self.driver, nodeToDelete, "Delete")
        
        self.assertIsNone(getNodeFromTreeByPath(self.driver, "GuiEditor_ViewmodelEditor_0_Tree", [2, 0]), 'Deleted viewmodel selection connections is still present')

    def test_ViewmodelToView_delClick(self):
        self.test_viewmodelToView()
        nodeToDelete = getNodeFromTreeByPath(self.driver, "GuiEditor_ViewEditor_0_Tree", [0, 0])
        clickInTreeContextMenu(self.driver, nodeToDelete, "Delete")
        
        self.assertIsNone(getNodeFromTreeByPath(self.driver, "GuiEditor_ViewEditor_0_Tree", [0, 0]), 'Deleted viewmodel-view connections is still present')

    # ---------- deleting connections using the DEL key ----------

    def test_helpToRequest_delKey(self):
        self.test_helpToRequest()
        nodeToDelete = getNodeFromTreeByPath(self.driver, "GuiEditor_RequestTree", [0])
        nodeToDelete.click()
        pressDelete(self.driver)
        
        self.assertIsNone(getNodeFromTreeByPath(self.driver, "GuiEditor_RequestTree", [0]), 'Deleted request is still present');

    def test_requestToViewmodelDataConnection_delKey(self):
        self.test_requestToViewmodel_dataConnection()
        nodeToDelete = getNodeFromTreeByPath(self.driver, "GuiEditor_ViewmodelEditor_0_Tree", [1, 0])
        nodeToDelete.click()
        pressDelete(self.driver)
        
        self.assertIsNone(getNodeFromTreeByPath(self.driver, "GuiEditor_ViewmodelEditor_0_Tree", [1, 0]), 'Deleted viewmodel data connections is still present')

    def test_requestToViewmodelSelectionConnection_delKey(self):
        self.test_requestToViewmodel_selectionConnection()
        nodeToDelete = getNodeFromTreeByPath(self.driver, "GuiEditor_ViewmodelEditor_0_Tree", [2, 0])
        nodeToDelete.click()
        pressDelete(self.driver)
        
        self.assertIsNone(getNodeFromTreeByPath(self.driver, "GuiEditor_ViewmodelEditor_0_Tree", [2, 0]), 'Deleted viewmodel selection connections is still present')

    def test_ViewmodelToView_delKey(self):
        self.test_viewmodelToView()
        nodeToDelete = getNodeFromTreeByPath(self.driver, "GuiEditor_ViewEditor_0_Tree", [0, 0])
        nodeToDelete.click()
        pressDelete(self.driver)
        
        self.assertIsNone(getNodeFromTreeByPath(self.driver, "GuiEditor_ViewEditor_0_Tree", [0, 0]), 'Deleted viewmodel-view connections is still present')

if __name__ == "__main__":
    unittest.main(catchbreak=True, verbosity=2)