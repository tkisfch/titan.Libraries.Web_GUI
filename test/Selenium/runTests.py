#!/usr/bin/env python
# -*- coding: utf-8 -*-
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

from Framework_LoadingWebApps import *
from GuiEditor_SetupHandlingTests import *
from GuiEditor_EditorTests  import *
from GuiEditor_ConnectionTests import *
from RequestConsole_Tests import *

if __name__ == "__main__":
    unittest.main(catchbreak=True, verbosity=2)